﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using jp.co.ivp.ers.util;
using System.Collections.Specialized;
using System.Web;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.compile.entity;
using jp.co.ivp.ers.mvc;
using System.Net;
using System.Reflection;
using jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.MapperProcessor.Dispatcher;
using System.Web.Management;
using System.Web.Routing;
using System.IO;
using System.Net.Mime;

namespace jp.co.ivp.ers.mvc
{
    public abstract class ErsControllerBase
        : Controller
    {

        public ErsControllerBase()
        {

            ErsDebug.WriteExecutingLog("Request executing");

            ViewData[ErsViewContext.addtionalModelKey] = new Dictionary<string, ErsModelBase>();

            this.commandBus = this.GetCommandBus();

            this.mapperBus = this.GetMapperBus();

        }

        public virtual ICommandBus commandBus { get; private set; }

        public virtual IMapperBus mapperBus { get; private set; }

        protected virtual ICommandBus GetCommandBus()
        {
            return new DefaultCommandBus();
        }

        protected virtual IMapperBus GetMapperBus()
        {
            return new DefaultMapperBus();
        }

        private Dictionary<string, ErsModelBase> AdditionalModelDictionary
        {
            get
            {
                var dictionary = (Dictionary<string, ErsModelBase>)this.ViewData[ErsViewContext.addtionalModelKey];
                if (dictionary == null)
                {
                    dictionary = new Dictionary<string, ErsModelBase>();
                    ViewData[ErsViewContext.addtionalModelKey] = dictionary;
                }
                return dictionary;
            }
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {

            ErsDebug.WriteExecutingLog("Result executing");

            base.OnResultExecuting(filterContext);
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {

            ErsDebug.WriteExecutingLog("Result executed");

            ErsDebug.RaiseResourceError();

            base.OnResultExecuted(filterContext);
        }

        /// <summary>
        /// セッション情報を取得する。
		/// <para>Retrieves session information</para>
        /// </summary>
        /// <returns></returns>
        [NonAction]
        protected abstract ErsState GetErsSessionState();

        /// <summary>
        /// アクション実行前に実行される。
		/// <para>Before action is executed</para>
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ErsDebug.WriteExecutingLog("Action executing");

            base.OnActionExecuting(filterContext);

            //セッションの復元
            //HiddenState.Restore(Request);
        }

        /// <summary>
        /// アクション実行後に事項される
		/// <para>While the action is being executed</para>
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            this.SetSessionToView();
        }

        /// <summary>
        /// セッション情報をViewにセットする
		/// <para>Sets view session information</para>
        /// </summary>
        protected abstract void SetSessionToView();

        /// <summary>
        /// エラー時のハンドルを行う
		/// <para>During error handler execution</para>
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnException(ExceptionContext filterContext)
        {
            HttpContext.Response.Clear();

            var exception = filterContext.Exception;

            if (exception is TargetInvocationException)
            {
                exception = filterContext.Exception.InnerException;
            }

            if (exception is IErsHandlableException)
            {
                //ErsExceptionを処理する。
                var e = (IErsHandlableException)exception;
                filterContext.Result = OnErsException(e);
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.StatusCode = (e.httpStatus.HasValue) ? (int)e.httpStatus : (int)HttpStatusCode.OK;
            }
            else if (exception is HttpException)
            {
                var e = (HttpException)exception;

                if (e.WebEventCode == WebEventCodes.RuntimeErrorPostTooLarge)
                {
                    //ファイル長制限時は明示的に413に書き換える。
                    filterContext.Result = new HttpStatusCodeResult((int)HttpStatusCode.RequestEntityTooLarge);
                }
                else
                {
                    filterContext.Result = new HttpStatusCodeResult(e.GetHttpCode());
                }

                filterContext.ExceptionHandled = true;
            }
            else
            {
                var e = (Exception)exception;

                //Exception
                filterContext.Result = OnException(e);
                filterContext.ExceptionHandled = true;
            }
        }

        /// <summary>
        /// (ERS用)ERSExceptionを処理する
		/// <para>Processing ERS Exception (for ERS)</para>
        /// </summary>
        /// <param name="e"></param>
        [NonAction]
        protected virtual ActionResult OnErsException(IErsHandlableException e)
        {
            var model = e.model;
            if (model == null)
            {
                model = this.GetErrorModel();
            }
            model._isNotBackTo = e.isNoBackTo;
            return GetErrorView(e.returnUrl, model, e.Message);
        }

        protected virtual ErsModelBase GetErrorModel()
        {
            return new ErsErrorModel();
        }

        /// <summary>
		/// (ERS用)Exceptionを処理する / To handle the Exception (for ERS)
        /// </summary>
        /// <param name="e"></param>
        [NonAction]
        protected virtual ActionResult OnException(Exception e)
        {
            var exceptionMessage = this.GetExceptionMessage(e);

            //logging
            ErsCommon.loggingException(exceptionMessage);

            return GetErrorView(GetSystemErrorMessage(), exceptionMessage);
        }

        protected string GetExceptionMessage(Exception e)
        {
            var exceptionMessage = e.ToString() + Environment.NewLine;
            exceptionMessage += CorrectRequestValue("QUERYSTRING COLLECTION", Request.QueryString);
            exceptionMessage += CorrectRequestValue("FORM COLLECTION", Request.Form);
            exceptionMessage += CorrectRequestValue("COOKIES COLLECTION", Request.Cookies);
            try
            {
                exceptionMessage += CorrectRequestValue("SERVER VARIABLES COLLECTION", Request.ServerVariables);
            }
            catch
            {
            }
            return exceptionMessage;
        }

        /// <summary>
        /// Get System Error Message
        /// </summary>
        /// <returns></returns>
        [NonAction]
        protected virtual string GetSystemErrorMessage()
        {
            return ErsResources.GetMessage("system_error");
        }

        [NonAction]
        protected string CorrectRequestValue(string header, NameValueCollection values)
        {
            var reject_logging_field_names = new SetupConfigReader().reject_logging_field_names;
            var retVal = "-----" + header + "-----" + Environment.NewLine;
            foreach (var key in values.Keys)
            {
                if (key != null)
                {
                    var value = values[key.ToString()];
                    if (reject_logging_field_names.Contains(key))
                    {
                        value = "********";
                    }
                    retVal += key + " = " + value  + Environment.NewLine;
                }
            }
            return retVal;
        }

        [NonAction]
        protected string CorrectRequestValue(string header, HttpCookieCollection values)
        {
            var retVal = "-----" + header + "-----" + Environment.NewLine;
            foreach (var key in values.Keys)
            {
                retVal += key + " = " + values[key.ToString()].Value + Environment.NewLine;
            }
            return retVal;
        }

        /// <summary>
        /// エラーテンプレート名を取得
        /// </summary>
        /// <returns></returns>
        protected virtual string GetErrorTemplateName()
        {
            if (HttpContext.Request.IsSecureConnection)
            {
                return "error_ssl";
            }
            else
            {
                return "error";
            }
        }

        /// <summary>
        /// エラー表示用のViewを取得する。(指定した文字列を表示)
		/// <para>Get ErrorView and display the specified string</para>
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [NonAction]
        protected virtual ViewResult GetErrorView(string message = null)
        {
            this.SetSessionToView();

            var result = new ViewResult();
            result.ViewName = this.GetErrorTemplateName();
            result.ViewData = this.ViewData;

            if (!string.IsNullOrEmpty(message))
            {
                result.ViewData.ModelState.AddModelError("exception", message);
            }

            return result;
        }

        /// <summary>
        /// エラー表示用のViewを取得する。(Exception用)
		/// <para>Get the Error View and the specified exception message</para>
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [NonAction]
        protected virtual ViewResult GetErrorView(string message, string exceptionMessage)
        {
            var result = GetErrorView(message);

            result.ViewData.Model = this.GetErrorModel();
            ErsDebug.SetDebugErrorToView(exceptionMessage, result);

            return result;
        }

        /// <summary>
        /// エラー表示用のViewを取得する。(戻り値を指定＋Hiddenに出力するモデルを指定)
		/// <para>Get ErrorView and specified Model's return values and hidden values</para>
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [NonAction]
        protected virtual ViewResult GetErrorView(string returnUrl, ErsModelBase model, string message = "")
        {
            var result = GetErrorView(message);

            //hidden出力
            if (model != null)
            {
                model.SetOutputHidden(true);
                result.ViewData.Model = model;
            }
            //戻りページ設定
            result.ViewBag.returnUrl = returnUrl;

            return result;
        }

        /// <summary>
        /// エラーのViewを取得（戻り先URL＆複数モデルをOutputHiddenして出力）
		/// <para>(Output to multiple models OutputHidden URL & return address) Gets the View of the error</para>
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [NonAction]
        protected virtual ViewResult GetErrorView(string returnUrl, params ErsModelBase[] models)
        {
            var result = GetErrorView(null);
            //hidden出力
            if (models != null)
            {
                foreach (var model in models)
                {
                    if (model != null)
                    {
                        model.SetOutputHidden(true);
                        AddModelToView(result.ViewData, model.GetType().Name, model);
                    }
                }
            }
            //戻りページ設定
            result.ViewBag.returnUrl = System.IO.Path.GetDirectoryName(Request.Path).Replace("\\", "/").Substring(1) + "/" + returnUrl;
            return result;
        }

        /// <summary>
        /// エラーのあったプロパティの値をnullに更新し、ModelStateをClearする。
		/// <para>Set to null the value of the properties that has an error and Clear the ModelState.</para>
        /// </summary>
        [NonAction]
        public virtual void ClearModelState(params IErsModelBase[] models)
        {
            foreach (var model in models)
            {
                if (model == null)
                {
                    continue;
                }

                foreach (var key in model.GetInvalidFieldNames())
                {
                    ModelState.Remove(key);
                }

                model.ClearInvalidFields();
            }
        }

        /// <summary>
        /// エラーのあった指定されたプロパティの値をnullに更新し、ModelStateをClearする。
        /// </summary>
        [NonAction]
        protected virtual void ClearInvalidField(ErsModelBase model, string propertyName)
        {
            if (model == null)
            {
                return;
            }

            var t = model.GetType();
            var prop = t.GetProperty(propertyName);
            if (!ModelState.IsValidField(propertyName) && prop != null)
            {
                model.RemoveInvalidField(propertyName);
                if (prop.CanWrite)
                {
                    var type = prop.GetType();
                    var defaultValue = type.IsValueType ? Activator.CreateInstance(type) : null;
                    prop.SetValue(model, defaultValue, null);
                }
            }

            ModelState.Remove(propertyName);
        }

        /// <summary>
        /// 複数のモデルをViewに渡せるようにサポート
		/// <para>Support for multiple models so that it can be passed to the View</para>
        /// </summary>
        /// <param name="view"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [NonAction]
        public void AddModelToView(ViewDataDictionary viewData, string name, ErsModelBase model)
        {
            this.AdditionalModelDictionary[name] = model;
        }

        [NonAction]
        public bool HasAdditionalModel(string name)
        {
            return this.AdditionalModelDictionary.ContainsKey(name);
        }

        [NonAction]
        public ErsModelBase GetAdditionalModel(string name)
        {
            if (this.HasAdditionalModel(name))
            {
                return this.AdditionalModelDictionary[name];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 複数のモデルをViewに渡せるようにサポート
		/// <para>Support for multiple models so that it can be passed to the View</para>
        /// </summary>
        /// <param name="view"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [NonAction]
        public void AddModelToView(string name, ErsModelBase model)
        {
            AddModelToView(this.ViewData, name, model);
        }

        /// <summary>
        /// 複数のモデルをViewに渡せるようにサポート
		///<para>Support for multiple models so that it can be passed to the View</para>
        /// </summary>
        /// <param name="view"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [NonAction]
        protected void AddModelToView(ErsModelBase model)
        {
            AddModelToView(this.ViewData, model.GetType().Name, model);
        }

        /// <summary>
        /// eckの値をチェックし、エラー戻りかどうかを判定する。
		/// <para>Check the value of the eck, to determine whether the error return.</para>
        /// </summary>
        /// <param name="eck"></param>
        /// <returns></returns>
        [NonAction]
        protected bool IsErrorBack(EnumEck? eck)
        {
            return (eck != null && eck != EnumEck.Normal);
        }

        /// <summary>
        /// redirect to action with session values.
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        protected override RedirectToRouteResult RedirectToAction(string actionName, string controllerName, System.Web.Routing.RouteValueDictionary routeValues)
        {
            if (routeValues == null)
            {
                routeValues = new System.Web.Routing.RouteValueDictionary();
            }

            this.PutSessionValueToRouteValueDictionary(actionName, controllerName, routeValues);

            this.SetSessionToView();

            return base.RedirectToAction(actionName, controllerName, routeValues);
        }

        /// <summary>
        /// Redirect to specified url after store session values.
        /// </summary>
        /// <param name="rootUrl"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        protected virtual ActionResult RedirectToAction(string rootUrl, string actionName, string controllerName, System.Web.Routing.RouteValueDictionary routeValues)
        {
            if (routeValues == null)
            {
                routeValues = new RouteValueDictionary();
            }

            this.PutSessionValueToRouteValueDictionary(actionName, controllerName, routeValues);

            this.SetSessionToView();

            if (string.IsNullOrEmpty(rootUrl))
            {
                return base.RedirectToAction(actionName, controllerName, routeValues);
            }
            else
            {
                var uri =new Uri(rootUrl);
                rootUrl = string.Format("{0}://{1}/", uri.Scheme, uri.Authority);

                var actionUrl = this.Url.Action(actionName, controllerName, routeValues);

                if (rootUrl.EndsWith("/") && !string.IsNullOrEmpty(actionUrl))
                {
                    actionUrl = actionUrl.Substring(1);
                }
                return base.Redirect(rootUrl + actionUrl);
            }
        }

        protected virtual void PutSessionValueToRouteValueDictionary(string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            //sessionの値
            var secureList = new List<Dictionary<string, object>>();
            var normalList = new List<Dictionary<string, object>>();
            var state = this.GetErsState();
            if (state != null)
            {
                state.OutputHidden(secureList, true);
                state.OutputHidden(normalList, false);
            }

            var actionUrl = this.Url.Action(actionName, controllerName, routeValues);
            if (ErsViewHelper.CheckSessionUrl(actionUrl))
            {
                this.AddRouteValues(routeValues, secureList);
            }

            if (ErsViewHelper.CheckErsUrl(actionUrl))
            {
                this.AddRouteValues(routeValues, normalList);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeValues"></param>
        /// <param name="secureList"></param>
        protected virtual void AddRouteValues(System.Web.Routing.RouteValueDictionary routeValues, List<Dictionary<string, object>> secureList)
        {
            foreach (var item in secureList)
            {
                if (item["value"] != null && (!(item["value"] is string) || (item["value"] is string && !string.IsNullOrEmpty((string)item["value"]))))
                    routeValues.Add(Convert.ToString(item["name"]), item["value"]);
            }
        }

        [NonAction]
        public ActionResult CsvFile(string filePath, string fileNameForClient = null)
        {
            if (fileNameForClient == null)
            {
                fileNameForClient = Path.GetFileName(filePath);
            }
            return base.File(filePath, "text/csv", fileNameForClient);
        }

        [NonAction]
        public ActionResult PdfFile(Byte[] pdfStream, string fileNameForClient = null)
        {
            return base.File(pdfStream, MediaTypeNames.Application.Pdf);
        }

        protected abstract ErsState GetErsState();

        /// <summary>
        /// Informationにメッセージを追加する。
        /// </summary>
        /// <param name="informationMassage"></param>
        public virtual void AddInformation(string informationMassage)
        {
            if (string.IsNullOrEmpty(informationMassage))
            {
                return;
            }

            ers_information_massage_list.Add(informationMassage);
        }

        /// <summary>
        /// Whether this model has information value.
        /// </summary>
        public bool HasInformation
        {
            get { return ers_information_massage_list.Count != 0; }
        }

        public List<string> ers_information_massage_list
        {
            get
            {
                if (this.ViewData["ers_information_massage_list"] == null)
                {
                    this.ViewData["ers_information_massage_list"] = new List<string>();
                }
                return ((List<string>)this.ViewData["ers_information_massage_list"]);
            }
        }

        /// <summary>
        /// 受信を使用してバインドしたモデルを取得する。
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        public TModel GetBindedModel<TModel>() 
            where TModel : ErsModelBase
        {
            var instance = (TModel)Activator.CreateInstance(typeof(TModel));
            this.TryUpdateModel(instance);
            instance.controller = this;
            return instance;
        }

        protected virtual ViewResult GetHttpErrorView(HttpErrorModel httpError)
        {
            ClearModelState(httpError);

            HttpContext.Response.Clear();

            HttpContext.Response.StatusCode = httpError.validStatusCode;

            if (!httpError.StatusCodeViewExists())
            {
                return this.GetErrorView("", httpError, ErsResources.GetMessage(httpError.errorKey, new SetupConfigReader().nor_url));
            }

            return View(httpError.ViewName, httpError);
        }
    }
}
