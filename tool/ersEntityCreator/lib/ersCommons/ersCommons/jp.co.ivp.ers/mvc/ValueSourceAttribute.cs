﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc
{
    public class ValueSourceAttribute
        : Attribute
    {
		/// <summary>
		/// Sets the Value Source and secured tag
		/// </summary>
        internal VALUE_SOURCE VALUE_SOURCE;
        internal bool IsSecure;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="VALUE_SOURCE">Data source for session</param>
        /// <param name="IsSecure">Add Secure attribute</param>
        public ValueSourceAttribute(VALUE_SOURCE VALUE_SOURCE, bool IsSecure)
        {
            this.VALUE_SOURCE = VALUE_SOURCE;
            this.IsSecure = IsSecure;
        }
    }
}
