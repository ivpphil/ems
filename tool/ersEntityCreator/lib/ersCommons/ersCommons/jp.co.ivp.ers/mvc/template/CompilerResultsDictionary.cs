﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom.Compiler;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.Web;
using System.IO;
using System.Threading;

namespace jp.co.ivp.ers.mvc.template
{
    class CompilerResultsDictionary
    {
        private static object _lockObject = new object();
        private static bool IsLocking { get; set; }

        /// <summary>
        /// ロックを取得する / Get Lock
        /// </summary>
        public static void GetLock()
        {
            Monitor.Enter(_lockObject);
            IsLocking = true;
        }

        /// <summary>
        /// ロックを開放する。/ Release the Lock
        /// </summary>
        public static void ReleaseLock()
        {
            IsLocking = false;
            Monitor.Exit(_lockObject);
        }

        /// <summary>
        /// テンプレートリロード中は、読み出しを待つ。/ Wait for release during template reload
        /// </summary>
        public static void WaitReleaseLock()
        {
            if (IsLocking)
            {
                lock (_lockObject)
                {
                }
            }
        }

		/// <summary>
		/// テンプレートリロード中は、読み出しを待つ。/ Wait for read during template reload
		/// </summary>
        private static Dictionary<string, CompilerResults> dicCompilerResults
        {
            get
            {
                WaitReleaseLock();

                var result = (Dictionary<string, CompilerResults>)ErsCommonContext.GetObjectFromApplication("dicCompilerResults");
                if (result == null)
                {
                    result = new Dictionary<string, CompilerResults>();
                    ErsCommonContext.SetObjectToApplication("dicCompilerResults", result);
                }
                return result;
            }
            set
            {
                ErsCommonContext.SetObjectToApplication("dicCompilerResults", value);
            }
        }

        /// <summary>
        /// 指定されたファイルがコンパイル済みか否か
        /// </summary>
        /// <param name="requestedPath"></param>
        /// <returns></returns>
        internal static bool IsCompiled(string requestedPath)
        {
            string key = GetCompilerResultsKey(requestedPath);
            return dicCompilerResults.ContainsKey(key);
        }

        /// <summary>
        /// コンパイル済みのコードを取得する
        /// </summary>
        /// <param name="requestedPath"></param>
        /// <returns></returns>
        internal static CompilerResults GetCompilerResults(string requestedPath)
        {
            string key = GetCompilerResultsKey(requestedPath);
            CompilerResults result;

            if (dicCompilerResults.TryGetValue(key, out result))
            {
                return result;
            }

            return null;
        }

        /// <summary>
        /// コンパイル結果を格納する
        /// </summary>
        /// <param name="requestedPath"></param>
        /// <param name="compilerResults"></param>
        internal static void RegistCompilerResults(string requestedPath, CompilerResults compilerResults)
        {
            string key = GetCompilerResultsKey(requestedPath);
            dicCompilerResults[key] = compilerResults;
        }

        /// <summary>
        /// コンパイル結果を格納するDictionaryのKeyを取得する。
        /// </summary>
        /// <param name="requestedPath"></param>
        /// <returns></returns>
        private static string GetCompilerResultsKey(string requestedPath)
        {
            return ErsCommonContext.MapPath("~/").ToUpper() + ErsCommonContext.MapPath(requestedPath).ToUpper();
        }

        /// <summary>
        /// コンパイル結果をリロードする
        /// </summary>
        /// <param name="filePath"></param>
        internal static void Reload(string filePath)
        {
            GetLock();
            try
            {
                string key = GetCompilerResultsKey(filePath);
                if (dicCompilerResults != null && dicCompilerResults.ContainsKey(key))
                {
                    dicCompilerResults.Remove(key);
                }
            }
            finally
            {
                ReleaseLock();
            }
        }
    }
}
