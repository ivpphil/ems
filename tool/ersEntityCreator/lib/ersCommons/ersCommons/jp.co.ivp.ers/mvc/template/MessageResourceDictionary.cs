﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.Web;

namespace jp.co.ivp.ers.mvc.template
{
    public class MessageResourceDictionary
    {
        /// <summary>
        /// Get MessageResource.config location.
        /// </summary>
        public static string MessageResourceFileName
        {
            get
            {
                var setup = new SetupConfigReader();
                var messageResourceFileName = setup.MessageResource;

                if (string.IsNullOrEmpty(messageResourceFileName))
                {
                    messageResourceFileName = "~/Views/MessageResource.config";
                }

                return ErsCommonContext.MapPath(messageResourceFileName);
            }
        }

        /// <summary>
        /// メッセージのリソースを取得する
		/// <para>Gets the resource of the message</para>
        /// </summary>
        internal static Dictionary<string, string> dicMessageResource
        {
            get
            {
                var result = (Dictionary<string, string>)ErsCommonContext.GetObjectFromApplication("dicMessageResource");
                if (result == null)
                {
                    Reload();
                    result = (Dictionary<string, string>)ErsCommonContext.GetObjectFromApplication("dicMessageResource");
                }
                return result;
            }
            private set
            {
                ErsCommonContext.SetObjectToApplication("dicMessageResource", value);
            }
        }

        /// <summary>
        /// メッセージリソースを初期化
		/// <para>Initializes the message resource</para>
        /// </summary>
        public static void Reload()
        {
            dicMessageResource = ErsCommon.LoadXml(MessageResourceFileName);
        }
    }
}
