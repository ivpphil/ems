﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.Web;

namespace jp.co.ivp.ers.mvc.template
{
    public class FieldNameResourceDictionary
    {
        /// <summary>
        /// Get FieldNameResource.config location.
        /// </summary>
        public static string FieldNameResourceFileName
        {
            get
            {
                var setup = new SetupConfigReader();
                var fieldNameResourceFileName = setup.FieldNameResource;

                if (string.IsNullOrEmpty(fieldNameResourceFileName))
                {
                    fieldNameResourceFileName = "~/Views/FieldNameResource.config";
                }

                return ErsCommonContext.MapPath(fieldNameResourceFileName);
            }
        }

        /// <summary>
        /// フィールド名や単語のリソースを取得する
		/// <para>Gets the resource of field names and words</para>
        /// </summary>
        internal static Dictionary<string, string> dicFieldNameResource
        {
            get
            {
                var result = (Dictionary<string, string>)ErsCommonContext.GetObjectFromApplication("dicFieldNameResource");
                if (result == null)
                {
                    Reload();
                    result = (Dictionary<string, string>)ErsCommonContext.GetObjectFromApplication("dicFieldNameResource");
                }
                return result;
            }
            private set
            {
                ErsCommonContext.SetObjectToApplication("dicFieldNameResource", value);
            }
        }

        /// <summary>
        /// フィールドリソースを初期化 
		/// <para>Initializes the resource fields</para>
        /// </summary>
        public static void Reload()
        {
            dicFieldNameResource = ErsCommon.LoadXml(FieldNameResourceFileName);
        }
    }
}
