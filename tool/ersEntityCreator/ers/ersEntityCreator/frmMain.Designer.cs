﻿namespace ersEntityCreator
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreate = new System.Windows.Forms.Button();
            this.lblTableName = new System.Windows.Forms.Label();
            this.lblNamespace = new System.Windows.Forms.Label();
            this.txtTableName = new System.Windows.Forms.TextBox();
            this.txtNamespace = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtConnectionStrings = new System.Windows.Forms.TextBox();
            this.lblConnectionStrings = new System.Windows.Forms.Label();
            this.lblPath = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.lblLibPath = new System.Windows.Forms.Label();
            this.txtLibPath = new System.Windows.Forms.TextBox();
            this.btnBrowsePath = new System.Windows.Forms.Button();
            this.fbdPath = new System.Windows.Forms.FolderBrowserDialog();
            this.btnBrowseLibPath = new System.Windows.Forms.Button();
            this.fbdLibPath = new System.Windows.Forms.FolderBrowserDialog();
            this.rtxtResults = new System.Windows.Forms.RichTextBox();
            this.lblMsgLog = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(118, 88);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 0;
            this.btnCreate.Text = "CREATE";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // lblTableName
            // 
            this.lblTableName.AutoSize = true;
            this.lblTableName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTableName.Location = new System.Drawing.Point(30, 23);
            this.lblTableName.Name = "lblTableName";
            this.lblTableName.Size = new System.Drawing.Size(98, 20);
            this.lblTableName.TabIndex = 1;
            this.lblTableName.Text = "Table Name:";
            // 
            // lblNamespace
            // 
            this.lblNamespace.AutoSize = true;
            this.lblNamespace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNamespace.Location = new System.Drawing.Point(31, 46);
            this.lblNamespace.Name = "lblNamespace";
            this.lblNamespace.Size = new System.Drawing.Size(98, 20);
            this.lblNamespace.TabIndex = 2;
            this.lblNamespace.Text = "Namespace:";
            // 
            // txtTableName
            // 
            this.txtTableName.Location = new System.Drawing.Point(135, 22);
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.Size = new System.Drawing.Size(152, 20);
            this.txtTableName.TabIndex = 3;
            // 
            // txtNamespace
            // 
            this.txtNamespace.Location = new System.Drawing.Point(135, 48);
            this.txtNamespace.Name = "txtNamespace";
            this.txtNamespace.Size = new System.Drawing.Size(152, 20);
            this.txtNamespace.TabIndex = 4;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(118, 279);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 7;
            this.btnUpdate.Text = "UPDATE";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtConnectionStrings
            // 
            this.txtConnectionStrings.Location = new System.Drawing.Point(34, 161);
            this.txtConnectionStrings.Name = "txtConnectionStrings";
            this.txtConnectionStrings.Size = new System.Drawing.Size(253, 20);
            this.txtConnectionStrings.TabIndex = 5;
            // 
            // lblConnectionStrings
            // 
            this.lblConnectionStrings.AutoSize = true;
            this.lblConnectionStrings.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnectionStrings.Location = new System.Drawing.Point(31, 138);
            this.lblConnectionStrings.Name = "lblConnectionStrings";
            this.lblConnectionStrings.Size = new System.Drawing.Size(140, 20);
            this.lblConnectionStrings.TabIndex = 6;
            this.lblConnectionStrings.Text = "ConnectionStrings";
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPath.Location = new System.Drawing.Point(31, 184);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(231, 20);
            this.lblPath.TabIndex = 9;
            this.lblPath.Text = "Path (Main Project Root Folder)";
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(34, 207);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(179, 20);
            this.txtPath.TabIndex = 8;
            // 
            // lblLibPath
            // 
            this.lblLibPath.AutoSize = true;
            this.lblLibPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibPath.Location = new System.Drawing.Point(31, 230);
            this.lblLibPath.Name = "lblLibPath";
            this.lblLibPath.Size = new System.Drawing.Size(93, 20);
            this.lblLibPath.TabIndex = 11;
            this.lblLibPath.Text = "Library Path";
            // 
            // txtLibPath
            // 
            this.txtLibPath.Location = new System.Drawing.Point(34, 253);
            this.txtLibPath.Name = "txtLibPath";
            this.txtLibPath.ReadOnly = true;
            this.txtLibPath.Size = new System.Drawing.Size(179, 20);
            this.txtLibPath.TabIndex = 10;
            // 
            // btnBrowsePath
            // 
            this.btnBrowsePath.Location = new System.Drawing.Point(219, 205);
            this.btnBrowsePath.Name = "btnBrowsePath";
            this.btnBrowsePath.Size = new System.Drawing.Size(75, 23);
            this.btnBrowsePath.TabIndex = 12;
            this.btnBrowsePath.Text = "Browse";
            this.btnBrowsePath.UseVisualStyleBackColor = true;
            this.btnBrowsePath.Click += new System.EventHandler(this.btnBrowsePath_Click);
            // 
            // fbdPath
            // 
            this.fbdPath.ShowNewFolderButton = false;
            // 
            // btnBrowseLibPath
            // 
            this.btnBrowseLibPath.Location = new System.Drawing.Point(219, 251);
            this.btnBrowseLibPath.Name = "btnBrowseLibPath";
            this.btnBrowseLibPath.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseLibPath.TabIndex = 13;
            this.btnBrowseLibPath.Text = "Browse";
            this.btnBrowseLibPath.UseVisualStyleBackColor = true;
            this.btnBrowseLibPath.Click += new System.EventHandler(this.btnBrowseLibPath_Click);
            // 
            // fbdLibPath
            // 
            this.fbdLibPath.ShowNewFolderButton = false;
            // 
            // rtxtResults
            // 
            this.rtxtResults.BackColor = System.Drawing.SystemColors.Control;
            this.rtxtResults.Location = new System.Drawing.Point(28, 343);
            this.rtxtResults.Name = "rtxtResults";
            this.rtxtResults.ReadOnly = true;
            this.rtxtResults.Size = new System.Drawing.Size(266, 92);
            this.rtxtResults.TabIndex = 14;
            this.rtxtResults.Text = "";
            // 
            // lblMsgLog
            // 
            this.lblMsgLog.AutoSize = true;
            this.lblMsgLog.Location = new System.Drawing.Point(35, 324);
            this.lblMsgLog.Name = "lblMsgLog";
            this.lblMsgLog.Size = new System.Drawing.Size(71, 13);
            this.lblMsgLog.TabIndex = 15;
            this.lblMsgLog.Text = "Message Log";
            this.lblMsgLog.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 458);
            this.Controls.Add(this.lblMsgLog);
            this.Controls.Add(this.rtxtResults);
            this.Controls.Add(this.btnBrowseLibPath);
            this.Controls.Add(this.btnBrowsePath);
            this.Controls.Add(this.lblLibPath);
            this.Controls.Add(this.txtLibPath);
            this.Controls.Add(this.lblPath);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lblConnectionStrings);
            this.Controls.Add(this.txtConnectionStrings);
            this.Controls.Add(this.txtNamespace);
            this.Controls.Add(this.txtTableName);
            this.Controls.Add(this.lblNamespace);
            this.Controls.Add(this.lblTableName);
            this.Controls.Add(this.btnCreate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "ErsEntityCreator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label lblTableName;
        private System.Windows.Forms.Label lblNamespace;
        private System.Windows.Forms.TextBox txtTableName;
        private System.Windows.Forms.TextBox txtNamespace;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtConnectionStrings;
        private System.Windows.Forms.Label lblConnectionStrings;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label lblLibPath;
        private System.Windows.Forms.TextBox txtLibPath;
        private System.Windows.Forms.Button btnBrowsePath;
        private System.Windows.Forms.FolderBrowserDialog fbdPath;
        private System.Windows.Forms.Button btnBrowseLibPath;
        private System.Windows.Forms.FolderBrowserDialog fbdLibPath;
        private System.Windows.Forms.RichTextBox rtxtResults;
        private System.Windows.Forms.Label lblMsgLog;
    }
}

