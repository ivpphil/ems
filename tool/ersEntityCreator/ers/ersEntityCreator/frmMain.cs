﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using System.Configuration;
using System.Xml;
using System.IO;

namespace ersEntityCreator
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            txtConnectionStrings.Text = ConfigurationManager.AppSettings["connection"];
            txtLibPath.Text = ConfigurationManager.AppSettings["libpath"];
            txtPath.Text = ConfigurationManager.AppSettings["path"];
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            bool manyTables = false;
            
            if (String.IsNullOrEmpty(txtTableName.Text) || String.IsNullOrEmpty(txtNamespace.Text))
            {
                rtxtResults.AppendText("Table Name and/or Namespace is Empty." + Environment.NewLine);
                rtxtResults.ScrollToCaret();
                return;
            }

            var tableName = txtTableName.Text;
            var nameSpace = txtNamespace.Text;
            List<string> tables = new List<string>();

            if(tableName.Contains(","))
            {
                string[] words = tableName.Split(',');
                foreach (string word in words)
                {
                    tables.Add(word.Trim());
                }
                manyTables = true;
            }

            if(!manyTables)
            {
                makeFiles(tableName, nameSpace);
            }
            else
            {
                foreach (string table in tables)
	            {
                    makeFiles(table, nameSpace);
	            }
            }
        }

        private void makeFiles(string table_name, string name_space)
        {   
            ersEntityCreatorFunctions func = new ersEntityCreatorFunctions();

            func.tableName = table_name;
            func.libpath = ConfigurationManager.AppSettings["libpath"];
            func.nameSpace = name_space;

            if (func.CheckFolder())
            {
                func.CreateFolder();
            }

            ErsCommonContext.Initialize(null, ConfigurationManager.AppSettings["path"]);

            Dictionary<string, string> columns = new Dictionary<string, string>();

            ErsDB_parent db = new ErsDB_parent(table_name, ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(ConfigurationManager.AppSettings["connection"]));

            try
            {
                foreach (DataColumn dc in db.Schema)
                {
                    string typeName = func.GetTypeName(dc);
                    columns.Add(dc.ColumnName, typeName);
                }
            }

            catch (Exception)
            {
                rtxtResults.AppendText("Table " + table_name + " does not exist." + Environment.NewLine);
                rtxtResults.ScrollToCaret();
                return;
            }

            //check if file exists then error if true
            if (!func.CheckFiles())
            {
                rtxtResults.AppendText("File or Files of " + table_name + " Already Exist" + Environment.NewLine);
                rtxtResults.ScrollToCaret();
                return;
            }

            func.columns = columns;

            func.CreateEntity();
            func.CreateCriteria();
            func.CreateRepository();

            func.CreateFactory();

            //check if files successfully created
            if (!func.CheckFiles())
            {
                rtxtResults.AppendText("Creation of " + table_name + " Files Success!" + Environment.NewLine);
                rtxtResults.ScrollToCaret();
            }
        }

        #region directory settings and db connection settings
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings["connection"].Value = txtConnectionStrings.Text;
            configuration.AppSettings.Settings["path"].Value = txtPath.Text;
            configuration.AppSettings.Settings["libpath"].Value = txtLibPath.Text;
            configuration.Save();

            ConfigurationManager.RefreshSection("appSettings");

            rtxtResults.AppendText("Settings Updated" + Environment.NewLine);
            rtxtResults.ScrollToCaret();
        }

        private void btnBrowsePath_Click(object sender, EventArgs e)
        {
            DialogResult btnBrowsePath_Click = fbdPath.ShowDialog();
            if (btnBrowsePath_Click == DialogResult.OK)
            {
                txtPath.Text = fbdPath.SelectedPath;
            }
        }

        private void btnBrowseLibPath_Click(object sender, EventArgs e)
        {
            DialogResult btnBrowseLibPath_Click = fbdLibPath.ShowDialog();
            if (btnBrowseLibPath_Click == DialogResult.OK)
            {
                txtLibPath.Text = fbdLibPath.SelectedPath;
            }
        }
        #endregion
    }
}
