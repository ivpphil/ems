﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace ersEntityCreator
{
    class ersEntityCreatorFunctions
    {
        //where library folder is located
        public string libpath;

        //used for factory, folder name
        public string nameSpace;

        //used for criteria, repository, entity
        public string tableName;

        //for db table columns and used for entity, criteria
        public Dictionary<string, string> columns;

        #region special chars and string functions
        private string tab(int number)
        {
            string tabs = "";
            for (int i = 1; i <= number; i++)
            {
                tabs = tabs + "\t";
            }

            return tabs;
        }

        private string openCurly = "{";
        private string closeCurly = "}";
        private string newline = Environment.NewLine;

        //capital first char on a word
        private string UppercaseFirst(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }
            return char.ToUpper(word[0]) + word.Substring(1);
        }

        //convert table name to function name, 
        //removes t from string and converts capital start on each word
        private string functionName()
        {
            string name = this.tableName;
            string newname = String.Empty;

            string[] words = name.Split('_');
            foreach (string word in words)
            {
                if (word != "t")
                {
                    newname = newname + UppercaseFirst(word);
                }
            }

            return newname;
        }

        //convert namespace with underscore to capital start on each word
        private string UpdateNameSpace(string oldname)
        {
            string name = oldname;
            string newname = String.Empty;

            if (oldname.Contains('_'))
            {
                string[] words = name.Split('_');
                foreach (string word in words)
                {
                    newname = newname + UppercaseFirst(word);
                }
            }
            else
            {
                newname = UppercaseFirst(name);
            }

            return newname;
        }
        #endregion

        #region filepaths
        private string fullpath()
        {
            return Path.Combine(this.libpath, this.nameSpace);
        }

        private string entityPath()
        {
            return Path.Combine(fullpath(),"Ers" + functionName() + ".cs");
        }

        private string criteriaPath() 
        {
            return Path.Combine(fullpath(),"Ers" + functionName() + "Criteria.cs");
        }

        private string factoryPath()
        {
            return Path.Combine(fullpath(),"Ers" + UpdateNameSpace(this.nameSpace) + "Factory.cs");
        }

        private string repositoryPath()
        {
            return Path.Combine(fullpath(),"Ers" + functionName() + "Repository.cs");
        }
        #endregion

        #region identify column type
        public string GetTypeName(DataColumn dc)
        {
            string typeName;
            var type = dc.DataType;
            if (dc.DataType == typeof(string))
            {
                typeName = "string";
            }
            else if (dc.DataType == typeof(short))
            {
                typeName = "short?";
            }
            else if (dc.DataType == typeof(int))
            {
                typeName = "int?";
            }
            else if (dc.DataType == typeof(int[]))
            {
                typeName = "int[]";
            }
            else if (dc.DataType == typeof(long))
            {
                typeName = "long?";
            }
            else if (dc.DataType == typeof(DateTime))
            {
                typeName = "DateTime?";
            }
            else
            {
                typeName = dc.DataType.Name;
            }

            return typeName;
        }
        #endregion

        #region check files and folders
        public bool CheckFolder()
        {
            if (Directory.Exists(this.fullpath()))
            {
                return false;
            }
            return true;
        }

        public bool CheckFiles()
        {
            if (File.Exists(this.entityPath()) 
                || File.Exists(this.criteriaPath()) 
                || File.Exists(this.repositoryPath()))
            {
                return false;
            }
            return true;
        }
        #endregion

        #region folder and file generators
        public void CreateFolder()
        {
            Directory.CreateDirectory(fullpath());
        }

        public void CreateEntity()
        {
            string[] usingLines = { "using System;", 
                                    "using System.Collections.Generic;",
                                    "using System.Linq;",
                                    "using jp.co.ivp.ers;",
                                    "using jp.co.ivp.ers.mvc;" };

            string nameSpaceLine = newline + "namespace jp.co.ivp.ers." + this.nameSpace;

            string[] xmlTitle = {   "/// <summary>",
                                    "/// Holds values of " + this.functionName() + " record.",
                                    "/// Inherit from repository",
                                    "/// </summary>"};

            string title = "public class Ers" + this.functionName() + " : ErsRepositoryEntity";

            using (StreamWriter file = new StreamWriter(this.entityPath()))
            {
                foreach (string line in usingLines)
                {
                    file.WriteLine(line);
                }
                file.WriteLine(nameSpaceLine);
                file.WriteLine(openCurly);
                foreach (string line in xmlTitle)
                {
                    file.WriteLine(tab(1) + line);
                }
                file.WriteLine(tab(1) + title);
                file.WriteLine(tab(1) + openCurly);

                bool haveID = false;
                foreach (var pair in columns)
                {
                    if ((pair.Key == "id" || !columns.ContainsKey("id")) && !haveID)
                    {
                        if (!columns.ContainsKey("id"))
                        {
                            //writes the first column and id notimplemented exception
                            //if table has no id column
                            file.WriteLine(tab(2) + "public override int? id { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }");
                            file.WriteLine(tab(2) + "public " + pair.Value + " " + pair.Key + " { get; set; }");
                        }
                        else
                        {
                            //has id column 
                            //if id is not int it will be declared as int
                            file.WriteLine(tab(2) + "public override int? id { get; set; }");
                        }
                        haveID = true;
                    }
                    else
                    {
                        file.WriteLine(tab(2) + "public " + pair.Value + " " + pair.Key + " { get; set; }");
                    }

                }
                file.WriteLine(tab(1) + closeCurly);
                file.WriteLine(closeCurly);
            }
        }

        public void CreateCriteria()
        {
            string[] usingLines = { "using System;", 
                                    "using System.Collections.Generic;",
                                    "using System.Linq;", 
                                    "using System.Text;", 
                                    "using jp.co.ivp.ers.db;", 
                                    "using jp.co.ivp.ers.mvc;", 
                                    "using jp.co.ivp.ers.util;" };

            string nameSpaceLine = newline + "namespace jp.co.ivp.ers." + this.nameSpace;

            string[] xmlTitle =  {  "/// <summary>",
                                    "/// Represents the search condition of "+this.functionName(),
                                    "/// inherit from criteria",
                                    "/// </summary>"}; 

            string title = "public class Ers" + this.functionName() + "Criteria : Criteria";

            using (StreamWriter file = new StreamWriter(this.criteriaPath()))
            {
                foreach (string line in usingLines)
                {
                    file.WriteLine(line);
                }
                file.WriteLine(nameSpaceLine);
                file.WriteLine(openCurly);
                foreach (string line in xmlTitle)
                {
                    file.WriteLine(tab(1) + line);
                }
                file.WriteLine(tab(1) + title);
                file.WriteLine(tab(1) + openCurly);

                foreach(var pair in columns)
                {
                    string[] xmlFunction = {"/// <summary>",
                                            "/// search condition for "+pair.Key,
                                            "/// </summary> "};
                    foreach (string line in xmlFunction)
                    {
                        file.WriteLine(tab(2) + line);
                    }
                    file.WriteLine(tab(2) + "public "+pair.Value+" "+pair.Key);
                    file.WriteLine(tab(2) + openCurly);
                    file.WriteLine(tab(3) + "set");
                    file.WriteLine(tab(3) + openCurly);

                    if (pair.Key == "id")
                    {
                        file.WriteLine(tab(4) + "this.Add(Criteria.GetCriterion(\"" + tableName + "." + pair.Key + "\", value, Operation.EQUAL));");
                    }
                    else
                    {
                        file.WriteLine(tab(4) + "Add(Criteria.GetCriterion(\"" + tableName + "." + pair.Key + "\", value, Operation.EQUAL));");
                    } 
                    file.WriteLine(tab(3) + closeCurly);
                    file.WriteLine(tab(2) + closeCurly + newline);
                }
                file.WriteLine(tab(1) + closeCurly);
                file.WriteLine(closeCurly);
            }
        }

        public void CreateRepository()
        {
            string[] usingLines = { "using System;", 
                                    "using System.Collections.Generic;",
                                    "using System.Linq;",
                                    "using jp.co.ivp.ers;",
                                    "using jp.co.ivp.ers.db;", 
                                    "using jp.co.ivp.ers.mvc;"};

            string nameSpaceLine = newline + "namespace jp.co.ivp.ers." + this.nameSpace;

            string[] xmlTitle = {   "/// <summary>",
                                    "/// Provides methods to "+ this.functionName() +" with CRUD",
                                    "/// inherit from ErsRepository",
                                    "/// </summary>"};

            string title = "public class Ers" + this.functionName() + "Repository : ErsRepository<Ers" + this.functionName() + ">";

            string[] xmlDefFunc1 = {"/// <summary>",
                                    "/// コンストラクタ",
                                    "/// </summary>"};

            string defFunc1 = "public Ers" + this.functionName() + "Repository()" + newline + tab(3) + ": base(\"" + this.tableName + "\")";

            string[] xmlDefFunc2 = {"/// <summary>",
                                    "/// コンストラクタ(ユニットテスト用)",
                                    "/// </summary>"};

            string defFunc2 = "public Ers" + this.functionName() + "Repository(ErsDatabase objDB)" + newline + tab(3) + ": base(\"" + this.tableName + "\", objDB)";

            using (StreamWriter file = new StreamWriter(repositoryPath()))
            {
                foreach (string line in usingLines)
                {
                    file.WriteLine(line);
                }
                file.WriteLine(nameSpaceLine);
                file.WriteLine(openCurly);
                foreach (string line in xmlTitle)
                {
                    file.WriteLine(tab(1) + line);
                }
                file.WriteLine(tab(1) + title);
                file.WriteLine(tab(1) + openCurly);
                foreach (string line in xmlDefFunc1)
                {
                    file.WriteLine(tab(2) + line);
                }
                file.WriteLine(tab(2) + defFunc1);
                file.WriteLine(tab(2) + openCurly + " " + closeCurly + newline);
                foreach (string line in xmlDefFunc2)
                {
                    file.WriteLine(tab(2) + line);
                }
                file.WriteLine(tab(2) + defFunc2);
                file.WriteLine(tab(2) + openCurly + " " + closeCurly);
                file.WriteLine(tab(1) + closeCurly);
                file.WriteLine(closeCurly);
            }
        }

        public void CreateFactory()
        {
            //if file exists copy old lines
            List<string> oldLines = new List<string>();
            bool fileExists = false;

            if (File.Exists(factoryPath()))
            {
                fileExists = true;
                string[] existLines = File.ReadAllLines(factoryPath());
                int ctr = 0;
                bool checkedFunc = false;
                foreach (var line in existLines)
                {
                    if (line.Contains("{"))
                    {
                        if (!checkedFunc && ctr == 2)
                        {
                            checkedFunc = true;
                        }
                        ctr++;
                    }

                    if (line.Contains("}"))
                    {
                        if (checkedFunc && ctr == 2)
                        {
                            continue;
                        }
                        ctr--;
                    }

                    oldLines.Add(line);
                }
            }

            string[] usingLines = { "using System;", 
                                    "using System.Collections.Generic;",
                                    "using System.Linq;",
                                    "using jp.co.ivp.ers;",};

            string nameSpaceLine = newline + "namespace jp.co.ivp.ers." + this.nameSpace;

            string[] xmlTitle = {   "/// <summary>",
                                    "/// Provides 会員関連クラス用Factory",
                                    "/// </summary>"};

            string title = "public class Ers" + UpdateNameSpace(this.nameSpace) + "Factory";

            string[] xmlRepo = {newline + tab(2) + "/// <summary>",
                                "/// Get an instance of Ers" + this.functionName() + "Repository", 
                                "/// </summary>",
                                "/// <returns>Returns new instance of Ers" + this.functionName() + "Repository</returns>" }; 

            string repo = "public Ers" + this.functionName() + "Repository GetErs" + this.functionName() + "Repository()";
            string repoReturn = "return new Ers" + this.functionName() + "Repository();";

            string[] xmlCrit = {"/// <summary>",
                                "/// Get an instance of Ers" + this.functionName() + "Criteria",
                                "/// </summary>",
                                "/// <returns>Returns new instance of Ers" + this.functionName() + "Criteria</returns>" }; 

            string crit = "public Ers" + this.functionName() + "Criteria GetErs" + this.functionName() + "Criteria()";
            string critReturn = "return new Ers" + this.functionName() + "Criteria();";

            string[] xmlEnt = { "/// <summary>",
                                "/// Get an instance of Ers" + this.functionName(),
                                "/// </summary>",
                                "/// <returns>Returns new instance of Ers" + this.functionName() + "</returns>" }; 

            string ent = "public Ers" + this.functionName() + " GetErs" + this.functionName() + "()";
            string entReturn = "return new Ers" + this.functionName() + "();";

            string[] xmlSearchID = {"/// <summary>",
                                    "/// Search " + this.tableName + " with records id",
                                    "/// </summary>",
                                    "/// <returns>Returns single result from repository</returns>"  };

            //if (columns.ContainsKey("id"))
            //{
            //    string searchID = "public Ers" + this.functionName() + " GetErs" + this.functionName() + "ById("+columns["id"]+" id)";
            //    string[] searchBody = { 
            //        "var repo = this.GetErs" + this.functionName() + "Repository();",
            //        "var cri = this.GetErs" + this.functionName() + "Criteria();",
            //        "cri.id = id;"+newline,
            //        "if (repo.GetRecordCount(cri) != 1)",
            //        openCurly,
            //        tab(1) + "return null;",
            //        closeCurly+newline,
            //        "return repo.FindSingle(cri);"
            //    };
            //}

            using (StreamWriter file = new StreamWriter(factoryPath()))
            {
                if (fileExists)
                {
                    foreach (var line in oldLines)
                    {
                        file.WriteLine(line);
                    }
                }
                else
                {
                    foreach (string line in usingLines)
                    {
                        file.WriteLine(line);
                    }

                    file.WriteLine(nameSpaceLine);
                    file.WriteLine(openCurly);

                    foreach (string line in xmlTitle)
                    {
                        file.WriteLine(tab(1) + line);
                    }

                    file.WriteLine(tab(1) + title);
                    file.WriteLine(tab(1) + openCurly);
                }
                foreach (string line in xmlRepo)
                {
                    file.WriteLine(tab(2) + line);
                }
                file.WriteLine(tab(2) + repo);
                file.WriteLine(tab(2) + openCurly);
                file.WriteLine(tab(3) + repoReturn);
                file.WriteLine(tab(2) + closeCurly + newline);
                foreach (string line in xmlCrit)
                {
                    file.WriteLine(tab(2) + line);
                }
                file.WriteLine(tab(2) + crit);
                file.WriteLine(tab(2) + openCurly);
                file.WriteLine(tab(3) + critReturn);
                file.WriteLine(tab(2) + closeCurly + newline);
                foreach (string line in xmlEnt)
                {
                    file.WriteLine(tab(2) + line);
                }
                file.WriteLine(tab(2) + ent);
                file.WriteLine(tab(2) + openCurly);
                file.WriteLine(tab(3) + entReturn);
                //file.WriteLine(tab(2) + closeCurly + newline);
                //foreach (string line in xmlSearchID)
                //{
                //    file.WriteLine(tab(2) + line);
                //}
                //if (columns.ContainsKey("id"))
                //{
                //    file.WriteLine(tab(2) + searchID);
                //    file.WriteLine(tab(2) + openCurly);
                //    foreach (string line in searchBody)
                //    {
                //        file.WriteLine(tab(3) + line);
                //    }
                //}
                file.WriteLine(tab(2) + closeCurly);
                file.WriteLine(tab(1) + closeCurly);
                file.WriteLine(closeCurly);
            }
        }
        #endregion
    }
}
