﻿using ErsDeploy.Lib;
using ErsDeploy.Lib.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy
{
    /// <summary>
    /// The class executes manifest to deploy one ERS site.
    /// </summary>
    public class ErsManifestExecutor
    {
        public ErsManifestExecutor(DeploymentOptions options)
        {
            this.options = options;
        }

        DeploymentOptions options { get; set; }

        /// <summary>
        /// Deploy one ERS site asynchronously
        /// </summary>
        /// <param name="computer"></param>
        /// <returns></returns>
        public async Task DeployToAsync(string computer)
        {
            await Task.Run(() => ExecuteManifestTo(computer));
        }

        /// <summary>
        /// 同期的に実行
        /// </summary>
        /// <param name="computer"></param>
        private void ExecuteManifestTo(string computer)
        {
            var sb = new StringBuilder();
            sb.AppendLine("#######################################");
            sb.AppendLine(computer);

            foreach(var provider in GetProvidersBy(computer, options))
            {
                sb.AppendLine($"-------------- {provider.ProviderName} --------------");
                sb.AppendLine();

                var deploymentResult = provider.Sync();

                sb.AppendLine(string.Join(Environment.NewLine + Environment.NewLine, deploymentResult));
                sb.AppendLine();
            }
            
            sb.AppendLine();

            Console.WriteLine(sb.ToString());
        }

        private IDeploymentProvider[] GetProvidersBy(string computerName, DeploymentOptions options)
        {
            return new IDeploymentProvider[]{
                new DirectoryDeployment(computerName, options),
                new FileDeployment(computerName, options),
                new WebDeployment(computerName, options)
            };
        }
    }
}
