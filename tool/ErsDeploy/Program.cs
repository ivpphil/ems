﻿using ErsDeploy.Lib;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = CreateOptions();

            ValidateArguments(options);

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, err) =>
            {
                return true;
            };

            Task.WaitAll(options.DestComputerNames.Select(c => new ErsManifestExecutor(options).DeployToAsync(c)).ToArray());
        }

        private static DeploymentOptions CreateOptions()
        {
            var configurationName = Environment.GetEnvironmentVariable("Configuration");

            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.GetFullPath("../../Config"))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{configurationName}.json", optional: true)
                .AddEnvironmentVariables();

            var configuration = builder.Build();

            var options = new DeploymentOptions();
            configuration.Bind(options);

            return options;
        }

        private static void ValidateArguments(DeploymentOptions options)
        {
            if (string.IsNullOrEmpty(options.SourceRoot))
            {
                throw new ArgumentNullException(nameof(options.SourceRoot));
            }

            if (string.IsNullOrEmpty(options.DestRoot))
            {
                throw new ArgumentNullException(nameof(options.DestRoot));
            }
        }

    }
}
