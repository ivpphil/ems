﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy.Lib
{
    public class SyncDirectory
    {
        public string Path { get; set; }

        public bool DoNotDeleteRule { get; set; }

        public string[] Rules { get; set; }
    }
}
