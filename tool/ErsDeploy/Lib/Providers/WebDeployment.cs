﻿using ErsDeploy.Lib;
using Microsoft.Web.Deployment;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy.Lib.Providers
{
    public class WebDeployment : IDeploymentProvider
    {
        public WebDeployment(string computerName, DeploymentOptions options)
        {
            this.deploymentBaseOptions = DeploymentFactory.CreateDeploymentBaseOptions(computerName, options);
            this.options = options;
        }

        private DeploymentBaseOptions deploymentBaseOptions { get; set; }

        private DeploymentOptions options { get; set; }

        public string ProviderName { get; } = "Web";

        public List<string> Sync()
        {
            var resultList = new List<string>();

            foreach (var site in options.WebSites)
            {
                foreach(var proj in site.WebProjects)
                {
                    var sourceBaseOptions = this.CreateSourceBaseOptions(proj);

                    var configurationName = options.Configuration;

                    var packagePath = Path.GetFullPath(Path.Combine(
                            options.SourceRoot,
                            "ers",
                            proj.SiteDirName,
                            "obj",
                            configurationName,
                            "Package",
                            proj.SiteDirName + ".zip"
                        ));

                    var webDeploymentObject = DeploymentManager.CreateObject(
                                   DeploymentWellKnownProvider.Package,
                                   packagePath,
                                   sourceBaseOptions);

                    var sitePath = $"{site.RootSitePrefix}-{configurationName.ToLower()}/{proj.SitePath}";

                    webDeploymentObject
                        .SyncParameters
                        .Single(p => p.Name == "IIS Web Application Name")
                        .Value = sitePath;

                    var webSyncOptions = new DeploymentSyncOptions();

                    var deploymentBaseOptions = this.CreateDeploymentBaseOptions(proj);

                    //-verb:sync
                    var webChanges =
                        webDeploymentObject.SyncTo(DeploymentWellKnownProvider.Auto,
                                                "", deploymentBaseOptions, webSyncOptions);

                    resultList.Add(webChanges.FormatBy(sitePath));
                }
            }

            return resultList;
        }

        private DeploymentBaseOptions CreateSourceBaseOptions(WebProject proj)
        {
            var sourceBaseOptions = new DeploymentBaseOptions();

            //-disableLink
            foreach (var extension in sourceBaseOptions.LinkExtensions
                .Where(ext => ext.Name == "AppPoolExtension"
                            || ext.Name == "ContentExtension"
                            || ext.Name == "CertificateExtension"))
            {
                extension.Enabled = false;
            }

            if(proj.SkipDirectories != null)
            {
                foreach (var skipDir in proj.SkipDirectories)
                {
                    sourceBaseOptions.SkipDirectory(skipDir);
                }
            }
            
            if(proj.SkipFiles != null)
            {
                foreach (var skipFile in proj.SkipFiles)
                {
                    sourceBaseOptions.SkipFile(skipFile);
                }
            }
            
            sourceBaseOptions.IncludeAcls = false;

            return sourceBaseOptions;
        }

        private DeploymentBaseOptions CreateDeploymentBaseOptions(WebProject proj)
        {
            foreach (var extension in this.deploymentBaseOptions.LinkExtensions
                .Where(ext => ext.Name == "AppPoolExtension"
                            || ext.Name == "ContentExtension"
                            || ext.Name == "CertificateExtension"))
            {
                extension.Enabled = false;
            }

            if (proj.SkipDirectories != null)
            {
                foreach(var skipDir in proj.SkipDirectories)
                {
                    this.deploymentBaseOptions.SkipDirectory(skipDir);
                }
            }

            if (proj.SkipFiles != null)
            {
                foreach (var skipFile in proj.SkipFiles)
                {
                    this.deploymentBaseOptions.SkipFile(skipFile);
                }
            }

            return this.deploymentBaseOptions;
        }

    }
}
