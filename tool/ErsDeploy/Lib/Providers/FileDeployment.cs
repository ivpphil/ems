﻿using ErsDeploy.Lib;
using Microsoft.Web.Deployment;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy.Lib.Providers
{
    public class FileDeployment : IDeploymentProvider
    {
        public FileDeployment(string computerName, DeploymentOptions options)
        {
            this.baseOptions = DeploymentFactory.CreateDeploymentBaseOptions(computerName, options);
            this.options = options;
        }

        private DeploymentBaseOptions baseOptions { get; set; }

        public string ProviderName { get; } = "File";

        private DeploymentOptions options { get; set; }

        public List<string> Sync()
        {
            var resultList = new List<string>();

            if (options.SyncFiles == null)
            {
                return resultList;
            }

            foreach (var filePath in options.SyncFiles)
            {
                var deploymentObject = DeploymentManager.CreateObject(
                                        DeploymentWellKnownProvider.FilePath,
                                        Path.GetFullPath(Path.Combine(options.SourceRoot, filePath)));

                var syncOptions = new DeploymentSyncOptions();

                var changes = deploymentObject.SyncTo(DeploymentWellKnownProvider.FilePath,
                                                       Path.GetFullPath(Path.Combine(options.DestRoot, filePath)),
                                                        baseOptions, syncOptions);

                resultList.Add(changes.FormatBy(filePath));
            }

            return resultList;
        }

    }
}
