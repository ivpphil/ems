﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy.Lib.Providers
{
    public interface IDeploymentProvider
    {
        string ProviderName { get; }
        List<string> Sync();
    }
}
