﻿using ErsDeploy.Lib;
using Microsoft.Extensions.Configuration;
using Microsoft.Web.Deployment;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy.Lib.Providers
{
    public class DirectoryDeployment : IDeploymentProvider
    {
        public DirectoryDeployment(string computerName, DeploymentOptions options)
        {
            baseOptions = DeploymentFactory.CreateDeploymentBaseOptions(computerName, options);
            this.options = options;
        }

        public string ProviderName { get; } = "Directory";

        private DeploymentBaseOptions baseOptions { get; set; }

        private DeploymentOptions options { get; set; }

        public List<string> Sync()
        {
            var resultList = new List<string>();

            foreach (var directory in options.SyncDirectories)
            {
                var deploymentObject = DeploymentManager.CreateObject(
                                        DeploymentWellKnownProvider.DirPath,
                                        Path.GetFullPath(Path.Combine(options.SourceRoot, directory.Path)));

                var syncOptions = new DeploymentSyncOptions();
                syncOptions.DoNotDelete = directory.DoNotDeleteRule;

                if(directory.Rules != null)
                {
                    var rules = DeploymentSyncOptions.GetAvailableRules().Where(r => directory.Rules.Contains(r.Name));

                    foreach (var rule in rules)
                    {
                        syncOptions.Rules.Add(rule);
                    }
                }

                var changes = deploymentObject.SyncTo(DeploymentWellKnownProvider.DirPath,
                                                        Path.GetFullPath(Path.Combine(options.DestRoot, directory.Path)),
                                                        baseOptions, syncOptions);

                resultList.Add(changes.FormatBy(directory.Path));
            }

            return resultList;
        }
    }
}
