﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy.Lib
{
    public class WebProject
    {
        public string SitePath { get; set; }

        public string SiteDirName { get; set; }

        public string[] SkipDirectories { get; set; }

        public string[] SkipFiles { get; set; }
    }
}
