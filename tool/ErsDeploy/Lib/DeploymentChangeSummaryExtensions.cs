﻿using Microsoft.Web.Deployment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy.Lib
{
    public static class DeploymentChangeSummaryExtensions
    {
        public static string FormatBy(this DeploymentChangeSummary changes, string path)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("Path:              {0}", path);
            sb.AppendLine();
            sb.AppendFormat("BytesCopied:       {0}", changes.BytesCopied.ToString());
            sb.AppendLine();
            sb.AppendFormat("Added:             {0}", changes.ObjectsAdded.ToString());
            sb.AppendLine();
            sb.AppendFormat("Updated:           {0}", changes.ObjectsUpdated.ToString());
            sb.AppendLine();
            sb.AppendFormat("Deleted:           {0}", changes.ObjectsDeleted.ToString());
            sb.AppendLine();
            sb.AppendFormat("Errors:            {0}", changes.Errors.ToString());
            sb.AppendLine();
            sb.AppendFormat("Warnings:          {0}", changes.Warnings.ToString());
            sb.AppendLine();
            sb.AppendFormat("ParametersChanged: {0}", changes.ParameterChanges.ToString());
            sb.AppendLine();
            sb.AppendFormat("TotalChanges:      {0}", changes.TotalChanges.ToString());

            return sb.ToString();
        }
    }
}
