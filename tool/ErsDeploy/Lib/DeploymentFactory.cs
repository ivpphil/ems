﻿using Microsoft.Web.Deployment;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy.Lib
{
    public class DeploymentFactory
    {
        public static DeploymentBaseOptions CreateDeploymentBaseOptions(string computerName, DeploymentOptions options)
        {
            //デプロイ先の設定
            //-dest:auto,computerName
            //="http://exsample.com/MSDeployAgentService"
            //,userName="nazo",password="pAssw0rd",includeAcls="False" 
            var baseOption = new DeploymentBaseOptions()
            {
                ComputerName = computerName,
                UserName = options.AuthUserName,
                Password = options.AuthPassword,
                IncludeAcls = false
            };

            if (!string.IsNullOrEmpty(baseOption.UserName) && !string.IsNullOrEmpty(baseOption.Password))
            {
                baseOption.AuthenticationType = "Basic";
            }

            baseOption.Trace += (sender, e) =>
                {
                    Console.WriteLine(e.Message);
                };

            baseOption.TraceLevel = (TraceLevel)Enum.Parse(typeof(TraceLevel), options.LogTraceLevel);

            return baseOption;
        }
    }
}
