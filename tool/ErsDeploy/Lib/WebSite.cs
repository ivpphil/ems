﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy.Lib
{
    public class WebSite
    {
        public string RootSitePrefix { get; set; }

        public List<WebProject> WebProjects { get; set; }
    }
}
