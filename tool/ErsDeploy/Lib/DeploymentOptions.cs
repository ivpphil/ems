﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErsDeploy.Lib
{
    public class DeploymentOptions
    {
        public string Configuration { get; set; }

        public List<WebSite> WebSites { get; set; }

        public List<string> SyncFiles { get; set; }

        public List<SyncDirectory> SyncDirectories { get; set; }

        public string SourceRoot { get; set; }

        public string DestRoot { get; set; }

        public List<string> DestComputerNames { get; set; }

        public string AuthUserName { get; set; }

        public string AuthPassword { get; set; }

        public string LogTraceLevel { get; set; }
    }
}
