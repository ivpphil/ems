-- BATCH SCHEDULE
UPDATE batch_schedule_t SET active = 0;
DELETE FROM batch_schedule_t WHERE batch_id = 'UploadEmpSchedule';
INSERT INTO public.batch_schedule_t(
             batch_id, batch_name, schedule, intime, active,  site_id)        
VALUES ('UploadEmpSchedule', 'UploadEmpSchedule', '0 6/6 * * *', current_timestamp, 1, 0);
            
-- BATCH SEQUENCE
UPDATE batch_sequence_t SET active = 0;
DELETE FROM batch_sequence_t WHERE batch_id = 'UploadEmpSchedule';
INSERT INTO public.batch_sequence_t(
             batch_id, execute_order, class_name,  intime,    active, site_id)        
VALUES ('UploadEmpSchedule', 1, 'jp.co.ivp.ers.batch.UploadEmpSchedule.UploadEmpScheduleFile', current_timestamp, 1, 0);
            
