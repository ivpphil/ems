CREATE TABLE public.schedule_t
(
  id serial NOT NULL, -- CHK_TYPE_Numeric
  desknet_id int NOT NULL, -- CHK_TYPE_Numeric
  date_start date, -- range to CHK_TYPE_Date
  date_end date, -- range to CHK_TYPE_Date
  time_start time without time zone,
  time_end time without time zone,
  sched_type integer,
  sched_details text,
  sched_location text,
  sched_location_details text,
  details text,
  CONSTRAINT schedule_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.schedule_t
  OWNER TO postgres;
COMMENT ON COLUMN public.schedule_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.schedule_t.desknet_id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.schedule_t.date_start IS 'range to CHK_TYPE_Date';
COMMENT ON COLUMN public.schedule_t.date_end IS 'range to CHK_TYPE_Date';

