DELETE FROM common_namecode_t WHERE type_code = 'SchedType';
INSERT INTO public.common_namecode_t(
            type_code, code, namename, disp_order, intime, active, 
            site_id)
    VALUES ('SchedType',0,'--',1,current_timestamp,1,0),
	   ('SchedType',1,'会議',1,current_timestamp,1,0),
	   ('SchedType',2,'休み',2,current_timestamp,1,0),
           ('SchedType',3,'往訪',3,current_timestamp,1,0),
           ('SchedType',4,'作業',4,current_timestamp,1,0),
           ('SchedType',5,'出張',5,current_timestamp,1,0),
           ('SchedType',6,'その他',6,current_timestamp,1,0),
           ('SchedType',7,'Work Schedule',7,current_timestamp,1,0),
           ('SchedType',8,'Vacation Leave',8,current_timestamp,1,0),
           ('SchedType',9,'Sick Leave',9,current_timestamp,1,0),
           ('SchedType',10,'Emergency Leave',10,current_timestamp,1,0);  