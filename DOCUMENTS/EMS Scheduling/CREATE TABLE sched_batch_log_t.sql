CREATE TABLE public.sched_batch_log_t
(
  id serial NOT NULL, -- CHK_TYPE_Numeric
  emp_no character varying(10), -- range to 10 CHK_TYPE_NumericString
  execution_date date NOT NULL DEFAULT now(), -- CHK_TYPE_Date
  execution_time timestamp with time zone NOT NULL DEFAULT now(), -- CHK_TYPE_Date
  details text,
  error_details text,
  error_exception text,
  utime timestamp with time zone, -- CHK_TYPE_Date
  status int NOT NULL, -- range to CHK_TYPE_All
  CONSTRAINT sched_batch_log_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.sched_batch_log_t
  OWNER TO postgres;
COMMENT ON COLUMN public.sched_batch_log_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.sched_batch_log_t.emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN public.sched_batch_log_t.details IS 'range to CHK_TYPE_All';
COMMENT ON COLUMN public.sched_batch_log_t.status IS 'range to CHK_TYPE_Numeric';