DELETE FROM common_namecode_t WHERE type_code = 'BatchStatus';
INSERT INTO public.common_namecode_t(
            type_code, code, namename, disp_order, intime, active, 
            site_id)
    VALUES ('BatchStatus',0,'Success',1,current_timestamp,1,0),
		   ('BatchStatus',1,'Executed',1,current_timestamp,1,0),
		   ('BatchStatus',2,'Error',2,current_timestamp,1,0);
           