﻿Please enter date of modification and description before the query
Sample:

--5/5/2017 update member t
--UPDATE lodging_facility_t SET *******;

--5/5/2017 added common name code supervisor names
INSERT INTO common_namecode_t (type_code,code,namename,disp_order,intime,active,site_id)
values ('Supervisor',1,'MICHAEL SAMSON',1,CURRENT_TIMESTAMP,1,0 ),('Supervisor',2,'LAWRENCE CONSTANTINO',2,CURRENT_TIMESTAMP,1,0 );

--5/15/2017 added announcement_t for announcement module
	CREATE TABLE announcement_t
	(
	  id serial,
	  emp_no character varying(10) NOT NULL,
	  fullname character varying(70) NOT NULL,
	  news text,
	  in_time timestamp with time zone NOT NULL DEFAULT now()
	)
	WITH (
	  OIDS=FALSE
	);
	ALTER TABLE announcement_t
	  OWNER TO postgres;

--5/8/2017 Added validation in employee_t table
COMMENT ON COLUMN public.employee_t.emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN public.employee_t.lname IS 'range to 100 CHK_TYPE_All';
COMMENT ON COLUMN public.employee_t.fname IS 'range to 100 CHK_TYPE_All';
COMMENT ON COLUMN public.employee_t.address IS 'range to 255 CHK_TYPE_All';
COMMENT ON COLUMN public.employee_t.password IS 'range 6 to 10 CHK_TYPE_HalfAlphabetOrNumber';
COMMENT ON COLUMN public.employee_t.email IS 'range to 100 CHK_TYPE_EMailAddr';
COMMENT ON COLUMN public.employee_t.position IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.employee_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.employee_t.contact_no IS 'range 5 to 15 CHK_TYPE_HyphenNumber';
COMMENT ON COLUMN public.employee_t.position IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.employee_t.team IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.employee_t.status IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.employee_t.mcode IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN public.employee_t.gender IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.employee_t.birthday IS 'CHK_TYPE_Date';

-- Table: public.emp_pos_t

-- DROP TABLE public.emp_pos_t;

CREATE TABLE public.emp_pos_t
(
  id serial,
  pos_name character varying(250) NOT NULL,
  in_time timestamp with time zone NOT NULL DEFAULT now(),
  CONSTRAINT emp_pos_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.emp_pos_t
  OWNER TO postgres;

-- Table: public.emp_team_t

-- DROP TABLE public.emp_team_t;

CREATE TABLE public.emp_team_t
(
  id serial,
  team_name character varying(255) NOT NULL,
  in_time timestamp with time zone NOT NULL DEFAULT now(),
  CONSTRAINT emp_team_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.emp_team_t
  OWNER TO postgres;


-- Table: public.employee_t

-- DROP TABLE public.employee_t;

CREATE TABLE public.employee_t
(
  id serial, -- CHK_TYPE_Numeric
  emp_no character(10) NOT NULL, -- range to 10 CHK_TYPE_NumericString
  lname character varying(100) NOT NULL, -- range to 100 CHK_TYPE_All
  fname character varying(100) NOT NULL, -- range to 100 CHK_TYPE_All
  birthday date NOT NULL, -- CHK_TYPE_Date
  address character(255), -- range to 255 CHK_TYPE_All
  contact_no character varying(100), -- range 5 to 15 CHK_TYPE_HyphenNumber
  "position" numeric, -- CHK_TYPE_Numeric
  team numeric, -- CHK_TYPE_Numeric
  status numeric, -- CHK_TYPE_Numeric
  password character(10) NOT NULL, -- range 6 to 10 CHK_TYPE_HalfAlphabetOrNumber
  email character varying(255), -- range to 100 CHK_TYPE_EMailAddr
  mcode character varying(10), -- range to 10 CHK_TYPE_NumericString
  gender numeric, -- CHK_TYPE_Numeric
  CONSTRAINT employee_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.employee_t
  OWNER TO postgres;
COMMENT ON COLUMN public.employee_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.employee_t.emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN public.employee_t.lname IS 'range to 100 CHK_TYPE_All';
COMMENT ON COLUMN public.employee_t.fname IS 'range to 100 CHK_TYPE_All';
COMMENT ON COLUMN public.employee_t.birthday IS 'CHK_TYPE_Date';
COMMENT ON COLUMN public.employee_t.address IS 'range to 255 CHK_TYPE_All';
COMMENT ON COLUMN public.employee_t.contact_no IS 'range 5 to 15 CHK_TYPE_HyphenNumber';
COMMENT ON COLUMN public.employee_t."position" IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.employee_t.team IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.employee_t.status IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.employee_t.password IS 'range 6 to 10 CHK_TYPE_HalfAlphabetOrNumber';
COMMENT ON COLUMN public.employee_t.email IS 'range to 100 CHK_TYPE_EMailAddr';
COMMENT ON COLUMN public.employee_t.mcode IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN public.employee_t.gender IS 'CHK_TYPE_Numeric';

-- Table: public.pcode_t

-- DROP TABLE public.pcode_t;

CREATE TABLE public.pcode_t
(
  id serial,
  pcode character varying(20) NOT NULL,
  pcode_name character varying(255),
  pcode_desc character varying(255),
  in_time timestamp with time zone DEFAULT now(),
  CONSTRAINT pcode_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.pcode_t
  OWNER TO postgres;


-- Table: public.dailyreport_details_t

-- DROP TABLE public.dailyreport_details_t;

CREATE TABLE public.dailyreport_details_t
(
  id serial,
  emp_no character varying(10) NOT NULL,
  report_code character varying(255) NOT NULL,
  pcode character varying(50) NOT NULL,
  proj_desc character varying(100),
  ref_no character varying(20),
  progress numeric,
  summary character varying(255),
  start_date date,
  due_date date,
  report_date date NOT NULL,
  um_hours numeric,
  status numeric,
  in_time time with time zone DEFAULT now(),
  CONSTRAINT dailyreport_details_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.dailyreport_details_t
  OWNER TO postgres;


--5/10/2017
COMMENT ON COLUMN public.dailyreport_details_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.dailyreport_details_t.emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN public.dailyreport_details_t.report_code IS 'range to 20 CHK_TYPE_All';
COMMENT ON COLUMN public.dailyreport_details_t.pcode IS 'range to 30 CHK_TYPE_All';
COMMENT ON COLUMN public.dailyreport_details_t.proj_desc IS 'range to 100 CHK_TYPE_All';
COMMENT ON COLUMN public.dailyreport_details_t.ref_no IS 'range to 20 CHK_TYPE_All';
COMMENT ON COLUMN public.dailyreport_details_t.progress IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.dailyreport_details_t.summary IS 'range to 255 CHK_TYPE_All';
COMMENT ON COLUMN public.dailyreport_details_t.start_date IS 'CHK_TYPE_Date';
COMMENT ON COLUMN public.dailyreport_details_t.due_date IS 'CHK_TYPE_Date';
COMMENT ON COLUMN public.dailyreport_details_t.report_date IS 'CHK_TYPE_Date';
COMMENT ON COLUMN public.dailyreport_details_t.um_hours IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.dailyreport_details_t.in_time IS 'CHK_TYPE_Date';
COMMENT ON COLUMN public.dailyreport_details_t.status IS 'range 0 to 3 CHK_TYPE_Numeric';


--5/11/2017

--For Messaging feautre

-- Table: emp_message_t

-- DROP TABLE emp_message_t;

CREATE TABLE emp_message_t
(
  id serial NOT NULL,
  emp_no character(10),
  subject character varying(255),
  message character varying(255),
  intime timestamp with time zone NOT NULL DEFAULT now()
)
WITH (
  OIDS=FALSE
);
ALTER TABLE emp_message_t
  OWNER TO postgres;
 
--5/15/2017 for position,team,emp_status,

INSERT INTO common_namecode_t(type_code,code,namename,disp_order,active) VALUES
('position',1,'Member',0,1), 
('position',2,'Team Leader',0,1),
('position',3,'Admin',0,1),
('Team',1,'C#',0,1),
('Team',2,'HTML',0,1),
('Team',3,'Administrative',0,1),
('Emp_Status',1,'Employed',0,1),
('Emp_Status',2,'Resigned',0,1),
('Emp_Status',3,'Terminated',0,1),
('Project_status',1,'On Project',0,1),
('Project_status',2,'Available',0,1);

--5/15/2017 for altering birthday column 
ALTER TABLE employee_t ALTER COLUMN birthday DROP NOT NULL;

--5/15/2017 for validation of status of employee
ALTER TABLE employee_t ALTER COLUMN status set not null;
ALTER TABLE employee_t ALTER COLUMN status set default 1;
COMMENT ON COLUMN employee_t.status IS 'range 0 to 3 CHK_TYPE_Numeric';



--5/15/2017 for roles_ems_t
CREATE TABLE role_ems_t
(
  id serial NOT NULL, -- CHK_TYPE_Numeric
  role_poscode character varying(20), -- range to 10 CHK_TYPE_OneByteCharacter
  role_posname character varying(20), -- range to 20 CHK_TYPE_All
  role_action character varying(40)[], -- CHK_TYPE_OneByteCharacterArray
  intime timestamp with time zone, -- CHK_TYPE_Date
  utime timestamp with time zone, -- CHK_TYPE_Date
  active smallint NOT NULL DEFAULT 1, -- range to 1 CHK_TYPE_Numeric
  site_id integer NOT NULL DEFAULT 0, -- CHK_TYPE_Numeric
  CONSTRAINT role_ems_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE role_ems_t
  OWNER TO postgres;
GRANT ALL ON TABLE role_group_t TO ers_v7_dev_sec;
COMMENT ON COLUMN role_ems_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN role_ems_t.role_poscode IS 'range to 10 CHK_TYPE_OneByteCharacter';
COMMENT ON COLUMN role_ems_t.role_posname IS 'range to 20 CHK_TYPE_All';
COMMENT ON COLUMN role_ems_t.role_action IS 'CHK_TYPE_OneByteCharacterArray';
COMMENT ON COLUMN role_ems_t.intime IS 'CHK_TYPE_Date';
COMMENT ON COLUMN role_ems_t.utime IS 'CHK_TYPE_Date';
COMMENT ON COLUMN role_ems_t.active IS 'range to 1 CHK_TYPE_Numeric';
COMMENT ON COLUMN role_ems_t.site_id IS 'CHK_TYPE_Numeric';

---------------------5/15/2017 for roles_ems_t data


Insert into role_ems_t (role_poscode,role_posname,active)values(1,'software_engineer',1),
(2,'team_leader',1),
(3,'admin',1);


UPDATE role_ems_t set role_action = role_action || '{emp_home,
AddAnnouncement,
ModifyAnnouncement,
emp_list,
emp_details,
emp_edit,
emp_edit_confirm,
emp_edit_complete,
emp_deactivate,
emp_deactivate_complete,
emp_search,
emp_profile,
emp_inbox,
emp_message,
emp_new_message,
pcode_regist,
pcode_regist_confirm,
pcode_regist_complete,
pcode_list,
pcode_delete,
pcode_delete_complete,
dreport_pcode_list,
emp_regist,
emp_regist_confirm,
emp_regist_complete,
dreport_edit,
dreport_edit_confirm,
dreport_edit_complete,
dreport_details,
dreport_add,
dreport_confirm,
dreport_complete,
drelport_search,
dreport_search_result,
download_csv_all,
download_csv_page,
download_csv_dateRange}'
WHERE role_poscode = '3' ;

UPDATE role_ems_t set role_action = role_action || '{emp_home,
AddAnnouncement,
ModifyAnnouncement,
emp_list,
emp_details,
emp_edit,
emp_edit_confirm,
emp_edit_complete,
emp_deactivate,
emp_deactivate_complete,
emp_search,
emp_profile,
emp_inbox,
emp_message,
emp_new_message,
pcode_regist,
pcode_regist_confirm,
pcode_regist_complete,
pcode_list,
pcode_delete,
pcode_delete_complete,
dreport_pcode_list,
emp_regist,
emp_regist_confirm,
emp_regist_complete,
dreport_edit,
dreport_edit_confirm,
dreport_edit_complete,
dreport_details,
dreport_add,
dreport_confirm,
dreport_complete,
dreport_search,
dreport_search_result,
download_csv_all,
download_csv_page,
download_csv_dateRange,
team_member_list,
remove_team_member,
none_member_list,
add_to_team}'
WHERE role_poscode = '2' ;

UPDATE role_ems_t set role_action = '{emp_home,
emp_edit,
emp_edit_confirm,
emp_edit_complete,
emp_profile,
emp_inbox,
emp_message,
emp_new_message,
dreport_pcode_list,
dreport_edit,
dreport_edit_confirm,
dreport_edit_complete,
dreport_details,
dreport_add,
dreport_confirm,
dreport_complete,
dreport_search,
dreport_search_result,
download_csv_all,
download_csv_page}'
WHERE role_poscode = '1' ;

--5/16/2017 added column in announcement_t
ALTER TABLE announcement_t
ADD u_time timestamp,
ADD status numeric NOT NULL DEFAULT '1';

--5/17/2017 alter role_poscode data type 
alter table role_ems_t alter column role_poscode set data type integer 
using 
role_poscode::integer 


--05/19/2017 added column in emp_message_t to display sender name
ALTER TABLE emp_message_t
ADD sender_emp_no character(10);

--05/23/2017 added column image_file in employee_t for the display image
ALTER TABLE employee_t ADD COLUMN image_file character(500);
COMMENT ON COLUMN public.employee_t.image_file IS 'range to 500 CHK_TYPE.All';

--05/24/2017 added column team_leader in employee_t for the teams
ALTER TABLE employee_t ADD COLUMN team_leader character varying(10);
COMMENT ON COLUMN public.employee_t.team_leader IS 'range to 10 CHK_TYPE_NumericString';

--05/24/2017 drop column fullname in announcement_t 
ALTER TABLE announcement_t
DROP COLUMN fullname;

--05/24/2017 alter table employee_t alter column password
ALTER TABLE employee_t ALTER COLUMN password type character varying (10);


--05/29/2017 insert pass_forget in mail_template_t
 
 INSERT INTO mail_template_t(
            key, value, utime, site_id)
    VALUES ('passforget_sendtype', '40', current_timestamp, 1);

	
	 INSERT INTO mail_template_t(
            key, value, utime, site_id)
    VALUES ('passforget_pc_title', 'IVP EMS Lost Password Request', current_timestamp, 1);

	 INSERT INTO mail_template_t(
            key, value, utime, site_id)
    VALUES ('passforget_pc_body', '
Dear <%=.Model.fname%> <%=.Model.lname%>,


You have recently asked for your Password reset

For security reasons, we do not send password by email. instead, please click on the link below to reset your password.
<%=.Model.changeUrl%>
(Note: This link will expire in 1 hour, please reset as soon as possible. if you are unable to click the link, please copy and paste it to your internet browser.)


If you did not request for Password recovery please ignore this email.

Regards,
IVP Global ph.
https://ww2.ivp.co.jp/english/', current_timestamp, 1);


--05/30/2017 Added Comments on pcode_t
COMMENT ON COLUMN public.pcode_t.pcode IS 'range to 20 CHK_TYPE_All';
COMMENT ON COLUMN public.pcode_t.pcode_name IS 'range to 255 CHK_TYPE_All';
COMMENT ON COLUMN public.pcode_t.pcode_desc IS 'range to 255 CHK_TYPE_All';


--06/01/2017 alter in_time column of dailyreport_details_t
alter table dailyreport_details_t drop column in_time;

alter table dailyreport_details_t add column in_time timestamp with time zone NOT NULL DEFAULT now() ;

--06/02/2017  UPDATE common_namecode_t
UPDATE common_namecode_t
SET namename='Member' WHERE type_code='position' AND code=1;

--06/05/2017 UPDATE emp_message_t - Add active column
ALTER TABLE emp_message_t
ADD COLUMN active smallint NOT NULL DEFAULT 1;

--06/06/2017 alter column type emp_no to character varying 
alter table employee_t alter column emp_no type character varying(10);
alter table emp_message_t alter column emp_no type character varying(10);
alter table emp_message_t alter column sender_emp_no type character varying(10);

--06/06/2017 UPDATE emp_message_t - add active sent items
ALTER TABLE emp_message_t
ADD COLUMN active_sent_item smallint NOT NULL DEFAULT 1;


--06/09/2017 UPDATE mail_template_t for empregist
INSERT INTO mail_template_t(
            key, value, utime, site_id)
    VALUES ('empregist_pc_body', '

Dear <%=.Model.fname%> <%=.Model.lname%>,


You are now Registered to IVP EMS

Username:<%=.Model.email%>
Password:<%=.Model.password%>


visit IVPEMS.co.jp to change your password thank you.

Regards,
IVP Global ph.
https://ww2.ivp.co.jp/english/',current_timestamp,1);


INSERT INTO mail_template_t(
            key, value, utime, site_id)
    VALUES ('empregist_pc_title', 'IVP EMS Account Registration',current_timestamp,1);


INSERT INTO mail_template_t(
            key, value, utime, site_id)
    VALUES ('empregist_sendtype', '40',current_timestamp,1);
	
	
--added emp_csv_upload_confirm	
	
UPDATE role_ems_t set role_action = role_action || '{emp_home,
AddAnnouncement,
ModifyAnnouncement,
emp_list,
emp_details,
emp_edit,
emp_edit_confirm,
emp_edit_complete,
emp_deactivate,
emp_deactivate_complete,
emp_search,
emp_profile,
emp_inbox,
emp_message,
emp_new_message,
pcode_regist,
pcode_regist_confirm,
pcode_regist_complete,
pcode_list,
pcode_delete,
pcode_delete_complete,
dreport_pcode_list,
emp_regist,
emp_regist_confirm,
emp_regist_complete,
dreport_edit,
dreport_edit_confirm,
dreport_edit_complete,
dreport_details,
dreport_add,
dreport_confirm,
dreport_complete,
drelport_search,
dreport_search_result,
download_csv_all,
download_csv_page,
download_csv_dateRange,
emp_csv_upload_confirm,
emp_csv_download_all,
emp_csv_download_page}'
WHERE role_poscode = '3' ;	

---------6/14/2017--------
INSERT INTO emp_team_t (id,team_name) VALUES ('3','Administrative');

---------6/16/2017--------
ALTER TABLE employee_t ADD CONSTRAINT constraint_emp_no UNIQUE (emp_no);

-- 6/19/2017 added intime and utime fields in employee_t

alter table employee_t add column intime timestamp with time zone not null default current_date;
alter table employee_t add column utime timestamp with time zone;

---06/20/2017------ Add seen_flag in emp_message_t
ALTER TABLE emp_message_t
ADD COLUMN seen_flag smallint NOT NULL DEFAULT 0;

-- 6/20/2017 create m_flg--
ALTER TABLE employee_t add COLUMN m_flg smallint DEFAULT 0;

-- 6/20/2017 add to batch_schedule_t --
INSERT INTO batch_schedule_t (id,batch_id,batch_name,schedule,last_date, intime,utime,active,last_success_date,site_id)
VALUES (47,'EmployeeRegisterSendEmail','従業員登録電子メールを送信','5 * * * *','2015-10-01 11:11:00+08',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,1, CURRENT_TIMESTAMP,0);

-- 6/20/2017 add to batch_sequence_t --
INSERT INTO batch_sequence_t (id,batch_id,execute_order,class_name,options, intime,utime,active,site_id)
VALUES (54,'EmployeeRegisterSendEmail',1,'jp.co.ivp.ers.batch.EmployeeRegisterSendEmail.EmployeeRegisterSendEmailCommand',null,CURRENT_TIMESTAMP,null,1,0);


-- 7/12/2017 add data to common_namecode_t for modtype

INSERT INTO common_namecode_t (type_code,code,namename,disp_order,intime,active,site_id)
values ('modtype',1,'Modify',1,CURRENT_TIMESTAMP,1,0 ),('modtype',2,'Delete',2,CURRENT_TIMESTAMP,1,0 );

-- 7/13/2017 update modtype code  --

UPDATE common_namecode_t
SET code = 2
WHERE type_code = 'modtype' AND namename = 'Delete';

UPDATE common_namecode_t
SET code = 1
WHERE type_code = 'modtype' AND namename = 'Modify';

INSERT INTO common_namecode_t(type_code,code,namename,disp_order,active) VALUES
('Gender',1,'Male',0,1);

INSERT INTO common_namecode_t(type_code,code,namename,disp_order,active) VALUES
('Gender',2,'Female',0,1);

-- 7/18/2017 add thread_email--
ALTER TABLE emp_message_t ADD thread_email varchar;

-- 7/20/2017 add emp_manage--
UPDATE role_ems_t set role_action = role_action || '{emp_home,
AddAnnouncement,
ModifyAnnouncement,
emp_list,
emp_details,
emp_edit,
emp_edit_confirm,
emp_edit_complete,
emp_deactivate,
emp_deactivate_complete,
emp_search,
emp_profile,
emp_inbox,
emp_message,
emp_new_message,
emp_manage,
pcode_regist,
pcode_regist_confirm,
pcode_regist_complete,
pcode_list,
pcode_delete,
pcode_delete_complete,
dreport_pcode_list,
emp_regist,
emp_regist_confirm,
emp_regist_complete,
dreport_edit,
dreport_edit_confirm,
dreport_edit_complete,
dreport_details,
dreport_add,
dreport_confirm,
dreport_complete,
drelport_search,
dreport_search_result,
download_csv_all,
download_csv_page,
download_csv_dateRange}'
WHERE role_poscode = '3' ;




-- 7/21/2017 add emp_profile edit--
UPDATE role_ems_t set role_action = role_action || '{emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,emp_inbox,emp_message,emp_new_message,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,emp_inbox,emp_message,emp_new_message,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,drelport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,emp_csv_upload_confirm,emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,emp_inbox,emp_message,emp_new_message,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,drelport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,emp_csv_upload_confirm,emp_csv_download_all,emp_csv_download_page,emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,emp_inbox,emp_message,emp_new_message,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,drelport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,
emp_manage,
emp_manage_confirm,
emp_manage_complete,
emp_profile_edit,
emp_profile_edit_confirm,
emp_profile_edit_complete}'
WHERE role_poscode = '3' ;	

UPDATE role_ems_t set role_action = role_action || '{emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,emp_inbox,emp_message,emp_new_message,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,team_member_list,remove_team_member,none_member_list,add_to_team,emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,emp_inbox,emp_message,emp_new_message,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,team_member_list,remove_team_member,none_member_list,add_to_team,emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,emp_inbox,emp_message,emp_new_message,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,team_member_list,remove_team_member,none_member_list,add_to_team,emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,emp_inbox,emp_message,emp_new_message,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,team_member_list,remove_team_member,none_member_list,add_to_team,emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,emp_inbox,emp_message,emp_new_message,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,team_member_list,remove_team_member,none_member_list,add_to_team,
emp_profile_edit,
emp_profile_edit_confirm,
emp_profile_edit_complete}'
WHERE role_poscode = '2' ;	

UPDATE role_ems_t set role_action = role_action || '{emp_home,emp_edit,emp_edit_confirm,emp_edit_complete,emp_profile,emp_inbox,emp_message,emp_new_message,dreport_pcode_list,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,
emp_profile_edit,
emp_profile_edit_confirm,
emp_profile_edit_complete}'
WHERE role_poscode = '1' ;	

------- 7/21/2017 add emp_list and emp_search
UPDATE role_ems_t set role_action = '{emp_home,emp_edit,emp_edit_confirm,emp_edit_complete,emp_profile,emp_search,emp_list,emp_inbox,emp_message,emp_new_message,dreport_pcode_list,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,
emp_profile_edit,
emp_profile_edit_confirm,
emp_profile_edit_complete}' WHERE role_poscode = '1' ;	


--------8/3/2017 revision of message
CREATE TABLE public.emp_thread_t
(
  id serial NOT NULL,
  subject character varying (30),
  sender_emp_no character varying(10),
  recipient_emp_no character varying(10),
  intime timestamp with time zone not null default current_date,
  sender_last_active timestamp with time zone,
  recipient_last_active timestamp with time zone,
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.emp_thread_t
  OWNER TO postgres;

COMMENT ON COLUMN public.emp_thread_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.emp_thread_t.subject IS 'range to 30 CHK_TYPE_All';
COMMENT ON COLUMN public.emp_thread_t.sender_emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN public.emp_thread_t.recipient_emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN public.emp_thread_t.intime IS 'CHK_TYPE_Date';
COMMENT ON COLUMN public.emp_thread_t.sender_last_active IS 'CHK_TYPE_Date';
COMMENT ON COLUMN public.emp_thread_t.recipient_last_active IS 'CHK_TYPE_Date';

CREATE TABLE public.emp_thread_details_t
(
  id serial NOT NULL,
  thread_no numeric not null,
  maker_emp_no character varying(10),
  message character varying(255),
  active smallint not null default 1,
  intime timestamp with time zone not null default current_date,
  seen_flg smallint
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.emp_thread_details_t
  OWNER TO postgres;
  
COMMENT ON COLUMN public.emp_thread_details_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.emp_thread_details_t.thread_no IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.emp_thread_details_t.maker_emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN public.emp_thread_details_t.message IS 'range to 255 CHK_TYPE_All';
COMMENT ON COLUMN public.emp_thread_details_t.active IS 'range to 1 CHK_TYPE_Numeric';
COMMENT ON COLUMN public.emp_thread_details_t.intime IS 'CHK_TYPE_Date';
COMMENT ON COLUMN public.emp_thread_details_t.seen_flg IS 'range to 1 CHK_TYPE_Numeric';


UPDATE role_ems_t set role_action = role_action || '{emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,
emp_manage,
emp_manage_confirm,
emp_manage_complete,
emp_profile_edit,
emp_profile_edit_confirm,
emp_profile_edit_complete,
employee_message,
employee_send_message,
employee_delete_thread}'
WHERE role_poscode = '3' ;	

UPDATE role_ems_t set role_action = role_action || '{emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,team_member_list,remove_team_member,none_member_list,add_to_team,
emp_profile_edit,
emp_profile_edit_confirm,
emp_profile_edit_complete,
employee_message,
employee_send_message,
employee_delete_thread}'
WHERE role_poscode = '2' ;	

UPDATE role_ems_t set role_action = '{emp_home,emp_edit,emp_edit_confirm,emp_edit_complete,emp_profile,emp_search,emp_list,dreport_pcode_list,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,
emp_profile_edit,
emp_profile_edit_confirm,
emp_profile_edit_complete,
employee_message,
employee_send_message,
employee_delete_thread}' WHERE role_poscode = '1' ;	

--update 08/15/2017--
CREATE TABLE public.sched_batch_log_t
(
  id serial NOT NULL, -- CHK_TYPE_Numeric
  emp_no character varying(10), -- range to 10 CHK_TYPE_NumericString
  execution_date date NOT NULL DEFAULT now(), -- CHK_TYPE_Date
  execution_time timestamp with time zone NOT NULL DEFAULT now(), -- CHK_TYPE_Date
  details text,
  error_details text,
  error_exception text,
  utime timestamp with time zone, -- CHK_TYPE_Date
  status int NOT NULL, -- range to CHK_TYPE_All
  CONSTRAINT sched_batch_log_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.sched_batch_log_t
  OWNER TO postgres;
COMMENT ON COLUMN public.sched_batch_log_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN public.sched_batch_log_t.emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN public.sched_batch_log_t.details IS 'range to CHK_TYPE_All';
COMMENT ON COLUMN public.sched_batch_log_t.status IS 'range to CHK_TYPE_Numeric';

--update 08/16/2017-- TIME KEEPING
CREATE TABLE public.leave_balance_t
(
  id serial NOT NULL,
  emp_no character varying(10),
  last_vl decimal(3,1),
  now_vl decimal(3,1),
  total_vl decimal(3,1),
  last_sl decimal(3,1),
  now_sl decimal(3,1),
  total_sl decimal(3,1),
  reason character varying(255),
  intime timestamp with time zone NOT NULL DEFAULT ('now'::text)::date,
  leave_credits decimal(3,1),
  CONSTRAINT leave_absence_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.leave_balance_t
  OWNER TO postgres;
COMMENT ON COLUMN leave_balance_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN leave_balance_t.emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN leave_balance_t.last_vl IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN leave_balance_t.now_vl IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN leave_balance_t.total_vl IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN leave_balance_t.last_sl IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN leave_balance_t.now_sl IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN leave_balance_t.total_sl IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN leave_balance_t.reason IS 'range to 255 CHK_TYPE_All';
COMMENT ON COLUMN leave_balance_t.intime IS 'CHK_TYPE_Date';
COMMENT ON COLUMN leave_balance_t.leave_credits IS 'CHK_TYPE_Numeric';



CREATE TABLE public.request_t
(
  id serial NOT NULL,
  emp_no character varying(10),
  request_type integer,
  date_filed timestamp with time zone NOT NULL DEFAULT ('now'::text)::date,
  status smallint NOT NULL DEFAULT 0,
  reason character varying(255),
  date_start date,
  date_end date,
  time_start time without time zone,
  time_end time without time zone,
  active smallint NOT NULL DEFAULT 1,
  utime timestamp with time zone,
  leave_type integer,
  cancel_reason character varying(255),
  used_leave decimal(3,1),
  CONSTRAINT request_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.request_t
  OWNER TO postgres;
COMMENT ON COLUMN request_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN request_t.emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN request_t.request_type IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN request_t.reason IS 'range to 255 CHK_TYPE_All';
COMMENT ON COLUMN request_t.date_start IS 'CHK_TYPE_Date';
COMMENT ON COLUMN request_t.date_end IS 'CHK_TYPE_Date';
COMMENT ON COLUMN request_t.leave_type IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN request_t.cancel_reason IS 'range to 255 CHK_TYPE_All';
COMMENT ON COLUMN request_t.used_leave IS 'CHK_TYPE_Numeric';


CREATE TABLE public.approver_t
(
  id serial NOT NULL,
  request_id integer,
  emp_no character varying(10),
  approval_status smallint,
  reason character varying(255),
  intime timestamp with time zone NOT NULL DEFAULT ('now'::text)::date,
  PRIMARY KEY (id),
        FOREIGN KEY (request_id) REFERENCES request_t
)

WITH (
  OIDS=FALSE
);
ALTER TABLE public.approver_t
  OWNER TO postgres;
COMMENT ON COLUMN approver_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN approver_t.request_id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN approver_t.emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN approver_t.reason IS 'range to 255 CHK_TYPE_All';


INSERT INTO common_namecode_t (type_code, code, namename, disp_order)
VALUES('RequestType', 1, 'Leave', 1),
('RequestType', 2, 'Undertime', 2),
('RequestType', 3, 'Overtime', 3),
('RequestType', 4, 'Logsheet Correction', 4);


INSERT INTO common_namecode_t (type_code, code, namename, disp_order)
VALUES('StatusRequest', 0, 'Pending', 1),
('StatusRequest', 1, 'Approved', 2),
('StatusRequest', 2, 'Declined', 3);


INSERT INTO common_namecode_t (type_code, code, namename, disp_order)
VALUES('LeaveType', 1, 'Vacation Leave', 1),
('LeaveType', 2, 'Sick Leave', 2),
('LeaveType', 3, 'Maternity Leave', 3),
('LeaveType', 4, 'Paternity Leave', 4),
('LeaveType', 5, 'Parental Leave', 5),
('LeaveType', 6, 'No Pay Leave', 6),
('LeaveType', 7, 'Compassionate Leave', 7),
('LeaveType', 8, 'Marriage Leave', 8),
('LeaveType', 9, 'Emergency Leave', 9),
('LeaveType', 10, 'Others', 10);

INSERT INTO common_namecode_t (type_code, code, namename, disp_order)
VALUES('UndertimeReason', 1, 'Personal Matter', 1),
('UndertimeReason', 2, 'Sickness', 2),
('UndertimeReason', 3, 'Others', 3);

INSERT INTO common_namecode_t (type_code, code, namename, disp_order)
VALUES('ApprovalStatus', 0, 'Declined', 1),
('ApprovalStatus', 1, 'Approved', 2);

-- 2017/08/29 Update --
INSERT INTO common_namecode_t (type_code, code, namename, disp_order)
VALUES('ClockPeriod', 1, 'AM', 1),
('ClockPeriod', 2, 'PM', 2);

--2017/8/30
alter table employee_t 
add position_title integer not null default 0 ,add date_hired timestamp with time zone, add reg_date timestamp with time zone

DELETE FROM common_namecode_t where type_code = 'PositionTitle';
insert into common_namecode_t (type_code,code,namename,disp_order,active)
values ('PositionTitle',0,'No position set',1,1),
('PositionTitle',1,'President',1,1),
('PositionTitle',2,'Vice President',2,1),
('PositionTitle',3,'Director',3,1),
('PositionTitle',4,'HR',4,1),
('PositionTitle',5,'Team Leader',5,1),
('PositionTitle',6,'System Administrator',6,1),
('PositionTitle',7,'Software Engineer',7,1),
('PositionTitle',8,'Business Analyst',8,1),
('PositionTitle',9,'Quality Assurance',9,1),
('PositionTitle',10,'Front-end Developer',10,1);


--automatic update on leave_balance_total

CREATE OR REPLACE FUNCTION trg_sl_total_compute()
  RETURNS trigger AS
$func$
BEGIN

NEW.total_sl := NEW.last_sl + New.now_sl;

RETURN NEW;

END
$func$ 

LANGUAGE plpgsql;

CREATE TRIGGER total_sl_default
BEFORE INSERT ON leave_balance_t
FOR EACH ROW
WHEN (NEW.total_sl IS NULL)
EXECUTE PROCEDURE trg_sl_total_compute();

CREATE OR REPLACE FUNCTION trg_vl_total_compute()
  RETURNS trigger AS
$func$
BEGIN

NEW.total_vl := NEW.last_vl + New.now_vl;  

RETURN NEW;

END
$func$ 

LANGUAGE plpgsql;

CREATE TRIGGER total_vl_default
BEFORE INSERT ON leave_balance_t
FOR EACH ROW
WHEN (NEW.total_vl IS NULL)
EXECUTE PROCEDURE trg_vl_total_compute();

DELETE FROM common_namecode_t where type_code = 'LeaveType';
insert into common_namecode_t (type_code,code,namename,disp_order,active)
values ('LeaveType',1,'Vacation Leave',1,1),
('LeaveType',2,'Sick Leave',2,1),
('LeaveType',3,'Maternity Leave',3,1),
('LeaveType',4,'Paternity Leave',4,1),
('LeaveType',5,'Parental Leave',5,1),
('LeaveType',6,'No Pay Leave',6,1),
('LeaveType',7,'Compassionate Leave',7,1),
('LeaveType',8,'Marriage Leave',8,1),
('LeaveType',9,'Emergency Leave',9,1);

DELETE FROM common_namecode_t where type_code = 'RequestType';
insert into common_namecode_t (type_code,code,namename,disp_order,active)
values ('RequestType',1,'Leave of Absence',1,1),
('RequestType',2,'Undertime',2,1),
('RequestType',3,'Overtime',3,1),
('RequestType',4,'Attendance Logsheet',4,1);

UPDATE role_ems_t set role_action = role_action || '{emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,
emp_manage,
emp_manage_confirm,
emp_manage_complete,
emp_profile_edit,
emp_profile_edit_confirm,
emp_profile_edit_complete,
employee_message,
employee_send_message,
employee_delete_thread,
download_requests}'
WHERE role_poscode = '3' ;	

--2017/09/07 

insert into common_namecode_t (type_code,code,namename,disp_order,active)
values ('StatusRequest',3,'Cancelled',4,1);

-- 2017/09/08 Update --
INSERT INTO common_namecode_t (type_code, code, namename, disp_order)
VALUES('EmployeeSearch', 1, 'Employee No.', 1),
('EmployeeSearch', 2, 'Employee Email', 2),
('EmployeeSearch', 3, 'First Name', 3),
('EmployeeSearch', 4, 'Last Name', 4);

-- 09/13 Update --
INSERT INTO common_namecode_t (type_code, code, namename, disp_order)
VALUES('EmployeeSearch', 5, 'Schedule', 5);

--- 9/14/17 ---
UPDATE role_ems_t set role_action = role_action || '{emp_home,AddAnnouncement,ModifyAnnouncement,emp_list,emp_details,emp_edit,emp_edit_confirm,emp_edit_complete,emp_deactivate,emp_deactivate_complete,emp_search,emp_profile,pcode_regist,pcode_regist_confirm,pcode_regist_complete,pcode_list,pcode_delete,pcode_delete_complete,dreport_pcode_list,emp_regist,emp_regist_confirm,emp_regist_complete,dreport_edit,dreport_edit_confirm,dreport_edit_complete,dreport_details,dreport_add,dreport_confirm,dreport_complete,dreport_search,dreport_search_result,download_csv_all,download_csv_page,download_csv_dateRange,team_management_list,remove_team_member,add_to_team,
emp_profile_edit,
emp_profile_edit_confirm,
emp_profile_edit_complete,
employee_message,
employee_send_message,
employee_delete_thread}'

--2017/09/15 add correction_id to request_t (employee_correction_t.id)
alter table request_t add correction_id integer;
COMMENT ON COLUMN request_t.correction_id IS 'CHK_TYPE_Numeric';

--2017/09/20
CREATE TABLE employee_correction_t
(
  id serial NOT NULL, -- CHK_TYPE_Numeric
  emp_no character varying(10) NOT NULL, -- range to 10 CHK_TYPE_NumericString
  lname character varying(100) NOT NULL, -- range to 100 CHK_TYPE_All
  fname character varying(100) NOT NULL, -- range to 100 CHK_TYPE_All
  birthday date, -- CHK_TYPE_Date
  address character(255), -- range to 255 CHK_TYPE_All
  contact_no character varying(100), -- range 5 to 15 CHK_TYPE_HyphenNumber
  email character varying(255), -- range to 100 CHK_TYPE_EMailAddr
  gender numeric, -- range 1 to 2 CHK_TYPE_Numeric
  intime timestamp with time zone NOT NULL DEFAULT ('now'::text)::date,
  utime timestamp with time zone,
  CONSTRAINT employee_correction_t_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE employee_correction_t
  OWNER TO postgres;
COMMENT ON COLUMN employee_t.id IS 'CHK_TYPE_Numeric';
COMMENT ON COLUMN employee_t.emp_no IS 'range to 10 CHK_TYPE_NumericString';
COMMENT ON COLUMN employee_t.lname IS 'range to 100 CHK_TYPE_All';
COMMENT ON COLUMN employee_t.fname IS 'range to 100 CHK_TYPE_All';
COMMENT ON COLUMN employee_t.birthday IS 'CHK_TYPE_Date';
COMMENT ON COLUMN employee_t.address IS 'range to 255 CHK_TYPE_All';
COMMENT ON COLUMN employee_t.contact_no IS 'range 5 to 15 CHK_TYPE_HyphenNumber';
COMMENT ON COLUMN employee_t.email IS 'range to 100 CHK_TYPE_EMailAddr';
COMMENT ON COLUMN employee_t.gender IS 'range 1 to 2 CHK_TYPE_Numeric';

--9/21/2017
alter table employee_t alter column address type character varying(255);

alter table request_t alter column reason type character varying(500);

DELETE FROM common_namecode_t where type_code = 'RequestType';
INSERT INTO common_namecode_t (type_code, code, namename, disp_order)
VALUES('RequestType', 1, 'Leave', 1),
('RequestType', 2, 'Undertime', 2),
('RequestType', 3, 'Overtime', 3),
('RequestType', 4, 'Logsheet Correction', 4),
('RequestType', 5, 'Personal Information Correction', 5);

------------ STARTING 09/29 ALL NEWLY ADDED COLUMNS/MODIFY TABLES SHOULD BE ADDED IN EMS_table_specification.xls----------------
------------ STARTING 09/29 ALL NEWLY ADDED COLUMNS/MODIFY TABLES SHOULD BE ADDED IN EMS_table_specification.xls----------------
------------ STARTING 09/29 ALL NEWLY ADDED COLUMNS/MODIFY TABLES SHOULD BE ADDED IN EMS_table_specification.xls----------------

 -- 09/29 modify ReportSummary name
 
DELETE FROM common_namecode_t WHERE type_code = 'ReportSummary';
INSERT INTO public.common_namecode_t(type_code, code, namename, disp_order, intime, active, site_id)                 
    VALUES ('ReportSummary', 1, 'First Quarter', 1, current_timestamp, 1, 0),
		   ('ReportSummary', 2, 'Second Quarter', 2, current_timestamp, 1, 0),
		   ('ReportSummary', 3, 'Third Quarter', 3, current_timestamp, 1, 0),
		   ('ReportSummary', 4, 'Fourth Quarter', 4, current_timestamp, 1, 0),
		   ('ReportSummary', 5, 'First Half', 5, current_timestamp, 1, 0),
		   ('ReportSummary', 6, 'Second Half', 6, current_timestamp, 1, 0),
		   ('ReportSummary', 7, 'Today', 7, current_timestamp, 1, 0),
           ('ReportSummary', 8, 'Week', 8, current_timestamp, 1, 0),
		   ('ReportSummary', 9, 'Month', 9, current_timestamp, 1, 0),
		   ('ReportSummary', 10, 'Year', 10, current_timestamp, 1, 0);
		   
		   
 -- 09/29 - increase length of character varying in some tables
-- add primary key for emp_message_t
ALTER TABLE emp_thread_t ALTER COLUMN subject TYPE character varying(255);

ALTER TABLE leave_balance_t ALTER COLUMN reason TYPE character varying(255);

ALTER TABLE emp_message_t ADD CONSTRAINT emp_message_t_pkey PRIMARY KEY (id);

-- 10/02 change reason column type and comment
-- drop not null employee_correction_t
 ALTER TABLE request_t ALTER COLUMN reason TYPE text;

COMMENT ON COLUMN request_t.reason IS 'range to 10000 CHK_TYPE_Templates';

alter table employee_correction_t alter fname drop not null,
                alter lname drop not null;

--10/4/2017 modified team list
DELETE FROM common_namecode_t WHERE type_code = 'Team';
INSERT INTO common_namecode_t(type_code,code,namename,disp_order,active) VALUES
('Team',1,'Administrative',0,1),
('Team',2,'ERS',0,1),
('Team',3,'HTML',0,1),
('Team',4,'Business Analyst',0,1),
('Team',5,'Quality Assurance',0,1),
('Team',6,'System Administration',0,1);

--10/18 added received_flg in request_t

ALTER TABLE request_t ADD COLUMN received_flg smallint;
ALTER TABLE request_t ALTER COLUMN received_flg SET DEFAULT 0;

--1/29/2018 modified team list
Delete from common_namecode_t where type_code = 'Team';
Insert into common_namecode_t(type_code,code,namename,disp_order,active)
values ('Team',1,'Administrative',1,1),
('Team',2,'C#',2,1),
('Team',3,'Front-end',3,1),
('Team',4,'Business Analyst',4,1),
('Team',5,'Quality Assurance',5,1),
('Team',6,'System Admin',6,1);

alter table leave_balance_t drop leave_credits;

--1/31/2018 for Report LIST
ALTER TABLE dailyreport_details_t ADD COLUMN utime timestamp with time zone
ALTER TABLE dailyreport_details_t ADD COLUMN downloaded integer not null default 0

--1/31/2018 set default for vl/sl
ALTER TABLE leave_balance_t
ALTER COLUMN last_vl SET DEFAULT 0,
ALTER COLUMN last_sl SET DEFAULT 0,
ALTER COLUMN now_vl SET DEFAULT 0,
ALTER COLUMN now_sl SET DEFAULT 0,
ALTER COLUMN total_vl SET DEFAULT 0,
ALTER COLUMN total_sl SET DEFAULT 0;

--2/13/2018 added tin and sss on employee details
ALTER TABLE employee_t ADD COLUMN sss_no character varying(15);
ALTER TABLE employee_t ADD COLUMN tin_no character varying(15);
COMMENT ON COLUMN employee_t.sss_no IS 'range to 15 CHK_TYPE_HyphenNumber';
COMMENT ON COLUMN employee_t.tin_no IS 'range to 15 CHK_TYPE_HyphenNumber';

-- 2/21/2018 used for batch reset leaves
insert into batch_sequence_t (batch_id, execute_order, class_name)
values
('EmployeeResetLeaves',1,'jp.co.ivp.ers.batch.EmployeeResetLeaves.EmployeeResetLeavesCommand');
 insert into batch_schedule_t (batch_id, batch_name, schedule)
values
('EmployeeResetLeaves','EmployeeResetLeaves', '0 0 1 1 *');

--2/22/2018 add file_name in request_t
ALTER TABLE request_t ADD file_name character varying(255);

-- 02/26/2018 add job_title_t 
-- Table: job_title_t

-- DROP TABLE job_title_t;

CREATE TABLE job_title_t
(
    id serial NOT NULL,
    job_title character varying(100) NOT NULL UNIQUE,
    job_description character varying(1000) NOT NULL UNIQUE,
    disp_order integer NOT NULL UNIQUE,
    intime timestamp with time zone NOT NULL DEFAULT now(),
    utime timestamp with time zone,
    CONSTRAINT job_title_t_job_title PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);
COMMENT ON COLUMN job_title_t.id
    IS 'CHK_TYPE_Numeric';

COMMENT ON COLUMN job_title_t.job_title
    IS 'range to 100 CHK_TYPE_All';

COMMENT ON COLUMN job_title_t.job_description
    IS 'range to 1000 CHK_TYPE_All';
        
COMMENT ON COLUMN job_title_t.disp_order
    IS 'CHK_TYPE_Numeric';

COMMENT ON COLUMN job_title_t.intime
    IS 'CHK_TYPE_Date';

COMMENT ON COLUMN public.job_title_t.utime
    IS 'CHK_TYPE_Date';

CREATE INDEX job_title_t_job_title_idx
    ON job_title_t USING btree
    (job_title)
    TABLESPACE pg_default;
    
CREATE INDEX job_title_t_id_idx
    ON job_title_t USING btree
    (id)
    TABLESPACE pg_default;

	
ALTER TABLE employee_t
    ADD COLUMN job_title integer DEFAULT 0;

COMMENT ON COLUMN employee_t.job_title
    IS 'CHK_TYPE_Numeric';

-- 02/27/2018 set schedule to every hour
UPDATE batch_schedule_t 
SET schedule='0 * * * *'
WHERE batch_id='UploadEmpSchedule';

--02/28 remove position_title in common_namecode_t
DELETE FROM common_namecode_t
WHERE type_code = 'PositionTitle'

ALTER TABLE employee_t DROP COLUMN position_title;