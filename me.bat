@Set Path=C:\Program Files (x86)\MSBuild\14.0\Bin;%PATH%

:: Build ERS solution
msbuild ers\ers-v7.2.0.sln /nologo ^
/t:Build ^
/v:q ^
/clp:ErrorsOnly ^
/p:DeployOnBuild=true;^
DeployTarget=Package;^
Configuration=DEBUG;^
IncludeSetAclProviderOnDestination=False

if not %ERRORLEVEL% == 0 (
	exit /b %ERRORLEVEL%
)