**Branch naming rule**
#[taskNo.]_[short description]
Ex.) #56_additonal_funtion_leave_request

**Comment rule for git commit**
#[taskNo.][correspondence]
Ex.) #56 added approve function for leave request

**Pull Process**

Step 1: 
Go to master branch and pull latest updates from origin/master

Step 2: 
Make a new branch with format like
"Number_DefinitionOfTask" - number should be the task number in 

https://docs.google.com/spreadsheets/d/1uJSvjdbxNK6NOGcNAeOGB6K0TKxNUKnVnz_VlU8D-pc/edit?ts=592ce6bc#gid=198269237

Ex.) 12_fixed_employee_list_sorting

**Push Process**

Step 1:
Make sure your branch is up to date
- Pull origin/master to current branch, select "commit merged changes immediately"
- look for conflicts and resolve it

Step 2: 
Commit and Push changes to remote branch 
**Note***
"DO NOT PUSH DIRECTLY TO MASTER BRANCH"
"DO NOT PUSH CSV AND IMAGE_FILES"

Step 3:
In https://bitbucket.org/ivpphil/ems/branches/ create a pull request and ask for PR review

Step 4:
When merged, ask sir sean to build jenkins to check if there is a conflict in remote repository