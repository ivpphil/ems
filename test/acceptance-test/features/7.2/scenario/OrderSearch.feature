﻿@ersCTS
Feature: OrderSearch
	Contains test case Order Search

#------------------------------------------------------------------------------------------------
Scenario: Order Search
	#This test case test checks if 該当なし message appears if the target search is order
	#And if unsubscribed customer and all the order types are unchecked.
#------------------------------------------------------------------------------------------------
	When Open /cts/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_id@name | passwd@name |
			| ivpers        | ivpers |
		And Click login_btn Wait[Loaded] 
		And Open /cts/search Wait[Loaded] 
		And Enter
		| Field                      | Value       |
		| TargetSearch@name          | OrderSearch |
		| mcode@name                 |             |
		| tel@name                   |             |
		| fax@name                   |             |
		| lnamek@name                |             |
		| fnamek@name                |             |
		| lname@name                 |             |
		| fname@name                 |             |
		| zip1@name                  |             |
		| zip2@name                  |             |
		| pref@name                  |             |
		| address@name               |             |
		| email@name                 |             |
		| src_deleted@name           | 0           |
		| ordertype@name             | 0           |
		| ctsordertype@name          | 0           |
		| regordertype@name          | 0           |
		| s_af_cancel_date_from@name |             |
		| s_af_cancel_date_to@name   |             |
		| s_cancel_date_from@name    |             |
		| s_cancel_date_to@name      |             |
		| d_no@name                  |             |
		| mall_d_no@name             |             |
		And Click SearchButton@class Wait[Loaded] 
	Then Assert Elements 
		| note_result |
		| 該当なし     |

		

