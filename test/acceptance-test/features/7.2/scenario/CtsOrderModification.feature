﻿@ers
Feature: CtsOrderModification
	Test case for cts order modification

Scenario: Change Payment From Credit Card to NoPayment With Point
	refs #

	Given Insert Template g_master_t Into g_master_t
		And Insert Template s_master_t Into s_master_t
		And Insert Template stock_t Into stock_t
		And Insert Template price_t Into price_t
		And Insert Template member_t Into member_t
		And Delete From point_t
		    | mcode              |
		    | @member_t[0].mcode |
		And Insert Template point_t Into point_t
		And Insert Template cts_login_t Into cts_login_t
	# Order in Front site using point
	When Open /top/detail/asp/detail.asp?gcode=E1401 Wait[None]
		And Click normal_basket_in@name Wait[Loaded]
		And Open /top/detail/asp/detail.asp?gcode=M5002 Wait[None]
		And Click normal_basket_in@name Wait[Loaded]
		And Enter
			| email@name                   | passwd@name |
			| k.matsuoka.ivp+004@gmail.com | matsuoka    |
		And Click login_btn@name Wait[Loaded]
		And Enter
			| pri_chk@name |
			| 1            |
		And Click entry_submit@name Wait[Loaded]
		And Enter
			| deliv_method@name | pay@name | card_id@name | card@name | cardno@name      | validity_y@name | validity_m@name |
			| 1                 | 1        |              | 1         | 4111111111111111 | 2020            | 5               |
		And Click entry2_submit@name Wait[Loaded]
		And Enter
			| ent_point@name |
			| 2000           |
		And Click point_flg@name Wait[Loaded]
		And Click entry3_submit@name Wait[Loaded]
	# Cancel one product and make the order NoNeedPayment
	When Open /cts/ Wait[None]
		And Enter
			| user_id@name | passwd@name |
			| ivpers       | ivpers      |
		And Click login_btn@name Wait[Loaded]
		And Open /cts/search Wait[None]
		And Enter
			| TargetSearch@name | email@name                   |
			| OrderSearch       | k.matsuoka.ivp+004@gmail.com |
		And Click search_order Wait[Loaded]
		And Click modify_order[0] Wait[Element] frameset
		And Switch[Frame] right_b@name Wait[Loaded]
			And Click goto_page2@name Wait[Loaded]
			And Enter
				| product_statusE1401_3000___NORMAL_____@name | up_amountE1401_3000___NORMAL_____@name |
				| 999                                         | 0                                      |
			And Wait[Loaded]
			And Click goto_confirm_button Wait[Loaded]
			And Click goto_complete_button Wait[None]
			And Accept Alert Wait[Loaded]
		And Close[Frame] Wait[None]
	# Assert if the order data(especially the used point) is correct
	When Open /top/member/asp/ Wait[None]
		And Enter
			| email@name                   | passwd@name |
			| k.matsuoka.ivp+004@gmail.com | matsuoka    |
		And Click login_btn Wait[Loaded]
		And Click point_history Wait[Loaded]
		And Click search_btn Wait[Loaded]
	Then Assert Elements
		| bayTotal[0]@class | usePoint[0]@class | getPoint[0]@class | havePoint[0]@class |
		| 0円                | -1,100            | 0                 | 98,900             |
	When Click detail_btn Wait[Loaded]
	Then Assert Elements
		| p_service |
		| -1,100円    |
	#Logout
	When Open /cts/ Wait[None]
		And Click Logout@class Wait[None]

Scenario: Card deletion in order update
	refs #

	Given Insert Template g_master_t Into g_master_t
		And Insert Template s_master_t Into s_master_t
		And Insert Template stock_t Into stock_t
		And Insert Template price_t Into price_t
		And Insert Template cts_login_t Into cts_login_t
		And Delete From member_t
			| lname   | fname        |
			| てすとCard | テストDeleteion |
		And Delete From d_master_t
			| lname   | fname        |
			| てすとCard | テストDeleteion |
	# Order in CTS
	When Open /cts/ Wait[None]
		And Enter
			| user_id@name | passwd@name |
			| ivpers       | ivpers      |
		And Click login_btn@name Wait[Loaded]
		And Open /cts/Order Wait[None]
		# Left side frame
		And Switch[Frame] left_t@name Wait[Loaded]
			And Enter
				| s_gcode@id |
				| E1401      |
			And Click search@name Wait[Loaded]
			And Click AddCart@class Wait[None]
		And Close[Frame] Wait[None]
		# Right botom frame
		And Switch[Frame] right_b@name Wait[Loaded]
			And Enter
				| lnamek@name | fnamek@name  | lname@name | fname@name | zip@name | tel@name      | sex@name |
				| テスト     | テスト | てすとCard        | テストDeleteion        | 675-0034 | 111-1111-1111 | 1        |
			And Click zip_flg1@name Wait[Seconds] 1
			And Click member_register@name Wait[Loaded]
			And Click goto_page2@name Wait[Loaded]
			And Enter
				| deliv_method@name | pay@name | card_id@name | card_type@name | cardno@name      | validity_y@name | card_will_add@name |
				| 1                 | 1        | 9999         | 1              | 4111111111111111 | 2020            | True               |
			And Click goto_confirm_button Wait[Loaded]
			And Click goto_complete_button Wait[None]
			And Accept Alert Wait[Loaded]
		And Close[Frame] Wait[None]
		# Go to modify Order then delete card without total changing
		And Open /cts/search Wait[None]
		And Enter
			| TargetSearch@name | lname@name | fname@name   |
			| OrderSearch       | てすとCard    | テストDeleteion |
		And Click search_order Wait[Loaded]
		And Click modify_order[0] Wait[Element] frameset
		And Switch[Frame] right_b@name Wait[Loaded]
			And Click goto_page2@name Wait[Loaded]
			And Enter
				| card_id@name |
				| [1]          |
			And Click card_delete@name Wait[Loaded]
			And Click goto_confirm_button Wait[Loaded]
			And Click goto_complete_button Wait[None]
			And Accept Alert Wait[Loaded]
		And Close[Frame] Wait[None]
	#Logout
	When Open /cts/ Wait[None]
		And Click Logout@class Wait[None]

Scenario: Update order which is created without member registration
	Given Insert Template g_master_t Into g_master_t
		And Insert Template s_master_t Into s_master_t
		And Insert Template stock_t Into stock_t
		And Insert Template price_t Into price_t
		And Delete From d_master_t
			| lname     | fname    |
			| 姓Without  | 名Member  |
			| 姓Without姓 | 名Member名 |
	# Order in Frontsite without member registration
	When Open /top/detail/asp/detail.asp?gcode=E1401 Wait[None]
		And Click normal_basket_in@name Wait[Loaded]
		And Click non_member_btn Wait[Loaded]
		And Enter
			| Field              | Value             |
			| lname@name         | 姓Without          |
			| fname@name         | 名Member           |
			| lnamek@name        | セイ                |
			| fnamek@name        | メイ                |
			| email@name         | nagaike@ivp.co.jp |
			| email_confirm@name | nagaike@ivp.co.jp |
			| tel@name           | 111-1111-1111     |
			| zip@name           | 675-0034          |
			| maddress@name      | 住所その他             |
			| pri_chk@name       | 1                 |
		And Click zip_flg1@name Wait[Seconds] 1
		And Click entry_submit@name Wait[Loaded]
		And Enter
			| deliv_method@name | pay@name | card_id@name | card@name | cardno@name      | validity_y@name | validity_m@name |
			| 1                 | 1        |              | 1         | 4111111111111111 | 2020            | 5               |
		And Click entry2_submit@name Wait[Loaded]
		And Click entry3_submit@name Wait[Loaded]
	# Goto Modify Order and assert if the changing customer address is affected to order
	When Open /cts/ Wait[None]
		And Enter
			| user_id@name | passwd@name |
			| ivpers       | ivpers      |
		And Click login_btn@name Wait[Loaded]
		And Open /cts/search Wait[None]
		And Enter
			| TargetSearch@name | lname@name | fname@name |
			| OrderSearch       | 姓Without   | 名Member    |
		And Click search_order Wait[Loaded]
		And Click modify_order[0] Wait[Element] frameset
		And Switch[Frame] right_b@name Wait[Loaded]
			And Click member_modify@name Wait[Loaded]
			And Enter
				| Field              | Value             |
				| lname@name         | 姓Without姓          |
				| fname@name         | 名Member名           |
				| lnamek@name        | セイセイ                |
				| fnamek@name        | メイメイ                |
				| email@name         | nagaike2@ivp.co.jp |
				| tel@name           | 222-2222-2222     |
				| zip@name           | 673-0001          |
				| maddress@name      | 住所その他2             |
			And Click zip_flg1@name Wait[Seconds] 1
			And Click member_register@name Wait[Loaded]
	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                        |
			| wk_order_name    | 姓Without姓 名Member名           |
			| wk_order_namek   | セイセイ メイメイ                    |
			| wk_order_tel     | 22222222222                |
			| wk_order_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |

	When Click goto_page2@name Wait[Loaded]

	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                        |
			| delivery_name    | 姓Without姓 名Member名           |
			| delivery_tel     | 22222222222                |
			| delivery_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |

	When Click goto_confirm_button Wait[Loaded]
			And Click goto_complete_button Wait[None]
			And Accept Alert Wait[Loaded]
		And Close[Frame] Wait[None]

	# Assert result
	When Open /cts/search Wait[None]
		And Enter
			| TargetSearch@name | lname@name | fname@name |
			| OrderSearch       | 姓Without姓  | 名Member名   |
		And Click search_order Wait[Loaded]
		And Click modify_order[0] Wait[Element] frameset
		And Switch[Frame] right_b@name Wait[Loaded]
	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                        |
			| wk_order_name    | 姓Without姓 名Member名           |
			| wk_order_namek   | セイセイ メイメイ                    |
			| wk_order_tel     | 22222222222                |
			| wk_order_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
	#Logout
	When Close[Frame] Wait[None]
		And Open /cts/ Wait[None]
		And Click Logout@class Wait[None]

Scenario: Update order which is created with member registration
	Given Insert Template g_master_t Into g_master_t
		And Insert Template s_master_t Into s_master_t
		And Insert Template stock_t Into stock_t
		And Insert Template price_t Into price_t
		And Insert Template member_t Into member_t
		And Delete From d_master_t
			| lname     | fname    |
			| てすと       | 松        |
			| 姓Without姓 | 名Member名 |
	# Order in Frontsite without member registration
	When Open /top/detail/asp/detail.asp?gcode=E1401 Wait[None]
		And Click normal_basket_in@name Wait[Loaded]
		And Enter
			| email@name                   | passwd@name |
			| k.matsuoka.ivp+004@gmail.com | matsuoka    |
		And Click login_btn@name Wait[Loaded]
		And Enter
			| pri_chk@name |
			| 1            |
		And Click entry_submit@name Wait[Loaded]
		And Enter
			| deliv_method@name | pay@name | card_id@name | card@name | cardno@name      | validity_y@name | validity_m@name |
			| 1                 | 1        |              | 1         | 4111111111111111 | 2020            | 5               |
		And Click entry2_submit@name Wait[Loaded]
		And Click entry3_submit@name Wait[Loaded]
	# Goto Modify Order and assert if the changing customer address is NOT affected to order
	# until user clicks [Apply current customer address]
	When Open /cts/ Wait[None]
		And Enter
			| user_id@name | passwd@name |
			| ivpers       | ivpers      |
		And Click login_btn@name Wait[Loaded]
		And Open /cts/search Wait[None]
		And Enter
			| TargetSearch@name | lname@name | fname@name |
			| OrderSearch       | てすと        | 松          |
		And Click search_order Wait[Loaded]
		And Click modify_order[0] Wait[Element] frameset
		And Switch[Frame] right_b@name Wait[Loaded]
			And Click member_modify@name Wait[Loaded]
			And Enter
				| Field              | Value             |
				| lname@name         | 姓Without姓          |
				| fname@name         | 名Member名           |
				| lnamek@name        | セイセイ                |
				| fnamek@name        | メイメイ                |
				| email@name         | nagaike2@ivp.co.jp |
				| tel@name           | 222-2222-2222     |
				| zip@name           | 673-0001          |
				| maddress@name      | 住所その他2             |
			And Click zip_flg1@name Wait[Seconds] 1
			And Click member_register@name Wait[Loaded]
	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                    |
			| wk_order_name    | てすと 松                    |
			| wk_order_namek   | テスト マツ                   |
			| wk_order_tel     | 000000000                |
			| wk_order_address | 〒651-0091 兵庫県 神戸市中央区 若菜通 |

	When Click goto_page2@name Wait[Loaded]
	
	# Asser if back button work
	When Click goto_page1@name Wait[Loaded]
	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                        |
			| wk_order_name    | てすと 松                    |
			| wk_order_namek   | テスト マツ                   |
			| wk_order_tel     | 000000000                |
			| wk_order_address | 〒651-0091 兵庫県 神戸市中央区 若菜通 |

	When Click goto_page2@name Wait[Loaded]

	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                    |
			| delivery_name    | てすと 松                    |
			| delivery_tel     | 000000000                |
			| delivery_address | 〒651-0091 兵庫県 神戸市中央区 若菜通 |

	When Click goto_confirm_button Wait[Loaded]
			And Click goto_complete_button Wait[None]
			And Accept Alert Wait[Loaded]
		And Close[Frame] Wait[None]

	# Assert result
	When Open /cts/search Wait[None]
		And Enter
			| TargetSearch@name | lname@name | fname@name |
			| OrderSearch       | てすと        | 松          |
		And Click search_order Wait[Loaded]
		And Click modify_order[0] Wait[Element] frameset
		And Switch[Frame] right_b@name Wait[Loaded]
	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                    |
			| wk_order_name    | てすと 松                    |
			| wk_order_namek   | テスト マツ                   |
			| wk_order_tel     | 000000000                |
			| wk_order_address | 〒651-0091 兵庫県 神戸市中央区 若菜通 |
	# Goto Modify Order and assert if the changing customer address is affected to order
	# after user clicked [Apply current customer address]
	When Click shipping_overwrite@id Wait[Seconds] 0.5
	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                        |
			| wk_order_name    | 姓Without姓 名Member名           |
			| wk_order_namek   | セイセイ メイメイ                    |
			| wk_order_tel     | 22222222222                |
			| wk_order_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |

	When Click goto_page2@name Wait[Loaded]
	
	# Asser if back button work
	When Click goto_page1@name Wait[Loaded]
	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                        |
			| wk_order_name    | 姓Without姓 名Member名           |
			| wk_order_namek   | セイセイ メイメイ                    |
			| wk_order_tel     | 22222222222                |
			| wk_order_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |

	When Click goto_page2@name Wait[Loaded]

	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                    |
			| delivery_name    | 姓Without姓 名Member名           |
			| delivery_tel     | 22222222222                |
			| delivery_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |

	When Click goto_confirm_button Wait[Loaded]
			And Click goto_complete_button Wait[None]
			And Accept Alert Wait[Loaded]
		And Close[Frame] Wait[None]

	# Assert result
	When Open /cts/search Wait[Loaded]
		And Enter
			| TargetSearch@name | lname@name | fname@name |
			| OrderSearch       | 姓Without姓  | 名Member名   |
		And Click search_order Wait[Loaded]
		And Click modify_order[0] Wait[Element] frameset
		And Switch[Frame] right_b@name Wait[Loaded]
	Then Assert Elements
			| Field            | Value                        |
			| customer_name    | 姓Without姓 名Member名           |
			| customer_namek   | セイセイ メイメイ                    |
			| customer_tel     | 22222222222                |
			| customer_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
		And Assert Elements
			| Field            | Value                    |
			| wk_order_name    | 姓Without姓 名Member名           |
			| wk_order_namek   | セイセイ メイメイ                    |
			| wk_order_tel     | 22222222222                |
			| wk_order_address | 〒673-0001 兵庫県 明石市 明南町 住所その他2 |
	#Logout
	When Close[Frame] Wait[None]
		And Open /cts/ Wait[None]
		And Click Logout@class Wait[None]
