﻿@createsample
Feature: CreateSample
	This is sample to learn test.
	specflow uses Gherkin language.
	Pelase refer http://www.specflow.org/documentation/Using-Gherkin-Language-in-SpecFlow/ to learn Gherkin

	#-------------------------------------------------------
Scenario: 1. How to prepare DB data
	-------------------------------------------------------

	Given Insert Template customer_t Into customer_t
	And Select From customer_t AS customer_t_result
		| customer_code                |
		| @customer_t[0].customer_code |
	Then Assert Data @customer_t
		| email         |
		| foo@ivp.co.jp |
		And Assert Data @customer_t_result
		| email                |
		| @customer_t[*].email |

	#-------------------------------------------------------
Scenario: 2. Overwrite template data
	-------------------------------------------------------

	Given Insert Template customer_t Into customer_t
		| email         |
		| bar@ivp.co.jp |
	And Select From customer_t AS customer_t_result
		| customer_code                |
		| @customer_t[0].customer_code |
	Then Assert Data @customer_t
		| email         |
		| bar@ivp.co.jp |
		And Assert Data @customer_t_result
		| email                |
		| @customer_t[*].email |

	#-------------------------------------------------------
Scenario: 3. Open browser
	-------------------------------------------------------

	When Open / Wait[None]
	Then Do Nothing

	#-------------------------------------------------------
Scenario: 4. Manipulates page
	-------------------------------------------------------

	Given Insert Template g_master_t Into g_master_t
		And Insert Template s_master_t Into s_master_t
		And Insert Template stock_t Into stock_t
		And Insert Template price_t Into price_t
	When Open / Wait[None]
		And Enter
			| s_keyword      |
			| ASUS-GTXTITANX |
	Then Do Nothing

	#-------------------------------------------------------
Scenario: 5. Clicks button
	-------------------------------------------------------

	Given Insert Template g_master_t Into g_master_t
		And Insert Template s_master_t Into s_master_t
		And Insert Template stock_t Into stock_t
		And Insert Template price_t Into price_t
	When Open / Wait[None]
		And Enter
			| s_keyword      |
			| ASUS-GTXTITANX |
		And Click search_keyword Wait[URL] /top/search/asp/list.asp
		And Click productList[0].thumbnail Wait[URL] /top/detail/asp/detail.asp
	Then Do Nothing

	#-------------------------------------------------------
Scenario: 6. Assertion of the page
	-------------------------------------------------------

	Given Insert Template customer_t Into customer_t
		And Insert Template g_master_t Into g_master_t
		And Insert Template s_master_t Into s_master_t
		And Insert Template stock_t Into stock_t
		And Insert Template price_t Into price_t
	When Open /top/detail/asp/detail.asp?gcode=ASUS-GTXTITANX Wait[None]
		And Click add_cart Wait[URL] /top/cart/asp/cart.asp
		And Enter
			| email         | passwd   |
			| foo@ivp.co.jp | aaaaaaaa |
		And Click customer_login_btn Wait[URL] /top/register/asp/login.asp
	Then Assert Elements
		| lname | fname | middle_name                |
		| TEST  | IVP   | @customer_t[0].middle_name |
		And Do Nothing

	#-------------------------------------------------------
Scenario: 7. Assertion of List element in the page
	-------------------------------------------------------

	Given Insert Template customer_t Into customer_t
		And Insert Template g_master_t Into g_master_t
		And Insert Template s_master_t Into s_master_t
		And Insert Template stock_t Into stock_t
		And Insert Template price_t Into price_t
	When Open / Wait[None]
		And Enter
			| s_keyword      |
			| ASUS-GTXTITANX |
		And Click search_keyword Wait[URL] /top/search/asp/list.asp
	Then Assert List productList
		| gname                              | price         |
		| Asus GTX TITAN X 12GB GDDR5 384Bit | 53,400.00 yen |
		And Assert List productList
		| thumbnail                                                                        |
		| <%=setup.pc_sec_url%>images/simg/ASUS-GTXTITANX/GASUS-GTXTITANX_01.jpg?width=160 |
		And Do Nothing

	#-------------------------------------------------------

Scenario: Purchasing
	Given Insert Template customer_t Into customer_t
		| email         | lname |
		| foo@ivp.co.jp | bar   |
		And Insert Template g_master_t Into g_master_t
		And Insert Template s_master_t Into s_master_t
		And Insert Template stock_t Into stock_t
		And Insert Template price_t Into price_t
		And Insert Template wh_stock_t Into wh_stock_t
		And Insert Template admin_user_t Into admin_user_t
		And Insert Template role_group_t Into role_group_t
		And Insert Template cts_agent_t Into cts_agent_t
	When Open / Wait[None]
		And Enter
			| s_keyword      |
			| ASUS-GTXTITANX |
		And Click search_keyword Wait[URL] /top/search/asp/list.asp
	Then Assert List productList
		| thumbnail                                                                        | gname                              | price         | description_for_list                                                                                                                                     |
		| <%=setup.pc_sec_url%>images/simg/ASUS-GTXTITANX/GASUS-GTXTITANX_01.jpg?width=160 | Asus GTX TITAN X 12GB GDDR5 384Bit | 53,400.00 yen | also meticulously built with the highest grade components for unbeatable stability and reliability while keeping the acoustics and temperature in check. |

	When Click productList[0].thumbnail Wait[URL] /top/detail/asp/detail.asp
		And Click add_cart Wait[URL] /top/cart/asp/cart.asp
		And Enter
			| email         | passwd   |
			| foo@ivp.co.jp | aaaaaaaa |
		And Click customer_login_btn Wait[URL] /top/register/asp/login.asp
	Then Assert Elements
		| lname | fname | middle_name                |
		| bar   | IVP   | @customer_t[0].middle_name |
	When Enter
			| pri_chk |
			| 1       |
		And Click entry_submit Wait[URL] /top/register/asp/entry2.asp
		And Enter
			| deliv_method        | plugin_PaymentId       |
			| japan_home_delivery | cashondelivery_payment |
		And Click entry2_submit Wait[URL] /top/register/asp/entry3.asp
	Then Assert Elements
			| fullname |
			| IVP Co bar |
	When Click entry3_submit Wait[URL] /top/register/asp/entry4.asp
		And Open /admin/top/login/asp/login.asp Wait[None]
		And Enter
			| user_login_id | passwd  |
			| ersuser       | ersuser |
		And Click login_button Wait[URL] /admin/
		And Open /admin/top/regular/asp/bill_search.asp Wait[None]
		And Enter
         | s_site_id | s_email       |
         | {1,5}     | foo@ivp.co.jp |
		And Click search_button Wait[URL] /admin/top/regular/asp/bill_list.asp
		And Click orderList[0].detail_button Wait[URL] /admin/top/regular/asp/bill_detail.asp

Scenario: Download Csv and Asserting
	-------------------------------------------------------

	When Open /admin/ Wait[None]
		And Enter
			| user_login_id@name | passwd@name |
			| ivpers             | ivpers      |
		And Click login_button@id Wait[Loaded]
	When Open /admin/top/customer/asp/cus_search.asp Wait[Loaded]
		And Enter
         | src_email@name       |
         | malamok160@ivp.co.jp |
		And Click search_btn Wait[Loaded]
		And Click form_btn[1]@class Save As downloadtest With[CSV][SHIFTJIS]
	Then Assert Data @downloadtest
	| ID       | Customership Code | E-mail Address       | First Name | Middle Name | Last Name | Company Name | Division Name | Address | Street Address, Apartment, Apartment Name | Additional Address Info | Region      | Postal Code | Country     | 電話番号        | Fax番号 | Birthdate  | Gender | Occupation | Total Purchase | E-zine Delivery Settings | Newsletter Setting | Phone Contact Setting | Comment on our store | Registration Date     | Last Updated         | Age   | Customer Rank | Sales Outlets |
	| 30001223 | 30001223          | malamok160@ivp.co.jp | Will       | SS          | Samong    |              |               | kobe    | sanomiya                                  |                         | Philippines | 123         | Philippines | 09069641061 |       | 1957/05/12 | Male   | 未設定        | 53697          | Delivery                 | Required           | Required              |                      | 12/14/2015 9:15:49 PM | 3/10/2016 7:31:07 PM | 50～59 |               | ERS           |
