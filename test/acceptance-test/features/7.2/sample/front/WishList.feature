﻿@V7.2PC
Feature: WishList
	This feature contains test cases for adding and deleting products in customer's wish list

#---------------------------------------------------------------------
Scenario: Add Product in Wish List
#---------------------------------------------------------------------
#This test case opens the front as robertnothanks
#Add another product in customer's wish list
#Checks the products under the wish list
#---------------------------------------------------------------------
	Given Insert Template 7.2/wishlist_t Into wishlist_t
		And Insert Template 7.2/member_t Into member_t
		And Delete From wishlist_t
			| scode    |
			| CARIN001 |
			| HMC      |

	When Open /top/detail/asp/detail.asp?gcode=CARIN101 Wait[Loaded] 	 
		And Click wish_btn Wait[Loaded] 	
		And Enter
			| email@name                    | passwd@name |
			| robertnothanks@mailinator.com | 12345678    |
		And Click next Wait[Loaded] 
		And Open /top/detail/asp/detail.asp?gcode=HERSHEY Wait[Loaded]
		And Click wish_btn Wait[Loaded]
		 	
	Then Assert List wishlist_List
		| img_for_list2@class                                        | itemName                   | attr1 | attr2 | itemDetails@class |
		| <%=setup.pc_sec_url%>images/simg/SHMC_01.jpg?width=92      | Hershey Milk Chocolate Bar | Milk  |       | 100円              |
		| <%=setup.pc_sec_url%>images/simg/SCARIN001_01.jpg?width=92 | BLACK SUNGLASSES           | BLACK |       | 5,000円            |
	
		When Click logout@class Wait[Loaded] 



#---------------------------------------------------------------------
Scenario: Delete Product in Wish List
#---------------------------------------------------------------------
#This test case opens the front as robertnothanks
#Clicks the 'details' button of the product in customer wish list
#Deletes one product in customer's wish list
#Checks if the product is no longer in the wish list
#---------------------------------------------------------------------
	Given Insert Template 7.2/wishlist_t Into wishlist_t
		And Insert Template 7.2/member_t Into member_t

	When Open / Wait[Loaded] 
		And Click wish_list2 Wait[Loaded] 
		And Enter
			| email_pc@id                   | password1@id |
			| robertnothanks@mailinator.com | 12345678     |
		And Click next Wait[Loaded] 
		And Click wishlist_List[0].dtl_btn Wait[Loaded]
		And Click backbtn Wait[Loaded] 
	    And Click wishlist_List[0].del_btn Wait[Loaded] 

	Then Assert Deleted List wishlist_List
	        | itemName                   |
	        | Hershey Milk Chocolate Bar |

	When Click logout@class Wait[Loaded] 