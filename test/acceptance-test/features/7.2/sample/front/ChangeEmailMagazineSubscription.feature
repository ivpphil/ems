﻿@V7.2PC
Feature: ChangeEmailMagazineSubscription
			Change Email Magazine Subscription


#----------------------------------------------
Scenario: Change Email Magazine Subscription
#----------------------------------------------

	# This scenario is used to change the subscription of email magazine.

	Given Insert Template member2_t Into member_t

	When Open / Wait[Loaded]
	And Click login Wait[Loaded]
	And Enter
		| email@name                  | passwd@name |
		| arieltest123@mailinator.com | 12345678    |
	And Click login_btn Wait[Loaded]
	And Click change_mag_sub Wait[Loaded]
	And Enter
		| Field      | Value |
		| m_flg@name | 1     |
	And Click next_step_btn Wait[Loaded]
	And Click change_btn Wait[Loaded]
	And Click rtn_mypage_btn Wait[Loaded]
	And Click left_logout_btn Wait[Loaded]

