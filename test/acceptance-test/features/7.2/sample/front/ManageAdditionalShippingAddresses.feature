﻿@V7.2PC
Feature: ManageAdditionalShippingAddresses
			Adding Other Shipping Recipient
			Modifying Other Shipping Recipient
			Deleting Other Shipping Recipient
			Asserting the List of Other Shipping Recipient

#----------------------------------------------------------
Scenario: Adding Other Shipping Recipient
#----------------------------------------------------------

# When Open to Front page| click LOGIN button then enter the email address and password.
# Click Manage Additional Shipping Addresses then click Register Another Shipping Recipient button and enter information of the recipient and will be asserted.
#		(Other Shipping Recipient| Name| Place| Zip| Country| Telephone Number.  '*' is mandatory input.)
# Then click the NEXT STEP button and confirmation page will show then assert data. And Click Register button.
# Completion page will show then click the Return to other Shipping Recipient List to return to the Other Shipping Recipient List.
# Click EDIT button and assert the data. Return to previous page then go back to my page and logout.


	Given Delete From addressbook_t
		| mcode |
		| 30001520      |

		When Open / Wait[Loaded]
		And Click login Wait[Loaded]
		And Enter
		| email@name                  | passwd@name |
		| arieltest123@mailinator.com | 12345678    |
		And Click login_btn Wait[Loaded]
		And Click mng_add_ship_address Wait[Loaded]
		And Click register_another_recipient Wait[Loaded]
		And Enter
			| Field        | Value                       |
			| address_name | IVP |
			| add_lname    | Galura                      |
			| add_fname    | Nyllene                     |
			| add_lnamek   | ガルラ                         |
			| add_fnamek   | ニリン                         |
			| add_tel      | 123456789                  |
			| add_zip      | 650-0012                    |
		And Click zip_flg2 Wait[Loaded]
		And Enter
			| Field        | Value |
			| add_maddress |       |	
		Then Assert Elements
			| Field        | Value                       |
			| address_name | IVP |
			| add_lname    | Galura                      |
			| add_fname    | Nyllene                     |
			| add_lnamek   | ガルラ                         |
			| add_fnamek   | ニリン                         |
			| add_tel      | 123456789                  |
			| add_zip      | 650-0012                    |
			| add_pref     | 28                          |
			| add_address  | 神戸市中央区                      |
			| add_taddress | 北長狭通                        |
			| add_maddress |                             |
		When Click next_step_btn Wait[Loaded]
		Then Assert Elements
		    | Field           | Value                       |
		    | other_addr_name | IVP |
		    | other_fullname  | Galura Nyllene              |
		    | other_fullnamek | ガルラ ニリン                     |
		    | other_tel       | 123456789                  |
		    | other_zip       | 650-0012                    |
		    | other_pref      | 兵庫県                         |
		    | other_address   | 神戸市中央区                      |
		    | other_taddress  | 北長狭通                        |
		When Click register_btn Wait[Loaded]
		And Click return_shipping_list Wait[Loaded]
		And Click edit_btn Wait[Loaded]
		Then Assert Elements
			| Field           | Value                       |
			| address_name@id | IVP |
			| add_lname@id    | Galura                      |
			| add_fname@id    | Nyllene                     |
			| add_lnamek@id   | ガルラ                         |
			| add_fnamek@id   | ニリン                         |
			| add_tel@id      | 123456789                  |
			| add_zip@id      | 650-0012                    |
			| add_pref@id     | 28                          |
			| add_address@id  | 神戸市中央区                      |
			| add_taddress@id | 北長狭通                        |
			| add_maddress@id |                             |
		When Click left_logout_btn Wait[Loaded]
		    

#----------------------------------------------------------
Scenario: Modifying Other Shipping Recipient
#----------------------------------------------------------

# When Open to Front page| click login button then enter the email address and password.
# Click Manage Additional Shipping Addresses then click EDIT button of the recipient you want to modify.
# Assert the data then Edit the values you want to edit. (Other Shipping Recipient| Name| Place| Zip| Country| Telephone Number.  '*' is mandatory input.)
# Then click the NEXT STEP button and confirmation page will show then assert data. And Click Register button.
# Completion page will show then click the Return to other Shipping Recipient List to return to the Other Shipping Recipient List.
# Click Edit button and assert the data. Return to previous page then go back to my page and logout.



	Given Delete From addressbook_t
		| mcode    |
		| 30001520 |
	And Insert Template addressbook2_t Into addressbook_t

	When Open / Wait[Loaded]
		And Click login Wait[Loaded]
		And Enter
		| email@name                    | passwd@name |
		| arieltest123@mailinator.com | 12345678  |
		And Click login_btn Wait[Loaded]
		And Click mng_add_ship_address Wait[Loaded]
		And Click edit_btn Wait[Loaded]
		Then Assert Elements
			| Field           | Value     |
			| address_name@id | IVP       |
			| add_lname@id    | Galura    |
			| add_fname@id    | Nyllene   |
			| add_lnamek@id   | ガルラ       |
			| add_fnamek@id   | ニリン       |
			| add_tel@id      | 123456789 |
			| add_zip@id      | 650-0012  |
			| add_pref@id     | 28        |
			| add_address@id  | 神戸市中央区    |
			| add_taddress@id | 北長狭通      |
			| add_maddress@id |           |
		When Enter
			| Field           | Value     |
			| address_name@id | IVP       |
			| add_lname@id    | Hugo      |
			| add_fname@id    | Alyanna   |
			| add_lnamek@id   | ユゴ        |
			| add_fnamek@id   | アリャナ      |
			| add_tel@id      | 222222222 |
			| add_zip@id      | 650-0012  |
			| add_pref@id     | 28        |
			| add_address@id  | 神戸市中央区    |
			| add_taddress@id | 北長狭通      |
			| add_maddress@id |           |
		When Click next_step_btn Wait[Loaded]
		Then Assert Elements
			| Field                  | Value        |
			| other_shipping_address | IVP          |
			| other_fullname         | Hugo Alyanna |
			| other_fullnamek        | ユゴ アリャナ      |
			| other_tel              | 222222222    |
			| other_zip              | 650-0012     |
			| other_pref             | 兵庫県          |
			| other_address          | 神戸市中央区       |
			| other_taddress         | 北長狭通         |		
		When Click register_btn Wait[Loaded]
		And Click rtn_shipping_list Wait[Loaded]
		And Click rtn_home_page Wait[Loaded]
		And Click left_logout_btn Wait[Loaded]


#----------------------------------------------------------
Scenario: Asserting the List of Other Shipping Recipient
#----------------------------------------------------------

	# When Open to Front page| click login button then enter the email address and password.
	# Click Manage Additional Shipping Addresses and assert all other recipient list.
	# If there are more pages| click the NEXT button to assert.
	# Return to previous page then go back to my page and logout.


	Given Delete From addressbook_t
		| mcode    |
		| 30001520 |
	And Insert Template addressbook_list2_t Into addressbook_t
	When Open / Wait[Loaded]
		And Click login Wait[Loaded]
		And Enter
			| email@name                    | passwd@name |
			| arieltest123@mailinator.com | 12345678  |
		And Click login_btn Wait[Loaded]
		And Click mng_add_ship_address Wait[Loaded]
		Then Assert List other_shipping_list
			| l_address_name | l_fullname       | l_pref | l_address | l_taddress |
			| ITG            | Santos Kristine  | 福岡県    | 北九州市若松区   | 高須南        |
			| ITG            | Romeo Eduard     | 福岡県    | 北九州市若松区   | 高須南        |
			| IVP            | Gomez Edgar      | 兵庫県    | 神戸市中央区    | 北長狭通       |
			| IVP            | Gutierrez Zen    | 兵庫県    | 神戸市中央区    | 北長狭通       |
			| Codev          | Panganiban Khaye | 兵庫県    | 神戸市中央区    | 下山手通       |
			| IVP            | Punongbayan Joy  | 兵庫県    | 神戸市中央区    | 北長狭通       |
			| IVP            | Trajeco Maria    | 兵庫県    | 神戸市中央区    | 北長狭通       |
			| ITG            | De Vera Lourdes  | 福岡県    | 北九州市若松区   | 高須南        |
			| Codev          | Briagas Mark     | 兵庫県    | 神戸市中央区    | 下山手通       |
			| IVP            | Alcantara Macky  | 兵庫県    | 神戸市中央区    | 北長狭通       |
			| IVP            | Manzano Lucky    | 兵庫県    | 神戸市中央区    | 北長狭通       |
			| ITG            | Piper Sean       | 福岡県    | 北九州市若松区   | 高須南        |
			| IVP            | Zack Efron       | 兵庫県    | 神戸市中央区    | 北長狭通       |
			| IVP            | Gavino Franz     | 兵庫県    | 神戸市中央区    | 北長狭通       |
			| Codev          | Santiago Matthew | 兵庫県    | 神戸市中央区    | 下山手通       |
			| Codev          | Ramos Michael     | 兵庫県    | 神戸市中央区    | 下山手通       |
			| ITG            | Smith John       | 福岡県    | 北九州市若松区   | 高須南        |
			| ITG            | Palma Nikka      | 福岡県    | 北九州市若松区   | 高須南        |
			| IVP            | Galura Nyllene   | 兵庫県    | 芦屋市       | 川西町        |
			| IVP            | Perez Ann        | 兵庫県    | 神戸市中央区    | 北長狭通       |

		When Click next_page_btn Wait[Loaded]
		Then Assert List other_shipping_list
			| l_address_name | l_fullname   | l_pref | l_address | l_taddress |
			| IVP            | Hugo Alyanna | 兵庫県    | 神戸市中央区    | 北長狭通       |
		When Click rtn_home_page Wait[Loaded]
		And Click left_logout_btn Wait[Loaded]
