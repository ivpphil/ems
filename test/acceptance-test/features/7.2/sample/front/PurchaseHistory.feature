﻿@V7.2PC
Feature: PurchaseHistory
			Order Product
			Assert Purchase History
	

#------------------------------------------
Scenario: Order Product
#------------------------------------------

# This is used to order product. 

# Go to product detail page then click add to cart then click continue shopping button.
# Then go to the next product page you want then add to cart.
# Then enter email address and password to go to next page. Check "I Agree" then click next.
# Enter data on the fields then submit.
# Go again to the history list page then click details button of the order you made then assert all data.

Given Insert Template g_master2_t Into g_master_t
	And Insert Template s_master2_t Into s_master_t
	And Insert Template price2_t Into price_t
	And Insert Template stock2_t Into stock_t
	And Insert Template wh_stock2_t Into wh_stock_t

	When Open / Wait[Loaded]
	And Click login Wait[Loaded]
	And Enter
		| email@name                  | passwd@name |
		| arieltest123@mailinator.com | 12345678    |
	And Click login_btn Wait[Loaded]
	And Click product_list Wait[Loaded]
	And Click productList[5].product_detail_btn Wait[Loaded]
	And Enter
		| matrix_form12 | amount12 |
		| SETB1         | 1        |
	And Click add_cart Wait[Loaded]
	And Click cont_shopping_btn Wait[Loaded]
	And Click productList[6].product_detail_btn Wait[Loaded]
	And Enter
		| matrix_form12 | amount12 |
		| SETA1         | 1        |
	And Click add_cart Wait[Loaded]
	And Click recalculation_btn Wait[Loaded]
	And Enter
		| Field       | Value                       |
		| email@name  | arieltest123@mailinator.com |
		| passwd@name | 12345678                    |
	And Click customer_login_btn Wait[Loaded]
	And Enter
		| pri_chk@id |
		| 1          |
	And Click entry_submit Wait[Loaded]
	And Enter
		| Field             | Value |
		| deliv_method@name | 1     |
		| pay@name          | 2     |
		| send_address      | 1     |
		| delivery_date     | 0     |
		| delivery_time     | 0     |
		| wrap_chk          | 0     |
		| memo2@id          |       |
	And Click entry2_submit Wait[Loaded]
	And Click entry3_submit Wait[Loaded]
	And Click mypage Wait[Loaded]
	And Click srch_purchase_hist Wait[Loaded]
	And Click detailButtons[0].detail_btn Wait[Loaded]
	Then Assert Elements
		| Field                             | Value         |
		| d_order_status                    | 新着注文      |
		| d_order_destination1              | 〒650-0012   |
		| d_pref                            | 兵庫県        |
		| d_address                         | 神戸市中央区  |
		| d_taddress                        | 北長狭通      |
		| d_order_fullname                  | Lopez Ariel   |
		| d_order_tel                       | 123456789      |
		| d_order_delivery_date             | 指定しない     |
		| d_order_delivery_time             | 指定しない     |
		| d_order_payment_method            | 銀行振込       |
		| d_order_wrap                      | 希望しない     |
		| d_order_memo2                     |                |
		| d_order_partial_shop_contact_info |                |
	And Assert List d_order_prodInfoList
		| d_order_sname           | d_order_price | d_order_quantity | d_order_prodPrice |
		| Included Product 1 I  | 201円          | 1                | 201円              |
		| Set Products Test 1 one | 201円          | 1                | 201円              |
	And Assert Elements
		| Field           | Value |
		| d_amount_total  | 2点    |
		| d_subtotal      | 402円  |
		| d_postage_price | 300円  |
		| d_etc           | 200円  |
		| d_tax           | 32円   |
		| d_grand_total   | 934円 |
	When Click rtn_mypage_btn Wait[Loaded]
	And Click left_logout_btn Wait[Loaded]



#------------------------------------------
Scenario: Assert Purchase History
#------------------------------------------
	
	# This scenario is used to assert the purchase history of the user.

	# Delete old test data for the member
	# Insert order data to DB
	# Assert list


	Given Delete From d_master_t
		| mcode          |
		| 30001520         |
		And Delete From member_t
         | email |
         |  arieltest123@mailinator.com     |
	And Insert Template member2_t Into member_t
	And Insert Template d_master2_t Into d_master_t
	And Insert Template ds_master2_t Into ds_master_t
	And Insert Template ds_set2_t Into ds_set_t
	When Open / Wait[Loaded]
	And Click login Wait[Loaded]
	And Enter
		| email@name                    | passwd@name |
		| arieltest123@mailinator.com | 12345678  |
	And Click login_btn Wait[Loaded]
	And Click srch_purchase_hist Wait[Loaded]
	And Enter
		| Field            | Value      |
		| datepickerTo@id | 2016/04/01 |
		| datepickerFrom@id   | 2016/04/30 |
	And Click date_search_btn Wait[Loaded]
	Then Assert List purchaseHistList
        | order_date | order_amount | order_status |
        | 2016年4月6日 | 934円         | 新着注文         |
        | 2016年4月6日 | 934円         | 新着注文         |
        | 2016年4月6日 | 934円         | 新着注文         |
        | 2016年4月6日 | 934円         | 新着注文         |
		And Assert List snameList
			| l_sname0             | l_attrib10 |
			| Included Product 1  | I         |
			| Included Product 1  | I         |
			| Included Product 1  | I         |
		And Assert List snameList
			| l_sname1            | l_attrib11 |
			| Set Products Test 1 | one       |
			| Set Products Test 1 | one       |
			| Set Products Test 1 | one       |			
	When Click h_prev_btn Wait[Loaded]
	And Click left_logout_btn Wait[Loaded]

