﻿@V7.2PC
Feature: CustomerRegistration
			Customer Registration - Front
			Customer Modification - Front

#------------------------------------------
Scenario: Customer Registration - Front
#------------------------------------------

	# This scenario is used to register new customer.

	Given Delete From member_t
		| email                       |
		| arieltest123@mailinator.com |

	When Open / Wait[Loaded]
		And Click new_customer_registration Wait[Loaded]
		And Enter
			| Field         | Value                       |
			| r_lname       | Lopez                       |
			| r_fname       | Ariel                       |
			| k_lname       | ロペズ                         |
			| k_fname       | アリエル                        |
			| r_compname    | IVP                         |
			| k_compname    | アイヴィピ                       |
			| r_division    |                             |
			| k_division    |                             |
			| email1        | arieltest123@mailinator.com |
			| mformat       | 1                           |
			| email_confirm | arieltest123@mailinator.com |
			| tel           | 123456789                   |
			| fax           | 111111111                   |
			| zip           | 650-0012                    |
			And Click auto_input Wait[Loaded]
			And Enter
			| Field          | Value    |
			| maddress       |          |
			| birthday_y     | 1990     |
			| birthday_m     | 3        |
			| birthday_d     | 18       |
			| sex            | 1        |
			| job            | 1        |
			| passwd1        | 12345678 |
			| passwd_confirm | 12345678 |
			| ques           | 1        |
			| ans            | ママ       |
			| m_flg          | 1        |
			| pri_chk@id     | 1        |
		And Click proceed_btn Wait[Loaded]
		Then Assert Elements
			| Field        | Value                       |
			| c_r_fullname | Lopez Ariel                 |
			| c_k_fullname | ロペズ アリエル                    |
			| c_r_compname | IVP                         |
			| c_k_compname | アイヴィピ                       |
			| c_r_division |                             |
			| c_k_division |                             |
			| c_email      | arieltest123@mailinator.com |
			| c_mformat    | PCメール形式                     |
			| c_tel        | 123456789                   |
			| c_fax        | 111111111                   |
			| c_zip        | 650-0012                    |
			| c_address    | 神戸市中央区                      |
			| c_taddress   | 北長狭通                        |
			| c_birthdate  | 1990年3月18日                  |
			| c_sex        | 男性                          |
			| c_job        | 会社員                         |
			| c_ques       | 母親の旧姓は？                     |
			| c_ans        | ママ                          |
			| c_m_flg      | 配信する                        |
		When Click register_btn Wait[Loaded]
		And Click back_btn Wait[Loaded]
		And Click login Wait[Loaded]
		And Enter
		| email@name                  | passwd@name |
		| arieltest123@mailinator.com | 12345678    |
		And Click login_btn Wait[Loaded]
		And Click edit_cust_info Wait[Loaded]
		Then Assert Elements
			| Field            | Value                       |
			| e_r_lname        | Lopez                       |
			| e_r_fname        | Ariel                       |
			| e_k_lname        | ロペズ                         |
			| e_k_fname        | アリエル                        |
			| e_r_compname     | IVP                         |
			| e_k_compname     | アイヴィピ                       |
			| e_r_division     |                             |
			| e_k_division     |                             |
			| e_email          | arieltest123@mailinator.com |
			| e_mformat        | 1                           |
			| e_email_confirm  | arieltest123@mailinator.com |
			| e_tel            | 123456789                   |
			| e_fax            | 111111111                   |
			| e_zip            | 650-0012                    |
			| e_pref           | 28                          |
			| e_address        | 神戸市中央区                      |
			| e_taddress       | 北長狭通                        |
			| e_maddress       |                             |
			| e_birthday_y     | 1990                        |
			| e_birthday_m     | 3                           |
			| e_birthday_d     | 18                          |
			| e_sex            | 1                           |
			| e_job            | 1                           |
			| e_passwd         |                             |
			| e_passwd_confirm |                             |
			| e_ques           | 1                           |
			| e_ans            | ママ                          |

		When Click mypage_btn Wait[Loaded]
		And Click left_logout_btn Wait[Loaded]


#------------------------------------------
Scenario: Customer Modification - Front
#------------------------------------------

	# This scenario is used to modify the customer information.

	Given Delete From member_t
		| email                       |
		| arieltest123@mailinator.com |
		And Insert Template member2_t Into member_t
	When Open / Wait[Loaded]
		And Click login Wait[Loaded]
		And Enter
		| email@name                  | passwd@name |
		| arieltest123@mailinator.com | 12345678    |
		And Click login_btn Wait[Loaded]
		And Click edit_cust_info Wait[Loaded]
		Then Assert Elements
			| Field            | Value                       |
			| e_r_lname        | Lopez                       |
			| e_r_fname        | Ariel                       |
			| e_k_lname        | ロペズ                         |
			| e_k_fname        | アリエル                        |
			| e_r_compname     | IVP                         |
			| e_k_compname     | アイヴィピ                       |
			| e_r_division     |                             |
			| e_k_division     |                             |
			| e_email          | arieltest123@mailinator.com |
			| e_mformat        | 1                           |
			| e_email_confirm  | arieltest123@mailinator.com |
			| e_tel            | 123456789                   |
			| e_fax            | 111111111                   |
			| e_zip            | 650-0012                    |
			| e_pref           | 28                          |
			| e_address        | 神戸市中央区                      |
			| e_taddress       | 北長狭通                        |
			| e_maddress       |                             |
			| e_birthday_y     | 1990                        |
			| e_birthday_m     | 3                           |
			| e_birthday_d     | 18                          |
			| e_sex            | 1                           |
			| e_job            | 1                           |
			| e_passwd         |                             |
			| e_passwd_confirm |                             |
			| e_ques           | 1                           |
			| e_ans            | ママ                         |
		When Enter
			| Field            | Value                       |
			| e_r_lname        | Lopez                       |
			| e_r_fname        | Ariel                       |
			| e_k_lname        | ロペズ                         |
			| e_k_fname        | アリエル                        |
			| e_r_compname     | ABC                         |
			| e_k_compname     | エイビシ                        |
			| e_r_division     |                             |
			| e_k_division     |                             |
			| e_email          | arieltest123@mailinator.com |
			| e_mformat        | 1                           |
			| e_email_confirm  | arieltest123@mailinator.com |
			| e_tel            | 987654321                   |
			| e_fax            | 222222222                   |
			| e_zip            | 650-0012                    |
			| e_pref           | 28                          |
			| e_address        | 神戸市中央区                      |
			| e_taddress       | 北長狭通                        |
			| e_maddress       |                             |
			| e_birthday_y     | 1990                        |
			| e_birthday_m     | 3                           |
			| e_birthday_d     | 18                          |
			| e_sex            | 1                           |
			| e_job            | 8                           |
			| e_passwd         |                             |
			| e_passwd_confirm |                             |
			| e_ques           | 1                           |
			| e_ans            | ママ                          |
		When Click next_btn Wait[Loaded]
		Then Assert Elements
			| Field        | Value                       |
			| c_r_fullname | Lopez Ariel                 |
			| c_k_fullname | ロペズ アリエル                    |
			| c_r_compname | ABC                         |
			| c_k_compname | エイビシ                        |
			| c_r_division |                             |
			| c_k_division |                             |
			| c_email      | arieltest123@mailinator.com |
			| c_mformat    | PCメール形式                     |
			| c_tel        | 987654321                   |
			| c_fax        | 222222222                   |
			| c_zip        | 650-0012                    |
			| c_address    | 神戸市中央区                      |
			| c_taddress   | 北長狭通                        |
			| c_birthdate  | 1990年3月18日                  |
			| c_sex        | 男性                          |
			| c_job        | 教職                          |
			| c_ques       | 母親の旧姓は？                     |
			| c_ans        | ママ                          |
		When Click register_btn Wait[Loaded]
		And Click return_mypage_btn Wait[Loaded]
		And Click left_logout_btn Wait[Loaded]

