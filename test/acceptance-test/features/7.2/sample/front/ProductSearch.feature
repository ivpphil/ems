﻿@V7.2PC

Feature: Front Product Search
	Contains test cases for Searching Product in the front side

#-----------------------------------------------------------------------------------------
Scenario: Product Search Front
#First, it inserts the ff. templates in the database
#It opens the ERS(front)
#It searches the product by the keyword and click the search button
#It checks the list of products that appeared
#Clicks the details of the product
#Checks the information of the product
#-----------------------------------------------------------------------------------------
	Given Insert Template 7.2/cate1_t Into cate1_t
		And Insert Template 7.2/cate2_t Into cate2_t
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t
	When Open / Wait[Loaded]
		And Enter
		| s_keyword@name |
		| CARIN |
		And Click search_submit-s@name Wait[Loaded]

	Then Assert List productList
		| gname            | price                     | description_for_list          | thumbnail                                                   |
		| CARIN SUNGLASSES | 4,000円(税抜)～| Carin Eyeglasses is the best. | <%=setup.pc_sec_url%>images/simg//GCARIN101_01.jpg?width=160 |

	When Click productList[0].thumbnail Wait[Loaded] 	 
	Then Assert Elements
		| breadcrumb_scode | title_sname      | detail_sname     | detail_scode | normal_price | detail_img                                                  | wBreak                                                                                                                                                                                                                                                                                                                                                                                  |
		| BLACK SUNGLASSES | BLACK SUNGLASSES | BLACK SUNGLASSES | CARIN001     | 5,000円       | <%=setup.pc_sec_url%>images/bimg/SCARIN001_01.jpg?width=264 | Carried nothing on am warrant towards. Polite in of in oh needed itself silent course. Assistance travelling so especially do prosperous appearance mr no celebrated. Wanted easily in my called formed suffer. Songs hoped sense as taken ye mirth at. Believe fat how six drawing pursuit minutes far. Same do seen head am part it dear open to. Whatever may scarcely judgment had. |

	When Click backbtn Wait[Loaded] 	 
		And Enter
		| s_keyword@name | s_price1@name | s_price2@name | sort@name | s_outstock@name |
		| chocolate      | 50            | 500           | 2         | 0               |
		And Click searchBtn Wait[Loaded]
	Then Assert List productList
		| gname       | price       | description_for_list                                                                                                                                                                                                                                                                                                                                        | thumbnail                                                   |
		| Hershey Bar | 100円(税抜) | The Hersheys Milk Chocolate Bar (commonly called the Hersheys Bar) is the flagship chocolate bar manufactured by the Hershey Company. It is often referred by Hershey as The Great American Chocolate Bar. The Hershey Milk Chocolate Bar was first sold in 1900 followed by the Hersheys Milk Chocolate with Almonds variety beginning production in 1908. | <%=setup.pc_sec_url%>images/simg//GHERSHEY_01.jpg?width=160 |
	
	When Click productList[0].thumbnail Wait[Loaded] 	 
	Then Assert Elements
		| breadcrumb_scode           | title_sname                | detail_sname               | detail_scode | normal_price | detail_img                                             | wBreak                                                                                                                                                                                                                                                                                                                                                      |
		| Hershey Milk Chocolate Bar | Hershey Milk Chocolate Bar | Hershey Milk Chocolate Bar | HMC          | 100円         | <%=setup.pc_sec_url%>images/bimg/SHMC_01.jpg?width=264 | The Hersheys Milk Chocolate Bar (commonly called the Hersheys Bar) is the flagship chocolate bar manufactured by the Hershey Company. It is often referred by Hershey as The Great American Chocolate Bar. The Hershey Milk Chocolate Bar was first sold in 1900 followed by the Hersheys Milk Chocolate with Almonds variety beginning production in 1908. |
	

#-----------------------------------------------------------------------------------------
	Scenario: Advanced Product Search
#First, it inserts the ff. templates in the database
#It opens the ERS(front)
#It clicks the advanced product search and fill in the search criterias
#It checks the list of products that appeared
#Clicks the details of the product
#Checks the information of the product
#-----------------------------------------------------------------------------------------
	Given Insert Template 7.2/cate1_t Into cate1_t
		And Insert Template 7.2/cate2_t Into cate2_t
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t
	When Open / Wait[Loaded] 	 
		And Click detail Wait[Loaded] 	 
		And Enter
		| s_scode@name | s_sname@name     | s_keyword@name | s_price1@name | s_price2@name | sort@name | s_outstock@name |
		| CARIN001     | CARIN SUNGLASSES | CARIN          |               |               | 1         | 1               |
		And Enter
		| s_cate1@name | s_cate3@name | s_cate4@name | s_cate5@name |
		|              |              |              |              |
	And Click search_item@name Wait[Loaded] 
	Then Assert List productList
		| gname            | price                     | description_for_list          | thumbnail                                                   |
		| CARIN SUNGLASSES | 4,000円(税抜)～| Carin Eyeglasses is the best. | <%=setup.pc_sec_url%>images/simg//GCARIN101_01.jpg?width=160 |

	When Click productList[0].thumbnail Wait[Loaded] 	 
	Then Assert Elements
		| breadcrumb_scode | title_sname      | detail_sname     | detail_scode | normal_price | detail_img                                                  | wBreak                                                                                                                                                                                                                                                                                                                                                                                  |
		| BLACK SUNGLASSES | BLACK SUNGLASSES | BLACK SUNGLASSES | CARIN001     | 5,000円       | <%=setup.pc_sec_url%>images/bimg/SCARIN001_01.jpg?width=264 | Carried nothing on am warrant towards. Polite in of in oh needed itself silent course. Assistance travelling so especially do prosperous appearance mr no celebrated. Wanted easily in my called formed suffer. Songs hoped sense as taken ye mirth at. Believe fat how six drawing pursuit minutes far. Same do seen head am part it dear open to. Whatever may scarcely judgment had. |
