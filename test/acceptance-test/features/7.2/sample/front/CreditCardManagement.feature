﻿@V7.2PC

Feature: CreditCardManagement
			Credit Card Registration
			Credit Card Modification
			Credit Card Deletion

#------------------------------------------
Scenario: Credit Card Registration
#------------------------------------------

# This scenario is used to register credit card.

# Delete old test data for the customer card.
# Enter data then assert.

	Given Insert Template member2_t Into member_t
	And Delete From member_card_t
         | mcode |
         | 30001520      |


	When Open / Wait[Loaded]
		And Click login Wait[Loaded]
		And Enter
		| email@name                    | passwd@name |
		| arieltest123@mailinator.com | 12345678  |
		And Click login_btn Wait[Loaded]
		And Click edit_credit_card_info Wait[Loaded]
		And Enter
			| Field      | Value            |
			| card_type  | 1                |
			| card_no    | 4111111111111111 |
			| validity_y | 2020             |
			| validity_m | 5                |
		And Click next_step_btn Wait[Loaded]
		Then Assert Elements
			| Field             | Value            |
			| confirm_card_type | VISA             |
			| confirm_cardno    | ************1111 |
			| confirm_validity  | 2020年5月          |
		When Click entry_submit_btn Wait[Loaded]
		And Click rtn_mypage_btn Wait[Loaded]

		And Click edit_credit_card_info Wait[Loaded]
		And Enter
			| Field      | Value            |
			| card_type  | 1                |
			| card_no    | 4111111111111112 |
			| validity_y | 2025             |
			| validity_m | 10                |
		And Click next_step_btn Wait[Loaded]
		Then Assert Elements
			| Field             | Value            |
			| confirm_card_type | VISA             |
			| confirm_cardno    | ************1112 |
			| confirm_validity  | 2025年10月          |
		When Click entry_submit_btn Wait[Loaded]
		And Click rtn_mypage_btn Wait[Loaded]
		And Click left_logout_btn Wait[Loaded]


#------------------------------------------
Scenario: Credit Card Modification
#------------------------------------------
	
	# This scenario is used to modify the credit card information.
	
	Given Insert Template member2_t Into member_t
	And Delete From member_card_t
         | mcode    |
         | 30001520 |

	When Open / Wait[Loaded]
		And Click login Wait[Loaded]
		And Enter
		| email@name                  | passwd@name |
		| arieltest123@mailinator.com | 12345678    |
		And Click login_btn Wait[Loaded]
		And Click edit_credit_card_info Wait[Loaded]
		And Enter
			| Field      | Value            |
			| card_type  | 1                |
			| card_no    | 4111111111111111 |
			| validity_y | 2020             |
			| validity_m | 5                |
		And Click next_step_btn Wait[Loaded]
		#And Wait Until that Element ers_page_loaded@id Exists
		Then Assert Elements
			| Field             | Value            |
			| confirm_card_type | VISA             |
			| confirm_cardno    | ************1111 |
			| confirm_validity  | 2020年5月          |
		When Click entry_submit_btn Wait[Loaded]
		And Click rtn_mypage_btn Wait[Loaded]

		And Click edit_credit_card_info Wait[Loaded]
		And Enter
			| Field      | Value            |
			| card_type  | 1                |
			| card_no    | 4111111111111112 |
			| validity_y | 2025             |
			| validity_m | 10                |
		And Click next_step_btn Wait[Loaded]
		Then Assert Elements
			| Field             | Value            |
			| confirm_card_type | VISA             |
			| confirm_cardno    | ************1112 |
			| confirm_validity  | 2025年10月          |
		When Click entry_submit_btn Wait[Loaded]
		And Click rtn_mypage_btn Wait[Loaded]
		
		And Click edit_credit_card_info Wait[Loaded]
		Then Assert List credit_cardList
			| c_card_type | c_card_no        | c_validity |
			| VISA        | *************111 | 2020年5月    |
			| VISA        | *************112 | 2025年10月   |
		When Click credit_cardList[0].modify_btn Wait[Loaded]
		And Enter
			| Field                        | Value            |
			| record_key_0_card_type@name  | 2                |
			| record_key_0_card_no@name    | 4111111111111111 |
			| record_key_0_validity_y@name | 2025             |
			| record_key_0_validity_m@name | 12               |
		And Click m_modify_btn Wait[Loaded]
		Then Assert Elements
			| Field             | Value            |
			| confirm_card_type | マスターカード          |
			| confirm_cardno    | ************1111 |
			| confirm_validity  | 2025年12月         |
		When Click entry_submit_btn Wait[Loaded]
		And Click rtn_mypage_btn Wait[Loaded]
		And Click edit_credit_card_info Wait[Loaded]
		And Click credit_cardList[1].modify_btn Wait[Loaded]
		And Enter
			| Field                        | Value            |
			| record_key_1_card_type@name  | 3                |
			| record_key_1_card_no@name    | 4111111111111112 |
			| record_key_1_validity_y@name | 2030             |
			| record_key_1_validity_m@name | 6                |
		And Click m_modify_btn Wait[Loaded]
		Then Assert Elements
			| Field             | Value            |
			| confirm_card_type | アメリカン・エキスプレス     |
			| confirm_cardno    | ************1112 |
			| confirm_validity  | 2030年6月          |
		When Click entry_submit_btn Wait[Loaded]
		And Click rtn_mypage_btn Wait[Loaded]
		And Click left_logout_btn Wait[Loaded]


#------------------------------------------
Scenario: Credit Card Deletion
#------------------------------------------

	# This scenario is used to delete the credit card information.

	Given Insert Template member2_t Into member_t
	And Delete From member_card_t
         | mcode    |
         | 30001520 |

	When Open / Wait[Loaded]
		And Click login Wait[Loaded]
		And Enter
		| email@name                  | passwd@name |
		| arieltest123@mailinator.com | 12345678    |
		And Click login_btn Wait[Loaded]
		And Click edit_credit_card_info Wait[Loaded]
		And Enter
			| Field      | Value            |
			| card_type  | 1                |
			| card_no    | 4111111111111111 |
			| validity_y | 2020             |
			| validity_m | 5                |
		And Click next_step_btn Wait[Loaded]
		Then Assert Elements
			| Field             | Value            |
			| confirm_card_type | VISA             |
			| confirm_cardno    | ************1111 |
			| confirm_validity  | 2020年5月          |
		When Click entry_submit_btn Wait[Loaded]
		And Click rtn_mypage_btn Wait[Loaded]

		And Click edit_credit_card_info Wait[Loaded]
		And Enter
			| Field      | Value            |
			| card_type  | 1                |
			| card_no    | 4111111111111112 |
			| validity_y | 2025             |
			| validity_m | 10                |
		And Click next_step_btn Wait[Loaded]
		Then Assert Elements
			| Field             | Value            |
			| confirm_card_type | VISA             |
			| confirm_cardno    | ************1112 |
			| confirm_validity  | 2025年10月          |
		When Click entry_submit_btn Wait[Loaded]
		And Click rtn_mypage_btn Wait[Loaded]

		And Click edit_credit_card_info Wait[Loaded]

		And Click credit_cardList[0].delete_btn Wait[Loaded]
		And Click entry_submit_btn Wait[Loaded]
		And Click rtn_mypage_btn Wait[Loaded]

		And Click edit_credit_card_info Wait[Loaded]
		And Click credit_cardList[0].delete_btn Wait[Loaded]
		And Click entry_submit_btn Wait[Loaded]
		And Click rtn_mypage_btn Wait[Loaded]
		And Click left_logout_btn Wait[Loaded]
