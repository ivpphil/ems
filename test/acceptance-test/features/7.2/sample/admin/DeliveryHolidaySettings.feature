﻿@ers7.2Admin
Feature: DeliveryHolidaySettings
	This feature contains test cases for modifying Delivery Holiday Setting.


Scenario: Check Delivery Holiday Settings
	# This scenario is for registering delivery holiday.
	# Login then open store_calendar.asp
	# Enter value in ddlYearMonth and checkDays then click register
	# Then assert values to check if registered
	Given Insert Template 7.2/calendar_t Into calendar_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/store/asp/store_calendar.asp Wait[Loaded]
			And Enter
			| Field        | Value    |
			| ddlYearMonth | 2016/5   |
			| checkDays    | {1,2,29} |
			And Click register Wait[Loaded]
			And Click dialog_ok_button Wait[Loaded]
			And Click back Wait[Loaded]
			And Enter
			| Field        | Value  |
			| ddlYearMonth | 2016/5 |
		Then Assert Elements
			| Field        | Value    |
			| ddlYearMonth | 2016/5   |
			| checkDays    | {1,2,29} |

Scenario: Uncheck Delivery Holiday Settings
	# This scenario is for deleting delivery holiday.
	# Login then open store_calendar.asp
	# Delete value in checkDays then click register
	# Then assert values to check if modified
	Given Insert Template 7.2/calendar_t Into calendar_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/store/asp/store_calendar.asp Wait[Loaded]
			And Enter
			| Field        | Value  |
			| ddlYearMonth | 2016/5 |
			| checkDays    |        |
			And Click register Wait[Loaded]
			And Click dialog_ok_button Wait[Loaded]
			And Click back Wait[Loaded]
			And Enter
			| Field        | Value  |
			| ddlYearMonth | 2016/5 |
		Then Assert Elements
			| Field        | Value  |
			| ddlYearMonth | 2016/5 |
			| checkDays    |        |
  
