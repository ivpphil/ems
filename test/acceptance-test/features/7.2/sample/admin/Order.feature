﻿@V7.2Admin
Feature: Order
	This feature contain test case for Order Registration, Search/Check and Modification. (ERS v7.2)

#--------------------------------------------------------------------------------------
Scenario: Order Registration
#--------------------------------------------------------------------------------------
#This test case opens the Order Registration as an Admin
#It deletes the data in the db table: wh_order_t and wh_order_info_t with supplier_code: IVP
#It inserts the info from product and supplier templates into the database
#It searches the product by supplier code
#Clicks the search button and the system will display the list of products under 'IVP' supplier
#It clicks the bulk button in date, stock and order checkbox
#It inserts value in the ff. fields: amount, date, auto-update of stock and order
#It clicks the confirm button then check if the order made is correct then clicks the register button
#It goes to completion page then asserts the order list
#It clicks the pdf button then closes it
#Admin logouts
#NOTE: NO ASSERT IN PDF FILE YET


	Given Delete From wh_order_t
			| supplier_code |
			| KAWAII        |
		And Delete From wh_order_info_t
			| supplier_code |
			| KAWAII        |
		And Insert Template 7.2/wh_supplier_t Into wh_supplier_t
		And Insert Template 7.2/cate1_t Into cate1_t
		And Insert Template 7.2/cate2_t Into cate2_t
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t
	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/warehouse/asp/order_search.asp Wait[Loaded] 
		And Enter
			| Field                   | Value  |
			| s_intime_less_than@name |        |
			| s_supplier_code@name    | KAWAII |
			| s_sname@name            |        |
			| s_scode@name            |        |
			| s_wh_order_type@name    |        |
		And Click search_btn Wait[Loaded] 
		And Enter
			|date_bulk|
			|2016/03/11|
		And Click date_bulk_btn Wait[Loaded] 
		And Click stock_chk Wait[Loaded] 
		And Click order_chk Wait[Loaded] 
		And Enter List supp_prodList
			| amount | schedule_date | up_stock | check_order |
			| 5      | 2016/03/11    | 0        | 1           |
			| 6      | 2016/03/11    | 0        | 1           |
		And Click go_confirm@name Wait[Loaded] 

	Then Assert List orderList
	| supplier_code | supplier_name      | scode | maker_scode | sname               | cost_price | w_wh_order_type | stock | wh_stock | base_amount | order_amount | stock_alert_amount | amount | schedule_date | w_up_stock |
	| KAWAII        | The Cutest Company | CH100 | CH100       | Cow Head Fresh Milk | 500円       | 在庫            | 202   | 108      | 2           | 5            |                    | 5      | 2016/03/11    | 更新しない      |
	| KAWAII        | The Cutest Company | CH101 | CH101       | Cow Head Strawberry | 500円       | 受注発注        | 188   | 99      | 0           | 6            |                    | 6      | 2016/03/11    | 更新しない      |

	When Click order_btn Wait[Loaded] 
	Then Assert List order_list
	| supplier_code | supplier_name      |
	| KAWAII        | The Cutest Company |

	#When Click pdf_btn PopupWait /admin/top/warehouse/asp/order_list_pdf.asp
	#And Close Popup Window

#--------------------------------------------------------------------------------------
Scenario: Order Search
#--------------------------------------------------------------------------------------
#This test case opens the Order Search as an Admin
#It deletes the data in the db table: wh_order_t and wh_order_info_t with supplier_code: IVP
#It inserts the order and product templates into the database
#It searches the product by supplier code
#Clicks the search button and the system will display the list of products under 'IVP' supplier
#It checks order list under that supplier
#It clicks the order in the first index and asserts the data

Given Insert Template 7.2/wh_supplier_t Into wh_supplier_t
		And Insert Template 7.2/wh_order_t Into wh_order_t
		And Insert Template 7.2/wh_order_info_t Into wh_order_info_t
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t

	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/warehouse/asp/past_order_search.asp Wait[Loaded] 
	And Enter
		| Field                  | Value                |
		| s_orderdate_from@name  |                      |
		| s_orderdate_to@name    |                      |
		| s_order_no@name        | S0000000000000000143 |
		| s_supplier_code@name   |                      |
		| s_supplier_name@name   |                      |
		| s_scode@name           |                      |
		| s_sname@name           |                      |
		| s_wh_order_status@name |                      |
	And Click search_btn Wait[Loaded] 
	Then Assert List supplier_list
		| order_no             | intime               | supplier_code | supplier_name  | w_wh_order_status |
		| S0000000000000000143 | 1/29/2016 4:20:02 PM | CRN           | THE CARIN CORP | 入庫完了           |
		
	When Click supplier_list[0].dtl_btn Wait[Loaded] 
	Then Assert Elements
		| Field                | Value                |
		| intime               | 1/29/2016 4:20:03 PM |
		| order_no             | S0000000000000000143 |
		| supplier_code        | CRN                  |
		| supplier_name        | THE CARIN CORP       |
		| wh_order_status@name | Storaged             |
		| remarks@name         |                      |
		
		And Assert List order_list
         | scode    | maker_scode | sname            | cost_price | w_wh_order_type | amount | shelf001 | shelf002 | shelf003 | schedule_date | first_intime | last_intime | w_up_stock |
         | CARIN001 | CS111       | BLACK SUNGLASSES | 3,000円     | 在庫            | 2      | 2        | 0        | 0        | 2016/01/29    | 2016/01/29   | 2016/01/29  | 更新しない   |



#--------------------------------------------------------------------------------------
Scenario: Order Modification
#--------------------------------------------------------------------------------------
#This test case opens the Order Search as an Admin
#It deletes the data in the db table: wh_order_t and wh_order_info_t with supplier_code: IVP
#It inserts the order and product templates into the database
#It searches the product by supplier code
#Clicks the search button and the system will display the list of products under 'IVP' supplier
#It clicks the order in the first index
#It modifies the order's status and remarks
#Clicks the update button to save the changes then goes tothe completion page
#Clicks the back button then checks the order details
Given Delete From wh_order_t
			| supplier_code |
			| IVP           |
		And Delete From wh_order_info_t
			| supplier_code |
			| IVP           |
		And Delete From g_master_t
			| gcode          |
			| ASUS-GTXTITANX |
		And Delete From s_master_t
			| gcode          |
			| ASUS-GTXTITANX |
		And Insert Template 7.2/wh_supplier_t Into wh_supplier_t
		And Insert Template 7.2/cate1_t Into cate1_t
		And Insert Template 7.2/cate2_t Into cate2_t
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t
	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/warehouse/asp/past_order_search.asp Wait[Loaded] 
		And Enter
		| Field                  | Value                |
		| s_orderdate_from@name  |                      |
		| s_orderdate_to@name    |                      |
		| s_order_no@name        | S0000000000000000143 |
		| s_supplier_code@name   |                      |
		| s_supplier_name@name   |                      |
		| s_scode@name           |                      |
		| s_sname@name           |                      |
		| s_wh_order_status@name |                      |
		And Click search_btn Wait[Loaded] 
		And Click supplier_list[0].dtl_btn Wait[Loaded] 
		And Enter
		| Field                | Value                   |
		| remarks@name         | CHANGE THE ORDER STATUS |
	
		And Click update_btn Wait[Loaded] 
		And Click dialog_ok_button Wait[Loaded] 
		And Click back_btn Wait[Loaded] 

	Then Assert Elements
		| Field                | Value                   |
		| intime               | 1/29/2016 4:20:03 PM    |
		| order_no             | S0000000000000000143    |
		| supplier_code        | CRN                     |
		| supplier_name        | THE CARIN CORP          |
		| wh_order_status@name | Storaged                |
		| remarks@name         | CHANGE THE ORDER STATUS |
		
	And Assert List order_list
         | scode    | maker_scode | sname            | cost_price | w_wh_order_type | amount | shelf001 | shelf002 | shelf003 | schedule_date | first_intime | last_intime | w_up_stock |
         | CARIN001 | CS111       | BLACK SUNGLASSES | 3,000円     | 在庫            | 2      | 2        | 0        | 0        | 2016/01/29    | 2016/01/29   | 2016/01/29  | 更新しない   |
