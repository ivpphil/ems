﻿@ers7.2Admin
Feature: ProductCategory
	This feature contains test cases for registering/modifying Product Category.


Scenario: Register Category
	# This scenario is for registraion of category
	Given Insert Template 7.2/setup_t Into setup_t
	When Open /admin Wait[Loaded]
		And Enter
	 	| user_login_id | passwd |
	 	| ivpers        | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/item/asp/cate_regist.asp  Wait[Loaded]
	When Enter List categorylist
		| cate_name  | cate_disp |
		| シリーズ-cate1 | 1         |
		| ケア種別-cate2 | 1         |
		| 商品種別-cate3 | 1         |
		| 効果-cate4   | 1         |
		| カテゴリ5      | 1         |
		And Click cate_register Wait[None]
		And Accept Alert Wait[Loaded]
		And Click back Wait[Loaded]
	Then Assert List categorylist
		| cate_name  | cate_disp |
		| シリーズ-cate1 | 1         |
		| ケア種別-cate2 | 1         |
		| 商品種別-cate3 | 1         |
		| 効果-cate4   | 1         |
		| カテゴリ5      | 1         |

Scenario: Edit Caterogy1
	# This scenario is for editing category 1
	Given Insert Template 7.2/cate1_t Into cate1_t
	When Open /admin Wait[Loaded]
		And Enter
	 	| user_login_id | passwd |
	 	| ivpers        | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/item/asp/cate_regist.asp Wait[Loaded]
		And Click cate_edit1 Wait[Loaded]
		When Enter List cateselectionlist
		| cate_delete_selection | cate_id_selection | cate_name_selection | cate_order_selection | cate_disp_selection |
		|                       | 1                 | Cate1 - 1 test                | 1                    | 1                   |
		|                       | 2                 | Cate1 - 2 Test                | 1                    | 1                   |
		|                       | 3                 | カリプソ                | 2                    | 1                   |
		|                       | 4                 | その他                 | 3                    | 1                   |
		|                       | 5               | My Cate Test        | 4                    | 1                   |
		And Click cate_selection_register Wait[None]
		And Accept Alert Wait[Loaded]
		And  Click back Wait[Loaded]
		And Click cate_edit1 Wait[Loaded]
	Then Assert List cateselectionlist
		| cate_id_selection | cate_name_selection | cate_order_selection | cate_disp_selection |
		| 1                 | Cate1 - 1 test                | 1                    | 1                   |
		| 2                 | Cate1 - 2 Test               | 1                    | 1                   |
		| 3                 | カリプソ                | 2                    | 1                   |
		| 4                 | その他                 | 3                    | 1                   |
		| 5                 | My Cate Test        | 4                    | 1                   |

Scenario: Edit Category 2
	# This scenario is for editing category 2
	Given Insert Template 7.2/cate2_t Into cate2_t
	When Open /admin Wait[Loaded]
		And Enter
	 	| user_login_id | passwd  |
		| ivpers       | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/item/asp/cate_regist.asp Wait[Loaded]
		And Click cate_edit2 Wait[Loaded]
		When Enter List cateselectionlist
		| cate_delete_selection | cate_parent_selection | cate_id_selection | cate_name_selection | cate_order_selection | cate_disp_selection |
		| 1                     | 1                     | 1                 | Cate2 - 1 Test      | 1                    | 1                   |
		|                       | 1                     | 2                 | Cate2 - 2 Test      | 1                    | 1                   |
		|                       | 2                     | 3                 | kahit Ano           | 1                    | 1                   |
		| 1                     | 2                     | 4                 | サプリメント              | 4                    | 1                   |
		|                       | 2                     | 5                 | 雑貨                  | 5                    | 1                   |
		|                       | 2                     | 6                 | その他                 | 6                    | 1                   |
		|                       | 2                     | 7                 | AddedCate2          | 7                    | 1                   |
		And Click cate_selection_register Wait[None]
		And Accept Alert Wait[Loaded]
		And  Click back Wait[Loaded]
		And Click cate_edit2 Wait[Loaded]
	Then Assert List cateselectionlist
		 | cate_parent_selection | cate_id_selection | cate_name_selection | cate_order_selection | cate_disp_selection |
		 | 1                     | 2                 | Cate2 - 2 Test      | 1                    | 1                   |
		 | 2                     | 3                 | kahit Ano           | 1                    | 1                   |
		 | 2                     | 5                 | 雑貨                  | 5                    | 1                   |
		 | 2                     | 6                 | その他                 | 6                    | 1                   |
		 | 2                     | 7                 | AddedCate2          | 7                    | 1                   |

Scenario: Edit Category 3
	# This scenario is for editing category 3
	Given Insert Template 7.2/cate3_t Into cate3_t
	When Open /admin Wait[Loaded]
		And Enter
	 	| user_login_id | passwd  |
		| ivpers       | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/item/asp/cate_regist.asp Wait[Loaded]
		And Click cate_edit3 Wait[Loaded]
	When Enter List cateselectionlist
		| cate_delete_selection | cate_parent_selection | cate_id_selection | cate_name_selection | cate_order_selection | cate_disp_selection |
		|                       |                       | 1                 | クレンジング・洗顔           | 1                    | 1                   |
		|                       |                       | 2                 | 化粧水                 | 2                    | 1                   |
		| 1                      |                       | 4                 | スペシャルケア             | 4                    | 1                   |
		And Click cate_selection_register Wait[None]
		And Accept Alert Wait[Loaded]
		And  Click back Wait[Loaded]
		And Click cate_edit3 Wait[Loaded]
	Then Assert List cateselectionlist
		| cate_parent_selection | cate_id_selection | cate_name_selection | cate_order_selection | cate_disp_selection |
		|                       | 1                 | クレンジング・洗顔           | 1                    | 1                   |
		|                       | 2                 | 化粧水                 | 2                    | 1                   |

Scenario: Edit Category 4
	# This scenario is for editing category 4
	Given Insert Template 7.2/cate4_t Into cate4_t
	When Open /admin Wait[Loaded]
		And Enter
	 	| user_login_id | passwd  |
		| ivpers       | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/item/asp/cate_regist.asp Wait[Loaded]
		And Click cate_edit4 Wait[Loaded]
	When Enter List cateselectionlist
		| cate_delete_selection | cate_parent_selection | cate_id_selection | cate_name_selection | cate_order_selection | cate_disp_selection |
		| 1                     |                       | 1                 | 美白                  | 1                    | 1                   |
		|                       |                       | 2                 | エイジング               | 2                    | 1                   |
		|                       |                       | 3                 | トラブル肌               | 3                    | 1                   |
		And Click cate_selection_register Wait[None]
		And Accept Alert Wait[Loaded]
		And  Click back Wait[Loaded]
		And Click cate_edit4 Wait[Loaded]
	Then Assert List cateselectionlist
		| cate_parent_selection | cate_id_selection | cate_name_selection | cate_order_selection | cate_disp_selection |
		|                       | 2                 | エイジング               | 2                    | 1                   |
		|                       | 3                 | トラブル肌               | 3                    | 1                   |

Scenario: Edit Category 5
	# This scenario is for editing category 5
	Given Insert Template 7.2/cate5_t Into cate5_t
	When Open /admin Wait[Loaded]
		And Enter
	 	| user_login_id | passwd  |
		| ivpers       | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/item/asp/cate_regist.asp Wait[Loaded]
		And Click cate_edit5 Wait[Loaded]
	When Enter List cateselectionlist
		| cate_delete_selection | cate_id_selection | cate_name_selection | cate_order_selection | cate_disp_selection |
		|                       | 1                 | admin category      | 0                    | 1                   |
		And Click cate_selection_register Wait[None]
		And Accept Alert Wait[Loaded]
		And  Click back Wait[Loaded]
		And Click cate_edit5 Wait[Loaded]
	Then Assert List cateselectionlist
		| cate_parent_selection | cate_id_selection | cate_name_selection | cate_order_selection | cate_disp_selection |
		|                       | 1                 | admin category      | 0                    | 1                   |


		# When opening /admin/top/item/asp/cate_tree_view.asp?s_site_id=1
		# error message found :  Could not found target destination
Scenario: Display Relation Tree Of All Categories 
	# This scenario is for testing the Display Relation Tree Of All Categories button 
	When Open /admin Wait[Loaded]
		And Enter
	 	| user_login_id | passwd  |
		| ivpers       | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/item/asp/cate_regist.asp Wait[Loaded]
		And Click cate_tree Wait[Loaded]
		And Switch[Popup] /admin/top/item/asp/cate_tree_view.asp Wait[Loaded]
		And Close[Popup] Wait[Loaded]