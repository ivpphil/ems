﻿@V7.2Admin

Feature: PaymentMethod
	It contains to test case in modifying the tax consumptions.

#----------------------------------------------------------------------------------------------
Scenario: Payment Method Modification
#----------------------------------------------------------------------------------------------
#This test opens the Payment Method as an Admin
#It modifies the information by checking(1) and unchecking(0) the checkbox of the payment method
#It clicks the reg button to save the changes
#It proceed to completion page then go back to the page
#It checks if the changes are made.
#----------------------------------------------------------------------------------------------

	Given Insert Template 7.2/pay_t Into pay_t
		And Insert Template 7.2/card_t Into card_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/store/asp/store_payment.asp Wait[Loaded]
		And Enter
		| Field        | Value |
		| site_id@name | 5     |
		And Enter
		| Field                     | Value |
		| pay_CREDIT_CARD@id        | 1     |
		| pay_BANK@id               | 1     |
		| pay_CASH_ON_DELIVERY@id   | 1     |
		| pay_CONVENIENCE_STORE@id  | 1     |
		| pay_PAYPAL@id             | 1     |
		| pay_NON_NEEDED_PAYMENT@id | 1     |		
		And Enter List card_list
		| Checkbox1@id |
		| 0            |
		| 0            |
		| 0            |
		| 0            |
		| 0            |
		And Click reg_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click btn01@class Wait[Loaded]
		And Enter
		| Field        | Value |
		| site_id@name | 5     |
	Then Assert Elements
		| Field                     | Value |
		| pay_CREDIT_CARD@id        | 1     |
		| pay_BANK@id               | 1     |
		| pay_CASH_ON_DELIVERY@id   | 1     |
		| pay_CONVENIENCE_STORE@id  | 1     |
		| pay_PAYPAL@id             | 1     |
		| pay_NON_NEEDED_PAYMENT@id | 1     |
	And Assert List card_list
		| Checkbox1@id |
		| 0           |
		| 0            |
		| 0            |
		| 0            |
		| 0            |