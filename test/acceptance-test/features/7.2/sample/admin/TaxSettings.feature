﻿@V7.2Admin

Feature: TaxSettings
	It contains to test case in modifying the tax consumptions.

#----------------------------------------------------------------------------------------------
Scenario: Tax Consumption Modification
#----------------------------------------------------------------------------------------------
#This test opens the Tax Settings as an Admin
#It modifies the information regarding the tax
#It clicks the reg button to save the changes
#It proceed to completion page then go back to the page
#It checks the information in the list if it is the same/correct.
#----------------------------------------------------------------------------------------------

	Given Insert Template 7.2/pay_t Into pay_t
		And Insert Template 7.2/setup_t Into setup_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/store/asp/store_tax.asp Wait[Loaded]
		And Enter
		| Field                            | Value |
		| tax@name                         | 10    |
		| enable_carriage_tax@name         | 1     |
		| pay_record_key_0_enable_tax@name | 1     |
		| pay_record_key_1_enable_tax@name | 1     |
		| pay_record_key_2_enable_tax@name | 1     |
		| pay_record_key_3_enable_tax@name | 1     |
		| pay_record_key_4_enable_tax@name | 1     |
		And Click reg_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click btn01@class Wait[Loaded]
	Then Assert Elements
		| Field                            | Value |
		| tax@name                         | 10    |
		| enable_carriage_tax@name         | 1     |
		| pay_record_key_0_enable_tax@name | 1     |
		| pay_record_key_1_enable_tax@name | 1     |
		| pay_record_key_2_enable_tax@name | 1     |
		| pay_record_key_3_enable_tax@name | 1     |
		| pay_record_key_4_enable_tax@name | 1     |

