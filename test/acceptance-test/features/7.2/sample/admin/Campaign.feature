﻿@ers7.2Admin
Feature: Campaign
	This feature contains test cases for campaign page


Scenario: Search Campaign
	# This scenario is for searching of campaign.
	# Login then open landing_list.asp
	# Enter values on the following fields
	# Click search
	# Then assert result.
	Given Insert Template 7.2/lp_page_manage_t Into lp_page_manage_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/lp/asp/landing_list.asp Wait[Loaded]
			And Enter
			 | Field             | Value |
			 | s_freeword  | camp1 |
			 | s_group_name |       |
			 | s_site_id    |       |
			 And Click search@name Wait[Loaded]
		Then Assert List campaignlist
			 | status | gname | pname     | code               | date                   |
			 | 掲載中    | camp1 | campaign1 | lp.asp?ccode=camp1 | 2016/03/01 ～2017/05/02 |

Scenario: Register Campaign
	# This scenario is for registering of campaign.
	# Login then open landing_list.asp
	# Enter values on the following fields
	# Click register then assert
	# Go to details and register landing page
	# Then assert landing pages
	Given Delete From lp_page_manage_t
		| page_name     |
		| Fish Campaign |
		And Insert Template 7.2/lp_page_manage_t Into lp_page_manage_t
		And Insert Template 7.2/lp_page_t Into lp_page_t
		And Insert Template 7.2/lp_page_type_t Into lp_page_type_t
		And Insert Template 7.2/lp_questionnaire_setup_t Into lp_questionnaire_setup_t
		And Insert Template 7.2/lp_questionnaire_t Into lp_questionnaire_t
		And Insert Template 7.2/lp_template_t Into lp_template_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/lp/asp/landing_list.asp Wait[Loaded]
			And Click register Wait[Loaded]
			And Enter
			 | Field                   | Value            |
			 | lp_group_name      | Fish Campaign |
			 | lp_group_name_list |                  |
			 | ccode              | fcamp            |
			 | site_id            | {1,5}            |
			 | page_name          | Fish Campaign |
			 | page_title         | Fish Campaign |
			 And Attach File
			 | Field      | Value                                      |
			 | logo_image | 7.2\sample\admin\Campaign\campaign.jpg |
			 And Enter
			 | Field          | Value                 |
			 | public_st_date | 4/1/2016 12:00:00 AM  |
			 | public_ed_date | 4/30/2016 11:59:59 PM |
			 And Click lpregist_base Wait[Loaded]
			And Enter
			| Field            | Value |
			| basic_stgy_kbn   | 2     |
			| upsell_stgy_kbn | 2     |
			| buy_limit_kbn    | 2     |
			And Click addButton Wait[Loaded]
			And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Loaded]
			And Enter
			| Field   | Value |
			| s_gcode |       |
			| s_scode | CH100 |
			| s_sname |       |
			And Click g_search_mode Wait[Element] btnSetting
			And Click campsearchlist[0].btnSetting Wait[None]
			And Close[Popup] Wait[Loaded]
			And Enter
			| Field                | Value |
			| sell_order_type      | 1     |
			| sell_price           | 1000  |
			| sell_discount_flg    | 0     |
			| sell_max_amount      | 2     |
			| sell_max_stock       | 100   |
			And Click addButton2 Wait[Loaded]
			And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Loaded]
			And Enter
			| Field   | Value |
			| s_gcode |       |
			| s_scode | CH101 |
			| s_sname |       |
			And Click g_search_mode Wait[Element] btnSetting
			And Click campsearchlist[0].btnSetting Wait[None]
			And Close[Popup] Wait[Loaded]
			And Enter
			| Field                  | Value |
			| upsell_order_type@name | 1     |
			| upsell_price           | 1500  |
			| upsell_discount_flg    | 0     |
			| upsell_max_amount      | 3     |
			| upsell_max_stock       | 100   |
			| coupon_flg             | 0     |
			| carriage_free_flg      | 1     |
			| carriage_free_price    | 3000  | 
			And Click lpregist_detail Wait[Loaded]
			And Enter
			| Field                  | Value    |
			| questlist[13].use      | Use      |
			| uquestlist[14].use     | Use      |
			| questlist[6].required  |          |
			| questlist[10].required |          |
			| questlist[11].required |          |
			| questlist[13].required | Required |
			| questlist[14].required | Required |
			And Enter List questlist 
			| item_note |
			|           |
			|           |
			|           |
			|           |
			|           |
			|           |
			|           |
			|           |
			|           |
			|           |
			|           |
			|           |
			|           |
			| Required  |
			| Required  |  
			And Enter
			| Field             | Value |
			| personal_info_kbn | 1     |
			And Click lpregist_questionnaire Wait[Loaded]
		Then Assert Elements
			| Field               | Value                                  |
			| lp_group_name       | Fish Campaign                          |
			| ccode               | fcamp                                  |
			| site_id             | ERS, ERS site2                         |
			| page_name           | Fish Campaign                          |
			| page_title          | Fish Campaign                          |
			#| logo_image          | <%=setup.pc_sec_url%>up/cms/news/qLEs74gAaH1ewLCyvTOohHlWrmeBnq46jIkPuKXGkVTMJLr2DWaV7OAWhWWixeE2.jpg | #cannot assert image
			| active              | 有効                                     |
			| basic_stgy_kbn      | アップセル                                  |
			| upsell_stgy_kbn     | 販売商品をキャンセルする （アップセル販売商品のみ）             |
			| buy_limit_kbn       | 数回申込み可能                                |
			| sell_sname          | 商品名:Cow Head Fresh Milk                    |
			| sell_scode          | 商品番号:CH100                                  |
			| sell_order_type     | 通常                                     |
			| sell_price          | 1000円                                  |
			| sell_discount_flg   | 使用しない                                  |
			| sell_max_amount     | 2                                      |
			| sell_max_stock    | 100                                    |
			| upsell_sname        | 商品名:Cow Head Strawberry                    |
			| upsell_scode        | 商品番号:CH101                                  |
			| upsell_order_type   | 通常                                     |
			| upsell_price        | 1500円                                  |
			| upsell_discount_flg | 使用しない                                  |
			| upsell_max_amount   | 3                                      |
			| upsell_max_stock    | 100                                    |
			| coupon_flg          | 使用しない                                  |
			| carriage_free_flg   | 使用する                                   |
			| carriage_free_price | 3000円以上の購入時                            |
			And Assert List questlist
			| use | required | item_note |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 非必須      |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 非必須      |           |
			| 使用  | 非必須      |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       | Required  |
			| 使用  | 必須       | Required  |
			And Assert Elements
			| Field             | Value |
			| personal_info_kbn | 登録済みの個人情報を指定する     |
		When Click lpregist_confirm Wait[Loaded]
			And Click back Wait[Loaded]
			And Click campaignlist[0].details Wait[Loaded]
			And Click lp_list[0].lp_register Wait[Loaded]
				And Click select Wait[Loaded]
				And Enter List lplist
				| body_use_flg             |
				| this is the landing page |
				| this is the landing page |
				| this is the landing page |
				And Attach File
				 | Field                | Value                                |
				 | upsell_button_1_file | 7.2\sample\admin\Campaign\button.jpg |
				And Enter
				 | Field                | Value |
				 | upsell_button_2_chk  | 1     |
				 | upsell_button_2_file |       |
				 | upsell_button_3_chk  | 1     |
				 | upsell_button_3_file |       |
				And Click register Wait[Loaded]
				And Click back Wait[Loaded]
			And Click lp_list[1].lp_register Wait[Loaded]
				And Click select Wait[Loaded]
				And Enter List lplist
				| body_use_flg             |
				| this is the upsell page |
				| this is the upsell page |
				And Click register Wait[Loaded]
				And Click back Wait[Loaded]
			And Click lp_list[2].lp_register Wait[Loaded]
				And Click select Wait[Loaded]
				And Enter List lplist
				| body_use_flg             |
				| this is the confirmation page |
				| this is the confirmation page |
				And Click register Wait[Loaded]
				And Click back Wait[Loaded]
			And Click lp_list[3].lp_register Wait[Loaded]
				And Click select Wait[Loaded]
				And Enter List lplist
				| body_use_flg             |
				| this is the completion page |
				| this is the completion page |
				And Click register Wait[Loaded]
				And Click back Wait[Loaded]
			And Click lp_register_form Wait[Loaded]
			And Click back Wait[Loaded]
		When Click lp_list[0].lp_edit Wait[Loaded]
			Then Assert List lplist
			| body_use_flg             |
			| <p>this is the landing page</p> |
			| <p>this is the landing page</p> |
			| <p>this is the landing page</p> |
		When Click back Wait[Loaded]
			And Click lp_list[1].lp_edit Wait[Loaded]
			Then Assert List lplist
			| body_use_flg             |
			| <p>this is the upsell page</p> |
			| <p>this is the upsell page</p> |
		When Click back Wait[Loaded]
			And Click lp_list[2].lp_edit Wait[Loaded]
			Then Assert List lplist
			| body_use_flg             |
			| <p>this is the confirmation page</p> |
			| <p>this is the confirmation page</p> |
		When Click back Wait[Loaded]
			And Click lp_list[3].lp_edit Wait[Loaded]
			Then Assert List lplist
			| body_use_flg             |
			| <p>this is the completion page</p> |
			| <p>this is the completion page</p> |


Scenario: Delete Campaign
	# This scenario is for searching of campaign.
	# Login then open landing_list.asp
	# Click detail button the click delete button
	# click dialog ok button then assert
	Given Insert Template 7.2/lp_page_manage_t Into lp_page_manage_t
		And Insert Template 7.2/lp_page_t Into lp_page_t
		And Insert Template 7.2/lp_page_type_t Into lp_page_type_t
		And Insert Template 7.2/lp_questionnaire_setup_t Into lp_questionnaire_setup_t
		And Insert Template 7.2/lp_questionnaire_t Into lp_questionnaire_t
		And Insert Template 7.2/lp_template_t Into lp_template_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/lp/asp/landing_list.asp Wait[Loaded]
			And Enter
			 | Field             | Value |
			 | s_freeword  | camp1 |
			 | s_group_name |       |
			 | s_site_id    |       |
			 And Click search@name Wait[Loaded]
		Then Assert List campaignlist
			 | status | gname | pname     | code               | date                   |
			 | 掲載中    | camp1 | campaign1 | lp.asp?ccode=camp1 | 2016/03/01 ～2017/05/02 |
		When Click campaignlist[0].details Wait[Loaded]
			 And Click lp_delete_form Wait[Loaded]
			 And Click dialog_ok_button Wait[Loaded]
			 And Click back Wait[Loaded]
		Then Assert Deleted List campaignlist
			| gname |
			| camp1 |
		#Cannot assert deleted list if there is no available data left.

Scenario: Modify Campaign
	# This scenario is for modifying of campaign.
	# Login then open landing_list.asp
	# Search for a campaign to be edited
	# Change values on the following fields
	# Click register
	# Then assert result.
	Given Delete From lp_page_manage_t
		| page_name      |
		| New Campaign   |
		| （コピー）campaign1 |
		And Insert Template 7.2/lp_page_manage_t Into lp_page_manage_t
		And Insert Template 7.2/lp_page_t Into lp_page_t
		And Insert Template 7.2/lp_page_type_t Into lp_page_type_t
		And Insert Template 7.2/lp_questionnaire_setup_t Into lp_questionnaire_setup_t
		And Insert Template 7.2/lp_questionnaire_t Into lp_questionnaire_t
		And Insert Template 7.2/lp_template_t Into lp_template_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/lp/asp/landing_list.asp Wait[Loaded]
			And Enter
			 | Field        | Value |
			 | s_freeword   | camp1 |
			 | s_group_name |       |
			 | s_site_id    |       |
			 And Click search@name Wait[Loaded]
		Then Assert List campaignlist
			 | status | gname | pname     | code               | date                   |
			 | 掲載中    | camp1 | campaign1 | lp.asp?ccode=camp1 | 2016/03/01 ～2017/05/02 |
		When Click campaignlist[0].details Wait[Loaded]
			 And Click base_edit1 Wait[Loaded]
			 And Enter
			 | Field                   | Value            |
			 | site_id            | {1,5}            |
			 | page_name          | New Campaign |
			 | page_title         | New Campaign |
			 And Attach File
			 | Field      | Value                                      |
			 | logo_image | 7.2\sample\admin\Campaign\campaign.jpg |
			 And Enter
			 | Field          | Value                 |
			 | public_st_date | 5/1/2016 12:00:00 AM  |
			 | public_ed_date | 5/30/2016 11:59:59 PM |
			 | active         | 1                     |
			 And Click lpregist_base Wait[Loaded]
			 And Click lp_list[0].lp_edit Wait[Loaded]
				And Enter List lplist
				| body_use_flg             |
				| this is the landing page |
				| this is the landing page |
				| this is the landing page |
				And Attach File
				 | Field                | Value                                |
				 | upsell_button_1_file | 7.2\sample\admin\Campaign\button.jpg |
				And Enter
				 | Field                | Value |
				 | upsell_button_2_chk  | 1     |
				 | upsell_button_2_file |       |
				 | upsell_button_3_chk  | 1     |
				 | upsell_button_3_file |       |
				And Click register Wait[Loaded]
				And Click back Wait[Loaded]
			And Click lp_list[1].lp_edit Wait[Loaded]
				And Enter List lplist
				| body_use_flg             |
				| this is the confirmation page |
				| this is the confirmation page |
				And Click register Wait[Loaded]
				And Click back Wait[Loaded]
			And Click lp_list[2].lp_edit Wait[Loaded]
				And Enter List lplist
				| body_use_flg             |
				| this is the completion page |
				| this is the completion page |
				And Click register Wait[Loaded]
				And Click back Wait[Loaded]
			And Click detail_edit1 Wait[Loaded]
				And Enter
				| Field           | Value |
				| basic_stgy_kbn  | 1     |
				| upsell_stgy_kbn |       |
				| buy_limit_kbn   | 2     |
				And Click addButton Wait[Loaded]
				And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Loaded]
				And Enter
				| Field   | Value |
				| s_gcode |       |
				| s_scode | CH100 |
				| s_sname |       |
				And Click g_search_mode Wait[Element] btnSetting
				And Click campsearchlist[0].btnSetting Wait[None]
				And Close[Popup] Wait[Loaded]
				And Enter
				| Field                | Value |
				| sell_order_type      | 1     |
				| sell_price           | 2000  |
				| sell_discount_flg    | 0     |
				| sell_max_amount      | 5     |
				| sell_max_stock       | 100   |
				| carriage_free_flg    | 1     |
				| carriage_free_price  | 3000  |
				And Click lpregist_detail Wait[Loaded]
			And Click quest_edit Wait[Loaded]
				And Enter
				| Field                  | Value |
				| questlist[13].use      |       |
				| questlist[14].use     |       |
				| questlist[6].required  |       |
				| questlist[10].required |       |
				| questlist[11].required |       |
				| questlist[13].required |       |
				| questlist[14].required |       |
				And Enter List questlist 
				| item_note |
				|           |
				|           |
				|           |
				|           |
				|           |
				|           |
				|           |
				|           |
				|           |
				|           |
				|           |
				|           |
				|           |
				|           |
				|           |     
				And Enter
				| Field             | Value |
				| personal_info_kbn | 1     |
				And Click lpregist_questionnaire Wait[Loaded] 
			And Click lp_register_form Wait[Loaded]
			And Click back Wait[Loaded]
		Then Assert Elements
			| Field         | Value                                                            |
			| lp_group_name | camp1                                                            |
			| ccode         | camp1                                                            |
			| site_id       | ERS, ERS site2                                                   |
			| page_name     | New Campaign                                                     |
			| page_title    | New Campaign                                                     |
			#| logo_image    | <%=setup.pc_sec_url%>images/cms/news/lp_camp1_20160421095232.jpg |
			| active        | 有効                                                               |              
		When Click lp_list[0].lp_edit Wait[Loaded]
			Then Assert List lplist
			| body_use_flg             |
			| <p>this is the landing page</p> |
			| <p>this is the landing page</p> |
			| <p>this is the landing page</p> |
		When Click back Wait[Loaded]
			And Click lp_list[1].lp_edit Wait[Loaded]
			Then Assert List lplist
			| body_use_flg             |
			| <p>this is the confirmation page</p> |
			| <p>this is the confirmation page</p> |
		When Click back Wait[Loaded]
			And Click lp_list[2].lp_edit Wait[Loaded]
			Then Assert List lplist
			| body_use_flg                |
			| <p>this is the completion page</p> |
			| <p>this is the completion page</p> |
		When Click back Wait[Loaded]
			Then Assert Elements
			| Field               | Value                   |
			| basic_stgy_kbn      | 本商品単体                   |
			| buy_limit_kbn       | 数回申込み可能                 |
			| sell_sname          | 商品名:Cow Head Fresh Milk |
			| sell_scode          | 商品番号:CH100               |
			| sell_order_type     | 通常                      |
			| sell_price          | 2000円                   |
			| sell_discount_flg   | 使用しない                   |
			| sell_max_amount     | 5                       |
			| sell_max_stock    | 100                     |
			| coupon_flg          | 使用しない                   |
			| carriage_free_flg   | 使用する                    |
			| carriage_free_price | 3000円以上の購入時             |
			And Assert List questlist
			| use | required | item_note |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 非必須      |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 必須       |           |
			| 使用  | 非必須      |           |
			| 使用  | 非必須      |           |
			| 使用  | 必須       |           |
			| 非使用 | 非必須      |           |
			| 非使用 | 非必須      |           |
			And Assert Elements
			| Field             | Value |
			| personal_info_kbn | 登録済みの個人情報を指定する     |

Scenario: Copy Campaign
	# This scenario is for copying of campaign.
	# Login then open landing_list.asp
	# Enter search criteria
	# Click search
	# Then assert result then click copy button
	# Click dialog ok button then click back
	# Assert the result
	Given Delete From lp_page_manage_t
		| page_name |
		|（コピー）campaign1        |
		And Insert Template 7.2/lp_page_manage_t Into lp_page_manage_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/lp/asp/landing_list.asp Wait[Loaded]
			And Enter
			 | Field             | Value |
			 | s_freeword  | camp1 |
			 | s_group_name |       |
			 | s_site_id    |       |
			 And Click search@name Wait[Loaded]
		Then Assert List campaignlist
			 | status | gname | pname     | code               | date                   |
			 | 掲載中    | camp1 | campaign1 | lp.asp?ccode=camp1 | 2016/03/01 ～2017/05/02 |
		When Click campaignlist[0].copy Wait[Loaded]
			 And Click dialog_ok_button Wait[Loaded]
			 And Click back Wait[Loaded]
		Then Assert Elements
			| Field | Value           |
			| page_name |（コピー）campaign1 |