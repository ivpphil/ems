﻿@V7.2Admin

Feature: BankTransfer
	It contains bank transfer payment registration


Scenario: Bank Transfer Registration
#This test opens the Bank Transfer Payment Registration as an Admin
#It inserts the templates in the database
#It inserts the amount and date of payment
#It checks the checkbox to save the payment 
#Registers the payment by clicking the register button

	Given Delete From old_d_master_t
		| d_no        |
		| 10000023-01 |
		And Insert Template 7.2/old_d_master_t Into old_d_master_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/regular/asp/regist_income.asp Wait[Loaded]
		And Enter List payment_list
		#NOTE: it needs the same amount of payment as the total amount
			| paid_check | paid_price | paid_date  |
			| true       | 2028       | 2016/04/13 |
			| false      | 2100       | 2016/04/13 |
		And Click reg_btn@name Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Open /admin/top/regular/asp/regist_income.asp Wait[Loaded]
	Then Assert Deleted List payment_list
		| d_no        |
		| 10000023-01 |
