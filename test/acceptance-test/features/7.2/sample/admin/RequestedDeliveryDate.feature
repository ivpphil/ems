﻿@V7.2Admin
Feature: RequestedDeliveryDate
	Contains test case that adds, modifies and delete of delivery time settings modifies the requested delivery date; (ERS v7.2) 



#-------------------------------------------------------------------------------------------------------
Scenario: Add New Delivery Time
#-------------------------------------------------------------------------------------------------------
#This test case deletes the data in the db table name that has the same sendtime as stated below
#It inserts the templates in the database
#It deletes the sample sendtime that we will add(In this case, the sendtime would be 'T E S T')
#It opens the Requested Delivery Time and Date under Store setting as an Admin
#Adds a new shipping time that the customer can select on the front side
#Checks if the new sendtime is added

	Given Delete From sendtime_t
			| sendtime  |
			| ＴＥＳＴ   |
			| 指定しない |
			| 12～15時   |
			| 14～17時   |
			| 18～21時   |
			| 16～19時   |
			| 20～22時   |
		And Insert Template 7.2/sendtime_t Into sendtime_t
		And Delete From sendtime_t
			| sendtime |
			| ＴＥＳＴ  |
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/store/asp/store_delivery_day.asp Wait[Loaded] 
		And Enter
			| add_sendtime@name |
			| ＴＥＳＴ          |
		And Click reg_btn Wait[Loaded] 
		And Click dialog_ok_button Wait[Loaded] 
		And Click back_btn Wait[Loaded] 

	Then Assert List sendtime_list
		| sendtime   |
		| 指定しない  |
		| 12～14時   |
		| 14～16時   |
		| 18～20時   |
		| 20～21時   |

#-------------------------------------------------------------------------------------------------------
Scenario: Modify Requested Delivery Time
#-------------------------------------------------------------------------------------------------------
#This test case deletes the data in the db table name that has the same sendtime as stated below
#It inserts the templates in the database
#This test opens the Requested Delivery Time and Date under Store setting as an Admin
#Modifies the requested delivery time 
#Checks if the changes made is reflected in the system

	Given Delete From sendtime_t
			| sendtime  |
			| ＴＥＳＴ   |
			| 指定しない |
			| 12～15時   |
			| 14～17時   |
			| 18～21時   |
			| 16～19時   |
			| 20～22時   |
		And Insert Template 7.2/sendtime_t Into sendtime_t
		And Insert Template 7.2/setup_t Into setup_t
	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/store/asp/store_delivery_day.asp Wait[Loaded] 
		And Enter List sendtime_list
		| sendtime   |
		| 指定しない |
		| 12～15時   |
		| 14～17時   |
		| 18～21時   |
		| 16～19時   |
		| 20～22時   |

		And Click reg_btn Wait[Loaded] 
		And Click dialog_ok_button Wait[Loaded] 
		And Click back_btn Wait[Loaded] 

	Then Assert List sendtime_list
		| sendtime   |
		| 指定しない |
		| 12～15時   |
		| 14～17時   |
		| 18～21時   |
		| 16～19時   |
		| 20～22時   |

#-------------------------------------------------------------------------------------------------------
Scenario: Delete Requested Delivery Time
#-------------------------------------------------------------------------------------------------------
#This test case deletes the data in the db table name that has the same sendtime as stated below
#It inserts the templates in the database
#This test opens the Requested Delivery Time and Date under Store setting as an Admin
#Deletes the 5th Requested Delivery Time
#Checks if the deleted time is no longer in the system

	Given Delete From sendtime_t
			| sendtime  |
			| ＴＥＳＴ   |
			| 指定しない |
			| 12～15時   |
			| 14～17時   |
			| 18～21時   |
			| 16～19時   |
			| 20～22時   |
		And Insert Template 7.2/sendtime_t Into sendtime_t
		And Insert Template 7.2/setup_t Into setup_t
	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/store/asp/store_delivery_day.asp Wait[Loaded] 
		And Click sendtime_list[4].del_btn Wait[Loaded] 
	Then Assert Deleted List sendtime_list
		| sendtime |
		| 20～21時 |


#-------------------------------------------------------------------------------------------------------
Scenario: Modify the Delivery Date Setting 
#-------------------------------------------------------------------------------------------------------
#This test case deletes the data in the db table name that has the same sendtime as stated below
#It inserts the templates in the database
#It modifies the Minimum Days before shipping the product(FIELD- shipping_days)
#It modifies the No. Days before shipping the product in Subscription basis(FIELD- regular_order_days)
#It modifies the limit of the soonest delivery date after order completion (FIELD- day_after and day_until)
#Checks if the changes made is reflected in the system

	Given Delete From sendtime_t
			| sendtime  |
			| ＴＥＳＴ   |
			| 指定しない |
			| 12～15時   |
			| 14～17時   |
			| 18～21時   |
			| 16～19時   |
			| 20～22時   |
		And Insert Template 7.2/sendtime_t Into sendtime_t
		And Insert Template 7.2/setup_t Into setup_t
	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/store/asp/store_delivery_day.asp Wait[Loaded] 
		And Enter
			| Field                             | Value |
			| shipping_csv_output_min_days@name | 4     |
			| create_regular_order_days@name    | 3     |
			| sendday@name                      | 10    |
			| sendday_count@name                | 50    |

		And Click reg_btn Wait[Loaded] 
		And Click dialog_ok_button Wait[Loaded] 
		And Click back_btn Wait[Loaded] 

		Then Assert Elements
			| Field                             | Value |
			| shipping_csv_output_min_days@name | 4     |
			| create_regular_order_days@name    | 3     |
			| sendday@name                      | 10    |
			| sendday_count@name                | 50    |

