﻿@ers7.2Admin
Feature: CreditCardErrorSearch
	This feature contains test cases for searching Credit Card Error


Scenario: Credit Card Error Search
	# This scenario is for searching of credit card error.
	# Login then open cus_crderr_search.asp
	# Enter values on the following fields
	# Click search
	# Then assert result.
	Given Insert Template 7.2/regular_error_t Into regular_error_t
		And Insert Template 7.2/member_t Into member_t
		And Insert Template 7.2/member_card_t Into member_card_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/customer/asp/cus_crderr_search.asp Wait[Loaded]
			And Enter
			 | Field                      | Value               |
			 | src_occured_date_from@name | 2016/04/14 00:00:00 |
			 | src_occured_date_to@name   |                     |
			 | src_tel@name               |                     |
			 | src_lnamek@name            |                     |
			 | src_fnamek@name            |                     |
			 | src_email@name             |                     |
			 | src_regular_num@name       |                     |
			 And Click search@name Wait[Loaded]
		Then Assert List membertabledata
			 | name      | occured_date         | id_list | tel         | email               |
			 | 試験山田 太郎次郎 | 4/14/2016 8:39:25 PM | 4       | 09012345687 | shibutani@ivp.co.jp |

