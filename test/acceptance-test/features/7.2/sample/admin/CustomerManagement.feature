﻿@V7.2Admin
Feature: CustomerManagement
			Customer Search
#-------------------------------------------------------
Scenario: Customer Search
#-------------------------------------------------------

	#This scenario is used to search the customer and modify the information.

	Given Delete From member_t
		| email                       |
		| arieltest123@mailinator.com |
	And Insert Template member2_t Into member_t

	When Open /admin/top/login/asp/login.asp Wait[Loaded]
		And Enter
			| user_login_id@name | passwd@name |
			| ivpers             | ivpers      |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/customer/asp/cus_search.asp Wait[Loaded]
		And Enter
			| Field                | Value                       |
			| s_site_id@id         | {1}                         |
			| src_email@id         | arieltest123@mailinator.com |
			| src_tel@id           | 123456789                   |
			| src_lnamek@id        | ロペズ                       |
			| src_fnamek@id        | アリエル                     |
			| src_compname@id      | IVP                         |
			| src_mailtype@id      | 0                           |
			| src_regdate_f@id     | 2016/03/08                  |
			| src_regdate_t@id     | 2016/03/31                  |
			| src_age_f@id         | 16                          |
			| src_age_t@id         | 100                         |
			| src_dm_flg@id        | 0                           |
			| src_out_bound_flg@id | 0                           |
			| src_sex@id           | 1                           |
			| src_point_f@id       |                             |
			| src_point_t@id       |                             |
			| src_pref@id          | 28                          |
			| src_deleted@id       | 0                           |
			| src_member_rank@id   |                             |
		And Click search_btn Wait[Loaded] 
		Then Assert List customerList
			| full_name   | age | email                       | w_sex | address | taddress |
			| Lopez Ariel | 26  | arieltest123@mailinator.com | 男性    | 神戸市中央区  | 北長狭通     |
		When Click customerList[0].detail_btn Wait[Loaded]  
		Then Assert Elements
			| Field               | Value                       |
			| lname@id            | Lopez                       |
			| fname@id            | Ariel                       |
			| lnamek@id           | ロペズ                       |
			| fnamek@id           | アリエル                     |
			| compname@id         | IVP                         |
			| compnamek@id        | アイヴィピ                   |
			| division@id         |                             |
			| divisionk@id        |                             |
			| email@id            | arieltest123@mailinator.com |
			| mformat@id          | 1                           |
			| email_confirm@id    | arieltest123@mailinator.com |
			| tel@id              | 123456789                   |
			| fax@id              | 111111111                   |
			| zip@id              | 650-0012                    |
			| pref@id             | 28                          |
			| address@id          | 神戸市中央区                 |
			| taddress@id         | 北長狭通                     |
			| maddress@id         |                             |
			| job@id              | 1                           |
			| sex@id              | 1                           |
			| birthday_y@id       | 1990                        |
			| birthday_m@id       | 3                           |
			| birthday_d@id       | 18                          |
			| passwd1@id          |                             |
			| password2@id        |                             |
			| ques@id             | 1                           |
			| ans@id              | ママ                        |
			| account_status@name | 0                           |
			| login_try_count@id  | 0                           |
			| delivery_method     | 0                           |
			| memo@id             |                             |
		When Enter
			| Field               | Value                       |
			| lname@id            | Lopez                       |
			| fname@id            | Ariel                       |
			| lnamek@id           | ロペズ                       |
			| fnamek@id           | アリエル                     |
			| compname@id         | ABC                         |
			| compnamek@id        | エイビシ                     |
			| division@id         |                             |
			| divisionk@id        |                             |
			| email@id            | arieltest123@mailinator.com |
			| mformat@id          | 1                           |
			| email_confirm@id    | arieltest123@mailinator.com |
			| tel@id              | 987654321                   |
			| fax@id              | 222222222                   |
			| zip@id              | 650-0012                    |
			| pref@id             | 28                          |
			| address@id          | 神戸市中央区                 |
			| taddress@id         | 北長狭通                     |
			| maddress@id         |                             |
			| job@id              | 8                           |
			| sex@id              | 1                           |
			| birthday_y@id       | 1990                        |
			| birthday_m@id       | 3                           |
			| birthday_d@id       | 18                          |
			| passwd1@id          | 12345678                    |
			| password2@id        | 12345678                    |
			| ques@id             | 1                           |
			| ans@id              | ママ                        |
			| account_status@name | 0                           |
			| login_try_count@id  | 0                           |
			| delivery_method     | 1                           |
			| memo@id             | Implementation updated      |
		And Click change_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded] 
		And Click back_btn Wait[Loaded] 
		Then Assert Elements
			| Field               | Value                       |
			| lname@id            | Lopez                       |
			| fname@id            | Ariel                       |
			| lnamek@id           | ロペズ                       |
			| fnamek@id           | アリエル                     |
			| compname@id         | ABC                         |
			| compnamek@id        | エイビシ                     |
			| division@id         |                             |
			| divisionk@id        |                             |
			| email@id            | arieltest123@mailinator.com |
			| mformat@id          | 1                           |
			| email_confirm@id    | arieltest123@mailinator.com |
			| tel@id              | 987654321                   |
			| fax@id              | 222222222                   |
			| zip@id              | 650-0012                    |
			| pref@id             | 28                          |
			| address@id          | 神戸市中央区                 |
			| taddress@id         | 北長狭通                     |
			| maddress@id         |                             |
			| job@id              | 8                           |
			| sex@id              | 1                           |
			| birthday_y@id       | 1990                        |
			| birthday_m@id       | 3                           |
			| birthday_d@id       | 18                          |
			| passwd1@id          |                             |
			| password2@id        |                             |
			| ques@id             | 1                           |
			| ans@id              | ママ                        |
			| account_status@name | 0                           |
			| login_try_count@id  | 0                           |
			| delivery_method     | 1                           |
			| memo@id             | Implementation updated      |
		When Click common_logout Wait[Loaded] 

