﻿@V7.2Admin

Feature: PostageSettings
		It contains to test case in modifying the tax consumptions.


#----------------------------------------------------------------------------------------------
Scenario: Postage Charge Modification
#----------------------------------------------------------------------------------------------
#This test opens the Postage Settings as an Admin
#It modifies the charge depending on the prefecture
#It clicks the reg button to save the changes
#It proceed to completion page then go back to the page
#It checks if the charges are the same/correct.
#----------------------------------------------------------------------------------------------

	Given Insert Template 7.2/pref_t Into pref_t
		And Insert Template 7.2/setup_t Into setup_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/store/asp/store_carriage.asp Wait[Loaded]
		And Enter
		| Field        | Value |
		| site_id@name | 5     |
		And Enter List postage_list
		| postage_charge |
		| 101            |
		| 102            |
		| 103            |
		| 104            |
		| 105            |
		| 106            |
		| 107            |
		| 108            |
		| 109            |
		| 110            |
		| 111            |
		| 112            |
		| 113            |
		| 114            |
		| 115            |
		| 116            |
		| 117            |
		| 118            |
		| 119            |
		| 120            |
		| 121            |
		| 122            |
		| 123            |
		| 124            |
		| 125            |
		| 126            |
		| 127            |
		| 128            |
		| 129            |
		| 130            |
		| 131            |
		| 132            |
		| 133            |
		| 134            |
		| 135            |
		| 136            |
		| 137            |
		| 138            |
		| 139            |
		| 140            |
		| 141            |
		| 142            |
		| 143            |
		| 144            |
		| 145            |
		| 146            |
		| 147            |
		| 148            |
		And Enter
		| Field     | Value |
		| free@name | 500   |
		And Click reg_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click btn01@class Wait[Loaded]
		And Enter
		| Field        | Value |
		| site_id@name | 5     |
	Then Assert List postage_list
		| postage_charge |
		| 101            |
		| 102            |
		| 103            |
		| 104            |
		| 105            |
		| 106            |
		| 107            |
		| 108            |
		| 109            |
		| 110            |
		| 111            |
		| 112            |
		| 113            |
		| 114            |
		| 115            |
		| 116            |
		| 117            |
		| 118            |
		| 119            |
		| 120            |
		| 121            |
		| 122            |
		| 123            |
		| 124            |
		| 125            |
		| 126            |
		| 127            |
		| 128            |
		| 129            |
		| 130            |
		| 131            |
		| 132            |
		| 133            |
		| 134            |
		| 135            |
		| 136            |
		| 137            |
		| 138            |
		| 139            |
		| 140            |
		| 141            |
		| 142            |
		| 143            |
		| 144            |
		| 145            |
		| 146            |
		| 147            |
		| 148            |
		And Assert Elements
		| Field     | Value |
		| free@name | 500   |

