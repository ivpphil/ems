﻿@ers7.2Admin
Feature: SystemGeneratedEmalis
	This feature contains test cases for modifying System Generated Emails.


Scenario: Modify System Generated Emails
	# This scenario is for modifying the contents of the various emails to be sent to the customer.
	# Login then open store_mail_text.asp
	# Enter values on the fields
	# Click register and dialog_ok_button then back
	# Assert if the value in the fields are correct
	Given Insert Template 7.2/mail_template_t Into mail_template_t
	When Open /admin Wait[Loaded]
		And Enter
	 	| user_login_id | passwd |
	 	| ivpers              | ivpers       |
		And Click login_button Wait[Loaded]
		And Open /admin/top/store/asp/store_mail_text.asp Wait[Loaded]
	When Enter 
         | Field                     | Value                                      |
         | new_cust_pc_title         | New Customer PC Title                      |
         | new_cust_pc_body          | New Customer PC Body                       |
         | new_cust_mobile_title     | New Customer Mobile Title                  |
         | new_cust_mobile_body      | New Customer Mobile Body                   |
         | ord_comp_pc_title         | Order Completion PC Title                  |
         | ord_comp_pc_body          | Order Completion PC Body                   |
         | ord_comp_mobile_title     | Order Completion Mobile Title              |
         | ord_comp_mobile_body      | Order Completion Mobile Body               |
         | bill_comp_pc_title        | Subscription Order Completion PC Title     |
         | bill_comp_pc_body         | Subscription Order Completion PC Body      |
         | bill_comp_mobile_title    | Subscription Order Completion Mobile Title |
         | bill_comp_mobile_body     | Subscription Order Completion Mobile Body  |
         | del_comp_pc_title         | Delivery Completion PC Title               |
         | del_comp_pc_body          | Delivery Completion PC Body                |
         | del_comp_mobile_title     | Delivery Completion Mobile Title           |
         | del_comp_mobile_body      | Delivery Completion Mobile Body            |
         | with_comp_title           | Cancellation Completion PC Title           |
         | with_comp_body            | Cancellation Completion PC Body            |
         | with_mobile_title         | Cancellation Completion Mobile Title       |
         | with_comp_mobile_body     | Cancellation Completion Mobile Body        |
         | res_pc_title              | Password Reset PC Title                    |
         | res_pc_body               | Password Reset PC Body                     |
         | res_mobile_title          | Password Reset Mobile Title                |
         | res_mobile_body           | Password Reset Mobile Body                 |
         | ind_pc_title              | Individual Email PC Title                  |
         | ind_pc_body               | Individual Email PC Body                   |
         | quest_accept_pc_title     | Inquiry Reply Email PC Title               |
         | quest_accept_pc_body      | Inquiry Reply Email PC Body                |
         | quest_accept_mobile_title | Inquiry Reply Email Mobile Title           |
         | quest_accept_mobile_body  | Inquiry Reply Email Mobile Body            |
         | ques_pc_title             | Inquiry Mail PC Title                      |
         | ques_pc_body              | Inquiry Mail PC Title                      |
		And Click register Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded] 
		And Click back Wait[Loaded]
	Then Assert Elements
		| Field                     | Value                                      |
		| new_cust_pc_title         | New Customer PC Title                      |
		| new_cust_pc_body          | New Customer PC Body                       |
		| new_cust_mobile_title     | New Customer Mobile Title                  |
		| new_cust_mobile_body      | New Customer Mobile Body                   |
		| ord_comp_pc_title         | Order Completion PC Title                  |
		| ord_comp_pc_body          | Order Completion PC Body                   |
		| ord_comp_mobile_title     | Order Completion Mobile Title              |
		| ord_comp_mobile_body      | Order Completion Mobile Body               |
		| bill_comp_pc_title        | Subscription Order Completion PC Title     |
		| bill_comp_pc_body         | Subscription Order Completion PC Body      |
		| bill_comp_mobile_title    | Subscription Order Completion Mobile Title |
		| bill_comp_mobile_body     | Subscription Order Completion Mobile Body  |
		| del_comp_pc_title         | Delivery Completion PC Title               |
		| del_comp_pc_body          | Delivery Completion PC Body                |
		| del_comp_mobile_title     | Delivery Completion Mobile Title           |
		| del_comp_mobile_body      | Delivery Completion Mobile Body            |
		| with_comp_title           | Cancellation Completion PC Title           |
		| with_comp_body            | Cancellation Completion PC Body            |
		| with_mobile_title         | Cancellation Completion Mobile Title       |
		| with_comp_mobile_body     | Cancellation Completion Mobile Body        |
		| res_pc_title              | Password Reset PC Title                    |
		| res_pc_body               | Password Reset PC Body                     |
		| res_mobile_title          | Password Reset Mobile Title                |
		| res_mobile_body           | Password Reset Mobile Body                 |
		| ind_pc_title              | Individual Email PC Title                  |
		| ind_pc_body               | Individual Email PC Body                   |
		| quest_accept_pc_title     | Inquiry Reply Email PC Title               |
		| quest_accept_pc_body      | Inquiry Reply Email PC Body                |
		| quest_accept_mobile_title | Inquiry Reply Email Mobile Title           |
		| quest_accept_mobile_body  | Inquiry Reply Email Mobile Body            |
		| ques_pc_title             | Inquiry Mail PC Title                      |
		| ques_pc_body              | Inquiry Mail PC Title                      |

		