﻿@V7.2Admin
Feature: Bundle
			Bundle Registration
			Bundle Search and Modification

#-------------------------------------------------------
Scenario: Bundle Registration
#-------------------------------------------------------

# This scenario is used to register bundled products to a specific target customers.

	Given Delete From doc_bundling_t
		| ccode     |
		| SAMPBCODE |
	And Delete From doc_target_t
		| ccode     |
		| SAMPBCODE |
	And Delete From campaign_t
		| ccode     |
		| SAMPBCODE |
	And Delete From target_t
		| target_name |
		| GC Buyers   |
	And Insert Template 7.2/g_product_t Into g_master_t
	And Insert Template 7.2/s_product_t Into s_master_t
	And Insert Template 7.2/wh_stock1_t Into wh_stock_t
	And Insert Template 7.2/stock1_t Into stock_t
	And Insert Template 7.2/target_item_t Into target_item_t
	And Insert Template 7.2/target_t Into target_t
	

	When Open /admin Wait[Loaded]
	And Enter
		| user_login_id | passwd |
		| ivpers        | ivpers |
	And Click login_button Wait[Loaded]
	And Open /admin/top/campaign/asp/campaign_regist.asp Wait[Loaded]
	And Enter
		| Field            | Value                  |
		| ccode            | SAMPBCODE              |
		| r_status         | 1                      |
		| campaign_name    | Freebies for GC Buyers |
		| datepicker_start | 2016/04/18             |
		| datepicker_end   | 2016/11/30             |
		| target           | 132                    |
		| site_id          | {1,5}                  |
	And Click nlt_prod_search_btn Wait[Loaded]
	And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Element] s_gcode
	And Enter
		| s_gcode        |
		| ASUS-GTXTITANX |
	And Click search_btn Wait[Element] set_btn
	And Click productSearchList[0].set_btn Wait[None]
	And Close[Popup] Wait[None]
	
	And Click t_add_prod_btn Wait[None]
	And Click nlt_prod_search_btn Wait[Loaded]
	And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Element] s_gcode
	And Enter
		| s_gcode      |
		| GB-R9-280XWF |
	And Click search_btn Wait[Element] set_btn
	And Click productSearchList[0].set_btn Wait[None]
	And Close[Popup] Wait[None]

	And Click nlb_prod_search_btn Wait[Loaded]
	And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Element] s_gcode
	And Enter
		| s_gcode     |
		| addthisitem |
	And Click search_btn Wait[Element] set_btn
	And Click productSearchList[0].set_btn Wait[None]
	And Close[Popup] Wait[None]
	And Enter
		| inc_amount@id | remarks@id |
		| 1             |            |
	And Click register_btn Wait[None]
	And Click dialog_ok_button Wait[Loaded]
	And Click back_btn Wait[Loaded]


#-------------------------------------------------------
Scenario: Bundle Search and Modification
#-------------------------------------------------------

# This scenario is used to search and modify the registered bundle.

	Given Delete From doc_bundling_t
		| ccode     |
		| SAMPBCODE |
	And Delete From doc_target_t
		| ccode     |
		| SAMPBCODE |
	And Delete From campaign_t
		| ccode     |
		| SAMPBCODE |
	And Delete From target_t
		| target_name |
		| GC Buyers   |
	And Insert Template 7.2/g_product_t Into g_master_t
	And Insert Template 7.2/s_product_t Into s_master_t
	And Insert Template 7.2/wh_stock1_t Into wh_stock_t
	And Insert Template 7.2/stock1_t Into stock_t
	And Insert Template 7.2/target_item_t Into target_item_t
	And Insert Template 7.2/target_t Into target_t
	And Insert Template 7.2/campaign1_t Into campaign_t
	And Insert Template 7.2/doc_bundling_t Into doc_bundling_t
	And Insert Template 7.2/doc_target_t Into doc_target_t
	And Insert Template 7.2/campaign1_t Into campaign_t


	When Open /admin Wait[Loaded]
	And Enter
		| user_login_id | passwd |
		| ivpers        | ivpers |
	And Click login_button Wait[Loaded]
	And Open /admin/top/campaign/asp/campaign_search.asp Wait[Loaded]
	And Enter
		| Field              | Value                  |
		| s_ccode@id         | SAMPBCODE              |
		| s_site_id          | {1,5}                  |
		| s_active@id        | 1                      |
		| s_campaign_name@id | Freebies for GC Buyers |
	And Click search_btn Wait[Loaded]
	Then Assert List bundleList
		| l_ccode   | l_campaign_name        | l_status |
		| SAMPBCODE | Freebies for GC Buyers | 有効       |
	When Click bundleList[0].l_details_btn Wait[Loaded]
	And Enter
		| Field            | Value                          |
		| r_status         | 1                              |
		| campaign_name    | Freebies for GC Buyers Updated |
		| datepicker_start | 2016/04/18                     |
		| datepicker_end   | 2017/12/31                     |
		| target           | 132                            |
		| site_id          | {1,5}                          |
	And Click bundledproductList[0].l_del_btn Wait[None]
	And Click nlb_prod_search_btn Wait[Loaded]
	And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Element] s_gcode
	And Enter
		| s_gcode    |
		| dokon_test |
	And Click search_btn Wait[Element] set_btn
	And Click productSearchList[0].set_btn Wait[None]
	And Close[Popup] Wait[None]
	And Enter
		| inc_amount@id | remarks@id             |
		| 1             | Implementation Updated |
	And Click change_btn Wait[None]
	And Click dialog_ok_button Wait[Loaded]
	And Click back_btn Wait[Loaded]
	Then Assert Elements
		| Field         | Value                          |
		| r_status      | 1                              |
		| campaign_name | Freebies for GC Buyers Updated |
		| target        | 132                            |
		| site_id       | {1,5}                          |
	And Assert List targetList
        | lt_prod_code   | lt_prod_name                                | sales_method |
        | ASUS-GTXTITANX | Asus GTX TITAN X 12GB GDDR5 384Bit          | 1            |
        | GB-R9-280XWF   | Gigabyte R9-280X WINDFORCE 3GB GDDR5 384bit | 1            |
	And Assert List bundledproductList
        | lb_prod_code | lb_prod_name | lb_inc_amount | lb_duplicate |
        | dokon_test   | dokon_test   | 1             | 1            |
	And Assert Elements
		| remarks@name           |
		| Implementation Updated |