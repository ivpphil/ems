﻿@ers7.2Admin
Feature: DeliverySettings
	This feature contains test cases for modifying Delivery Setting


Scenario: Modify Delivery Setting
	# This scenario is for updating shipping and delivery settings.
	# Login then open store_delivery.asp
	# Modify the values
	# Click updatePlugin and dialog_ok_button then click back
	# Assert registered value
	Given Insert Template 7.2/setup_t Into setup_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/store/asp/store_delivery.asp Wait[Loaded]
			And Enter
			 | Field         | Value                   |
			 | delivery      | Codev                   |
			 | url           | http://www.codev.co.ph/ |
			 | mail_delivery | IVP                     |
			 | mail_url      | http://www.ivp.co.jp/   |
			 And Click register Wait[Loaded]
			 And Click dialog_ok_button Wait[Loaded]
			 And Click back_button Wait[Loaded]
		Then Assert Elements
			 | Field         | Value                   |
			 | delivery      | Codev                   |
			 | url           | http://www.codev.co.ph/ |
			 | mail_delivery | IVP                     |
			 | mail_url      | http://www.ivp.co.jp/   |



	