﻿@ers7.2Admin
Feature: ModifyProductPrice

Scenario: Modify Product Pricing for SKU 
	-------------------------------------------------------
	 Given Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/stock_t Into stock_t
	 When Open /admin Wait[Loaded]
		  And Enter
		   | user_login_id | passwd  |
		   | ivpers       | ivpers |
		  And Click login_button Wait[Loaded]
		  And Open /admin/top/price/asp/price_search.asp Wait[Loaded]
		  And Enter
		   | Field             | Value    |
		   | s_gcode           | GOLDMAKER   |
		   | s_gname           |          |
		   | s_scode           |          |
		   | s_sname           |          |
		   | price_search_type | SKU_LIST |
		  And Click price_search_submit Wait[Loaded]
	 Then Assert List pricelist
		  | price_gcode | price_scode | price_sname    | price_normal | price_msrp | price_regular | price_first_regular | price_sale |
		  | GOLDMAKER   | GOLDMAKER01 | GOLDMAKER01 | 101,000円     | 150,000円   | 円             | 円                   | 円          |
	 When Click pricelist[0].price_Sdetail Wait[Loaded]
		And Enter
		  | Field            | Value  |
		  | p_detail_normal  | 101000 |
		  | p_detail_msrp    | 150000 |
		  | p_detail_cost    | 100000 |
		  | p_detail_regular |        |
		  | p_detail_first   |        |
		  And Enter List p_detail_rank_normal
		  | rank_price |
		  | 40000      |
		  | 40000      |
		  | 20000      |
		  | 20000      |
		  | 10000      |
		  | 10000      |
		  And Enter List p_detail_rank_regular
		  | rank_price |
		  | 40000      |
		  | 40000      |
		  | 20000      |
		  | 20000      |
		  | 10000      |
		  | 10000      |        
		  And Enter List p_detail_rank_first
		  | rank_price |
		  | 40000      |
		  | 40000      |
		  | 20000      |
		  | 20000      |
		  | 10000      |
		  | 10000      |
		  And Enter List p_detail_sale
		  | sale_delete | start_date          | end_date            | value |
		  |             | 2016/02/17 02:00:00 | 2016/04/20 02:00:00 | 200    |
		  And Click p_detail_change Wait[Loaded]
		  And Click back Wait[Loaded]
	 Then Assert Elements
		  | Field            | Value  |
		  | p_detail_normal  | 101000 |
		  | p_detail_msrp    | 150000 |
		  | p_detail_cost    | 100000 |
		  | p_detail_regular |        |
		  | p_detail_first   |        |
		  And Assert List p_detail_rank_normal
		  | rank_price |
		  | 40000      |
		  | 40000      |
		  | 20000      |
		  | 20000      |
		  | 10000      |
		  | 10000      |
		  And Assert List p_detail_rank_regular
		  | rank_price |
		  | 40000      |
		  | 40000      |
		  | 20000      |
		  | 20000      |
		  | 10000      |
		  | 10000      |      
		  And Assert List p_detail_rank_first
		  | rank_price |
		  | 40000      |
		  | 40000      |
		  | 20000      |
		  | 20000      |
		  | 10000      |
		  | 10000      |
		  And Assert List p_detail_sale
		  | sale_delete | start_date           | end_date              | value |
		  |             | 2/17/2016 3:00:00 AM | 4/20/2016 3:00:00 AM | 200   |


Scenario: Modify Product Pricing for GROUP 
	 Given Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/stock_t Into stock_t
	 When Open /admin Wait[Loaded]
		  And Enter
		   | user_login_id | passwd |
		   | ivpers        | ivpers |
		  And Click login_button Wait[Loaded]
		  And Open /admin/top/price/asp/price_search.asp Wait[Loaded]
		  And Enter
		   | Field             | Value          |
		   | s_gcode           | milk101 |
		   | s_gname           |                |
		   | s_scode           |                |
		   | s_sname           |                |
		   | price_search_type | GROUP_LIST     |
		  And Click price_search_submit Wait[Loaded]
	 Then Assert List pricelist
		  | price_gcode | price_sname         | price_normal | price_msrp | price_regular | price_first_regular | price_sale |
		  | milk101     | Cow Head Fresh Milk | 1,000円       | 2,000円     | 円             | 円                   | 円          |
	 When Click pricelist[0].price_Gdetail Wait[Loaded]
		And Enter
		  | Field            | Value |
		  | p_detail_normal  | 5000  |
		  | p_detail_msrp    | 5500  |
		  | p_detail_cost    | 100   |
		  | p_detail_regular | 4000  |
		  | p_detail_first   | 4500  |
		  And Enter List p_detail_rank_normal
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |
		  And Enter List p_detail_rank_regular
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |         
		  And Enter List p_detail_rank_first
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |
		  And Enter List p_detail_sale
		  | sale_delete | start_date          | end_date            | value |
		  |             | 2016/02/17 02:00:00 | 2017/02/20 02:00:00 | 50    |
		  And Click p_detail_change Wait[Loaded]
		  And Click back Wait[Loaded]
	 Then Assert Elements
		 | Field            | Value |
		 | p_detail_normal  | 5000  |
		 | p_detail_msrp    | 5500  |
		 | p_detail_cost    | 100     |
		 | p_detail_regular | 4000  |
		 | p_detail_first   | 4500  |
		  And Assert List p_detail_rank_normal
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |
		  And Assert List p_detail_rank_regular
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |      
		  And Assert List p_detail_rank_first
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |
		  And Assert List p_detail_sale
		  | sale_delete | start_date           | end_date             | value |
		  |             | 2/17/2016 3:00:00 AM | 2/20/2017 3:00:00 AM | 50    |
	 When Open /admin/top/price/asp/price_search.asp Wait[Loaded]
		  And Enter
		   | Field             | Value    |
		   | s_gcode           | milk101  |
		   | s_gname           |          |
		   | s_scode           |          |
		   | s_sname           |          |
		   | price_search_type | SKU_LIST |
		   And Click price_search_submit Wait[Loaded]
		   And Click pricelist[0].price_Sdetail Wait[Loaded]
	 Then Assert Elements
		  | Field            | Value |
		  | p_detail_normal  | 5000  |
		  | p_detail_msrp    | 5500  |
		  | p_detail_cost    | 100     |
		  | p_detail_regular | 4000  |
		  | p_detail_first   | 4500  |
		  And Assert List p_detail_rank_normal
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |
		  And Assert List p_detail_rank_regular
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |      
		  And Assert List p_detail_rank_first
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |
		  And Assert List p_detail_sale
		  | sale_delete | start_date           | end_date             | value |
		  |             | 2/17/2016 3:00:00 AM | 2/20/2017 3:00:00 AM | 50    |
	 When Click back Wait[Loaded]
		  And Click pricelist[1].price_Sdetail Wait[Loaded]
	 Then Assert Elements
		  | Field            | Value |
		  | p_detail_normal  | 5000  |
		  | p_detail_msrp    | 5500  |
		  | p_detail_cost    | 100     |
		  | p_detail_regular | 4000  |
		  | p_detail_first   | 4500  |
		  And Assert List p_detail_rank_normal
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |
		  And Assert List p_detail_rank_regular
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |      
		  And Assert List p_detail_rank_first
		  | rank_price |
		  | 43000 |
		  | 43000 |
		  | 25000 |
		  | 25000 |
		  | 18000 |
		  | 18000 |
		  And Assert List p_detail_sale
		  | sale_delete | start_date           | end_date             | value |
		  |             | 2/17/2016 3:00:00 AM | 2/20/2017 3:00:00 AM | 50    |
