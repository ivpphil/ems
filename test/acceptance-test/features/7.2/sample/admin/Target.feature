﻿@V7.2Admin
Feature: Target

#-------------------------------------------------------
Scenario: Target Registration
#-------------------------------------------------------

# This scenario is used to register new Target.

	Given Delete From target_t
		| target_name    |
		| GC Buyers      |
	And Delete From target_item_t
         | target_id |
         | 132       |
	And Delete From target_item_t
		| gcode          |
		| ASUS-GTXTITANX |
		| GB-R9-280XWF   |
		| AMDFX-8350BE   |
		

	When Open /admin Wait[Loaded]
	And Enter
		| user_login_id | passwd |
		| ivpers        | ivpers |
	And Click login_button Wait[Loaded]
	And Open /admin/top/campaign/asp/target_regist.asp Wait[Loaded]
	And Enter
		| Field          | Value     |
		| target_name    | GC Buyers |
		| site_id        | {1,5}     |
		| recency_from   | 5         |
		| recency_to     | 365       |
		| frequency_from | 1         |
		| frequency_to   | 1000      |
		| monetary_from  | 1000      |
		| monetary_to    | 999999999 |
		| order_ptn_kbn  | 3         |
	And Click p_product_search_btn Wait[Loaded]
	And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Element] s_gcode
	And Enter
		| Field   | Value          |
		| s_gcode | ASUS-GTXTITANX |
	And Click search_btn Wait[Element] s_gcode
	And Click productSearchList[0].set_btn Wait[None]
	And Close[Popup] Wait[None]

	And Click p_add_another_item_btn Wait[Loaded]
	And Click p_product_search_btn Wait[Loaded]
	And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Element] s_gcode
	And Enter
		| Field   | Value        |
		| s_gcode | GB-R9-280XWF |
	And Click search_btn Wait[Element] s_gcode
	And Click productSearchList[0].set_btn Wait[None]
	And Close[Popup] Wait[None]


	And Click e_product_search_btn Wait[Loaded]
	And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Element] s_gcode
	And Enter
		| Field   | Value        |
		| s_gcode | AMDFX-8350BE |
	And Click search_btn Wait[Element] s_gcode
	And Click productSearchList[0].set_btn Wait[None]
	And Close[Popup] Wait[None]
	
	And Click register_btn Wait[None]
	And Click dialog_ok_button Wait[Loaded]
	And Click back_btn Wait[Loaded]


#-------------------------------------------------------
Scenario: Target Search and Modification
#-------------------------------------------------------

# This scenario is used to search the Target and edit the data.

	Given Delete From target_t
		| target_name    |
		| GC Buyers      |
	And Delete From target_item_t
         | target_id |
         | 132       |
	And Delete From target_item_t
         | gcode          | scode          |
         | ASUS-GTXTITANX | ASUS-GTXTITANX |
         | GB-R9-280XWF   | GB-R9-280XWF   |
         | AMDFX-8350BE   | AMDFX-8350BE   |
	
	And Insert Template 7.2/target_t Into target_t
	And Insert Template 7.2/target_item_t Into target_item_t

	When Open /admin Wait[Loaded]
	And Enter
		| user_login_id | passwd |
		| ivpers        | ivpers |
	And Click login_button Wait[Loaded]
	And Open /admin/top/campaign/asp/target_search.asp Wait[Loaded]
	And Enter
		| Field                 | Value      |
		| s_intime_from@name    | 2016/04/14 |
		| s_intime_to@name      | 2016/04/15 |
		| s_target_name@name    | GC Buyers  |
		| s_site_id@name        | {1,5}      |
		| s_recency_from@name   | 5          |
		| s_recency_to@name     | 365        |
		| s_frequency_from@name | 1          |
		| s_frequency_to@name   | 1000       |
		| s_monetary_from@name  | 1000       |
		| s_monetary_to@name    | 999999999  |
	And Click search_btn Wait[Loaded]
	Then Assert List targetList
		| l_target_name | l_list_count | l_recency_from | l_recency_to | l_frequency_from | l_frequency_to | l_monetary_from | l_monetary_to | l_purchase_course |
		| GC Buyers     | 12           | 5              | 365          | 1                | 1000           | 1000            | 999999999     |                   |
	When Click targetList[0].details_btn Wait[Loaded]
	Then Assert Elements
		| Field          | Value     |
		| target_name    | GC Buyers |
		| site_id        | {1,5}     |
		| recency_from   | 5         |
		| recency_to     | 365       |
		| frequency_from | 1         |
		| frequency_to   | 1000      |
		| monetary_from  | 1000      |
		| monetary_to    | 999999999 |
		| order_ptn_kbn  | 3         |
	And Assert List in_list
		| in_prod_num    | in_prod_name                                | in_order_type |
		| ASUS-GTXTITANX | Asus GTX TITAN X 12GB GDDR5 384Bit          | 1             |
		| GB-R9-280XWF   | Gigabyte R9-280X WINDFORCE 3GB GDDR5 384bit | 1             |
	And Assert List ex_list
		| ex_prod_num  | ex_prod_name                             | ex_order_type |
		| AMDFX-8350BE | AMD Vishera FX-8350 Black Edition 4.0GHz | 1             |
	
	When Click in_list[1].in_del_btn Wait[None]
	And Click p_product_search_btn Wait[Loaded]

	And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Element] s_gcode
	And Enter
		| Field   | Value |
		| s_gcode | mfcd  |
	And Click search_btn Wait[Element] s_gcode
	And Click productSearchList[0].set_btn Wait[None]
	And Close[Popup] Wait[None]

	And Click e_product_search_btn Wait[Loaded]
	And Switch[Popup] /admin/top/campaign/asp/common_scode_search.asp Wait[Element] s_gcode
	And Enter
		| Field   | Value        |
		| s_gcode | AMDA10-7850C |
	And Click search_btn Wait[Element] s_gcode
	And Click productSearchList[0].set_btn Wait[None]
	And Close[Popup] Wait[None]
	And Click modify_btn Wait[Loaded]
	And Click dialog_ok_button Wait[Loaded]
	And Click back_btn Wait[Loaded]