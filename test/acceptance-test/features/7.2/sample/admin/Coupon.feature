﻿@V7.2Admin

Feature: Coupon
	It contains bank transfer payment registration, modification and deletion.

#----------------------------------------------------------------------------------------------
Scenario: Coupon Registration
#----------------------------------------------------------------------------------------------
#This test opens the Coupon Registration as an Admin
#It deletes the data in the database wtih the same coupon_code as our sample
#It fill in the information regarding the new coupon
#It clicks the reg button to register the coupon
#It proceed to completion page then go back to the list
#It checks the information in the list if it is the same/correct.
#----------------------------------------------------------------------------------------------

	Given Delete From coupon_t
		| coupon_code |
		| ALY_COUP1   |
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/promotion/asp/coupon_regist.asp Wait[Loaded]
		And Enter
		| Field            | Value               |
		| coupon_code@name | ALY_COUP1           |
		| site_id@name     | {1,5}               |
		| coupon_type@name | 1                   |
		| price@name       | 300                 |
		| base_price@name  | 3000                |
		| start_date@name  | 2016/01/29 00:00:00 |
		| end_date@name    | 2026/01/29 23:59:59 |
		| active@name      | 1                   |
		And Click reg_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click btn01@class Wait[Loaded]
	Then Assert List coupon_list
		| coupon_code | coupon_type         | price | base_price | start_date           | end_date              | active |
		| ALY_COUP1   | ワンタイムクーポン   | 300   | 3000       | 1/29/2016 1:00:00 AM | 1/30/2026 12:59:59 AM | 有効   |


#----------------------------------------------------------------------------------------------
Scenario: Coupon Modification
#----------------------------------------------------------------------------------------------
#This test opens the Coupon Search as an Admin
#It deletes the data in the database wtih the same coupon_code as our sample
#It inserts the template into the database
#It narrow down the search result by filling in the search criterias
#It click the edit button
#It modifies the information of the coupon then click the modify button
#It proceed to completion page then go back to the list
#It checks the information in the list if it is the same/correct.
#----------------------------------------------------------------------------------------------


	Given Delete From coupon_t
		| coupon_code |
		| ALY_COUP1   |
		And Insert Template 7.2/coupon_t Into coupon_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/promotion/asp/coupon_search.asp Wait[Loaded]
		And Enter
		| Field                  | Value               |
		| s_intime_from@name     |                     |
		| s_intime_to@name       |                     |
		| s_coupon_code@name     | ALY_COUP1           |
		| s_site_id@name         | {1,5}               |
		| s_coupon_type@name     | 1                   |
		| s_price_from@name      | 100                 |
		| s_price_to@name        | 500                 |
		| s_base_price_from@name | 1000                |
		| s_base_price_to@name   | 3500                |
		| s_start_date_from@name | 2016/01/01 00:00:00 |
		| s_start_date_to@name   | 2016/12/31 23:59:59 |
		| s_end_date_from@name   | 2016/02/01 00:00:00 |
		| s_end_date_to@name     | 2026/01/29 23:59:59 |
		| active@name            | 1                   |
		And Click search_btn@name Wait[Loaded]
		And Click download_csv_page Wait[Loaded]
		And Click download_csv_all Wait[Loaded]
		And Click coupon_list[0].dtl_btn Wait[Loaded]
		And Enter
		| Field            | Value               |
		| coupon_code@name | ALY_COUP1           |
		| site_id@name     | {1}                 |
		| coupon_type@name | 0                   |
		| price@name       | 500                 |
		| base_price@name  | 5000                |
		| start_date@name  | 2016/01/01 00:00:00 |
		| end_date@name    | 2025/03/29 23:59:59 |
		| active@name      | 0                   |
		And Click modify_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click btn01@class Wait[Loaded]
	Then Assert List coupon_list
		| coupon_code         | coupon_type        | price | base_price | start_date            | end_date              | active   |
		| YAMA_C2             | ワンタイムクーポン   | 10000 | 100        | 3/1/2016 12:00:00 AM | 3/31/2016 11:59:59 PM  | 有効     |
		| YAMA_C              | ワンタイムクーポン   | 5000  | 100        | 3/7/2016 12:00:00 AM | 12/31/2017 11:59:59 PM | 有効     |
		| TOASTED             | ワンユーザクーポン   | 100   | 500        | 2/1/2016 12:00:00 AM | 6/30/2016 11:59:59 PM  | 有効     |
		| SUDO APT-GET COUPON | ワンタイムクーポン   | 2000  | 600        | 1/1/2016 12:00:00 AM | 3/31/2016 11:59:59 PM  | 有効     |
		| ALY_COUP1           | ワンユーザクーポン   | 500   | 5000       | 1/1/2016 1:00:00 AM  | 3/30/2025 12:59:59 AM  | 無効     |


#----------------------------------------------------------------------------------------------
Scenario: Coupon Deletion
#----------------------------------------------------------------------------------------------
#This test opens the Coupon Search as an Admin
#It deletes the data in the database wtih the same coupon_code as our sample
#It inserts the template into the database
#It narrow down the search result by filling in the search criterias
#It clicks the edit button
#It clicks the delete button
#It proceed to completion page then go back to the list
#It checks if the coupon is no longer in the list
#----------------------------------------------------------------------------------------------

	Given Delete From coupon_t
		| coupon_code |
		| ALY_COUP1   |
		And Insert Template 7.2/coupon_t Into coupon_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/promotion/asp/coupon_search.asp Wait[Loaded]
		And Enter
		| Field                  | Value               |
		| s_intime_from@name     |                     |
		| s_intime_to@name       |                     |
		| s_coupon_code@name     | ALY_COUP1           |
		| s_site_id@name         | {1,5}               |
		| s_coupon_type@name     | 1                   |
		| s_price_from@name      | 100                 |
		| s_price_to@name        | 500                 |
		| s_base_price_from@name | 1000                |
		| s_base_price_to@name   | 3500                |
		| s_start_date_from@name | 2016/01/01 00:00:00 |
		| s_start_date_to@name   | 2016/12/31 23:59:59 |
		| s_end_date_from@name   | 2016/02/01 00:00:00 |
		| s_end_date_to@name     | 2026/01/29 23:59:59 |
		| active@name            | 1                   |
		And Click search_btn@name Wait[Loaded]
		And Click coupon_list[0].dtl_btn Wait[Loaded]
		And Enter
		| Field            | Value               |
		| coupon_code@name | ALY_COUP1           |
		| site_id@name     | {1}               |
		| coupon_type@name | 0                   |
		| price@name       | 500                 |
		| base_price@name  | 5000                |
		| start_date@name  | 2016/01/01 00:00:00 |
		| end_date@name    | 2025/03/29 23:59:59 |
		| active@name      | 0                   |
		And Click del_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click btn01@class Wait[Loaded]
	Then Assert Deleted List coupon_list
		| coupon_code |
		| ALY_COUP1   |



