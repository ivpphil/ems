﻿@ers7.2Admin
Feature: SupplierRegistration
	This feature contains test cases for supplier registration/search and modification of supplier

Scenario: Register Supplier
	# This scenario is to register a supplier
	# Login then open supplier_regist.asp
	# Enter values on the fields
	# Click register and dialog_ok_button then back
	Given Insert Template 7.2/wh_supplier_t Into wh_supplier_t
		And Delete From wh_supplier_t
         | supplier_code |
         | CODEV              |
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/warehouse/asp/supplier_regist.asp Wait[Loaded]
			And Enter
			| Field         | Value                      |
			| supplier_code | CODEV                      |
			| supplier_name | Codev Global Solutions Inc |
			| zip           | 650-0012                   |
			And Click autofill Wait[Loaded]
			And Enter
			| Field | Value           |
			| tel   | 12345678        |
			| fax   | 12345678        |
			| email | codev@ivp.co.jp |
			And Click register Wait[Loaded]
			And Click dialog_ok_button Wait[Loaded]
			And Click back Wait[Loaded]


Scenario: Search Supplier
	# This scenario is to search a supplier
	# Login then open supplier_search.asp
	# Enter values on the fields
	# Click search the assert the supplierlist
	# Click supplier_detail button then assert
	Given Insert Template 7.2/wh_supplier_t Into wh_supplier_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
		When Open /admin/top/warehouse/asp/supplier_search.asp Wait[Loaded]
			And Enter
			| Field           | Value |
			| s_supplier_code | CODEV |
			| s_supplier_name |       |
			| s_zip           |       |
			| s_pref          |       |
			| s_address       |       |
			| s_tel           |       |
			| s_fax           |       |
			| s_email         |       |
			And Click search Wait[Loaded]
		Then Assert List supplierlist
			| supplier_code | supplier_name              | address                 | tel      | email           |
			| CODEV         | Codev Global Solutions Inc | 650-0012 兵庫県 神戸市中央区北長狭通 | 12345678 | codev@ivp.co.jp |
		When Click supplierlist[0].supplier_detail Wait[Loaded]
		Then Assert Elements
			| Field         | Value                      |
			| supplier_code | CODEV                      |
			| supplier_name | Codev Global Solutions Inc |
			| zip           | 650-0012                   |
			| pref          | 28                         |
			| address       | 神戸市中央区北長狭通        |
			| tel           | 12345678                   |
			| fax           | 12345678                   |
			| email         | codev@ivp.co.jp            |


 
Scenario: Modify Supplier
	# This scenario is to modify a supplier
	# Login then open supplier_search.asp
	# Enter values on the fields
	# Click search and supplier_detail button
	# Then enter values to modify
	# Click change button and dialog_ok_button then back
	# Click supplier_detail button then assert
	Given Insert Template 7.2/wh_supplier_t Into wh_supplier_t
		And Delete From wh_supplier_t
		| supplier_code |
		| IVP           |
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
		When Open /admin/top/warehouse/asp/supplier_search.asp Wait[Loaded]
			And Enter
			| Field           | Value |
			| s_supplier_code | CODEV                      |
			| s_supplier_name | Codev Global Solutions Inc |
			| s_zip           | 650-0012                   |
			| s_pref          | 28                         |
			| s_address       | 神戸市中央区北長狭通        |
			| s_tel           | 12345678                   |
			| s_fax           | 12345678                   |
			| s_email         | codev@ivp.co.jp            |
			And Click search Wait[Loaded]
		When Click supplierlist[0].supplier_detail Wait[Loaded]
			And Enter
			| Field           | Value |
			| supplier_code | IVP                      |
			| supplier_name | IVP Global Solutions Inc |
			| zip           | 060-0035                  |
			And Click autofill Wait[Loaded]
			And Enter
			| Field | Value         |
			| tel   | 99999999    |
			| fax   | 99999999    |
			| email | ivp@ivp.co.jp |
			And Click change Wait[Loaded]
			And Click dialog_ok_button Wait[Loaded]
			And Click backbtn Wait[Loaded]
			And Enter
			| Field         | Value                    |
			| s_supplier_code | IVP                      |
			| s_supplier_name | IVP Global Solutions Inc |
			| s_zip           | 060-0035                 |
			| s_pref          | 1                        |
			| s_address       | 札幌市中央区北五条東               |
			| s_tel           | 99999999                 |
			| s_fax           | 99999999                 |
			| s_email         | ivp@ivp.co.jp            |
			And Click search Wait[Loaded]
			And Click supplierlist[0].supplier_detail Wait[Loaded]
		Then Assert Elements
			| Field         | Value                    |
			| supplier_code | IVP                      |
			| supplier_name | IVP Global Solutions Inc |
			| zip           | 060-0035                 |
			| pref          | 1                        |
			| address       | 札幌市中央区北五条東               |
			| tel           | 99999999                 |
			| fax           | 99999999                 |
			| email         | ivp@ivp.co.jp            |
