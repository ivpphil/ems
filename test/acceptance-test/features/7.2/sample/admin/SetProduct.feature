﻿@V7.2Admin

Feature: SetProduct
	It contains Set Product Search and Modification

#--------------------------------------------------------------------------------------
Scenario: Set Product Search
#--------------------------------------------------------------------------------------
#This test case opens the Set Product Search in ERSv7.2 as an admin
#It deletes the data in the database with the ff. condition
#It inserts the template into the database
#It searches the set product which parent_scode is SPN01
#It downloads the 2 csv files of the set product
#It checks the data in the list
#It clicks the details button
#It checks the data of the set product

	Given Delete From g_master_t
			| gcode |
			| SPN   |
		And Delete From s_master_t
			| gcode |
			| SPN   |
		And Delete From price_t
			| gcode |
			| SPN   |
		And Delete From stock_t
			| scode |
			| SPN01 |
		And Delete From wh_stock_t
			| scode |
			| SPN01 |
		And Delete From set_master_t
			| parent_scode |
			| SPN01        |
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t
		And Insert Template 7.2/set_master_t Into set_master_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/item/asp/set_item_search.asp Wait[Loaded]
		And Enter
		| s_parent_scode@name | s_child_scode@name |
		| SPN01               |                    |
		And Click search_btn@name Wait[Loaded] 
		And Click dl_all Wait[Loaded]
		And Click dl_page Wait[Loaded]
		Then Assert List setproduct_list
		| scode | sname |
		| SPN01 | SPOON |

	When Click setproduct_list[0].dtls_btn Wait[Loaded]
	Then Assert Elements
         | Field               | Value |
         | parent_scode        | SPN01 |
         | parent_sname        | SPOON |
         | price               | 30    |
         | regular_price       |       |
         | regular_first_price |       |

	 And Assert List child_list
		| child_scode | child_sname            | amount |
		| HDC         | Hershey Dark Chocolate | 1      |



#--------------------------------------------------------------------------------------
Scenario: Set Product Modification
#--------------------------------------------------------------------------------------
#It deletes the data in the database with the certain conditions
#This test case opens the Set Product Search in ERSv7.2 as an admin
#It inserts the template into the database
#It searches the set product which parent_scode is SPN01
#It clicks the detail button of the set product in the list
#It adds product under that set
#It deletes one of the product and set the quantity of the other product
#It registers the changes by clicking the register button
#It checks the data in the set product

	Given Delete From g_master_t
			| gcode |
			| SPN   |
		And Delete From s_master_t
			| gcode |
			| SPN   |
		And Delete From price_t
			| gcode |
			| SPN   |
		And Delete From stock_t
			| scode |
			| SPN01 |
		And Delete From wh_stock_t
			| scode |
			| SPN01 |
		And Delete From set_master_t
			| parent_scode |
			| SPN01        |
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t
		And Insert Template 7.2/set_master_t Into set_master_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/item/asp/set_item_search.asp Wait[Loaded]
		And Enter
		| s_parent_scode@name | s_child_scode@name |
		| SPN01               |                    |
		And Click search_btn@name Wait[Loaded]
		And Click setproduct_list[0].dtls_btn Wait[Loaded]
		
	When Click search_button@name Wait[Loaded]
	    And Switch[Popup] /admin/top/item/asp/common_item_search.asp Wait[Loaded]
		And Enter
		    | Field        | Value |
		    | s_gcode@name |       |
		    | s_scode@name | HMC   |
		    | s_sname@name |       |
		    #| s_cate1@name |       |
		    #| s_cate2@name |       |
		    #| s_cate3@name |       |
		    #| s_cate4@name |       |
		    #| s_cate5@name |       |
		And Click submit Wait[Loaded]
		And Click item_list[0].set_btn Wait[None]
		And Close[Popup] Wait[URL] /admin/top/item/asp/set_item_modify.asp		
		And Enter List child_list
		| amount |
		| 1      |
		| 2      |
		And Click child_list[0].delete Wait[Loaded]
		And Click reg_btn Wait[Loaded]
		And Click dialog_ok_button Wait[None] 
		And Click btn01@class Wait[Loaded]

	   Then Assert List child_list
		| child_scode | child_sname                | amount |
		| HMC         | Hershey Milk Chocolate Bar | 2      |
		And Assert Deleted List child_list
			| child_scode |
			| HDC         |


