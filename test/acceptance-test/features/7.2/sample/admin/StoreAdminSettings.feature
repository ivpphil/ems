﻿@V7.2Admin
Feature: Store Admin Settings
	Contains the test cases that modifies the Mail Settings and the User and Permission Settings (ERS v7.2)

#-------------------------------------------------------------------------------------------------------
Scenario: Mail Setting Modification
#-------------------------------------------------------------------------------------------------------
#This scenerio inserts the templates in the database
#It opens the Store Admin Settings as an Admin
#It modifies the mail setting(ERS's sales outlet, E-mail Address of the sender, receiver and admin)
#Registers the changes by clicking the register button
#Goes to completion page then goes back
#Chooses the sales outlet if its (ERS[1], ERS site2[5])
#Checks the value of the ff. mail address under that sales outlet

	Given Insert Template 7.2/administrator_t Into administrator_t
		And Insert Template 7.2/setup_t Into setup_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/store/asp/store_admin.asp Wait[Loaded] 
		And Enter
			| Field              | Value             |
			| site_id@name       | 5                 |
			| replying_mail_addr | nagaike@ivp.co.jp |
			| report_mail_addr1  | nagaike@ivp.co.jp |
			| report_mail_addr2  | sample1@ivp.co.jp |
			| report_mail_addr3  | sample2@ivp.co.jp |
			| quest_email_to     | sample3@ivp.co.jp |

		And Click reg_btn Wait[Loaded] 
		And Click dialog_ok_button Wait[Loaded] 
		And Click back_btn Wait[Loaded] 
		And Enter
			| Field              | Value             |
			| site_id@name       | 5                 |   
	Then Assert Elements
		| Field              | Value             |
		| replying_mail_addr | nagaike@ivp.co.jp |
		| report_mail_addr1  | nagaike@ivp.co.jp |
		| report_mail_addr2  | sample1@ivp.co.jp |
		| report_mail_addr3  | sample2@ivp.co.jp |
		| quest_email_to     | sample3@ivp.co.jp |



#-------------------------------------------------------------------------------------------------------
Scenario: User Setting Modification
#-------------------------------------------------------------------------------------------------------
#This scenerio inserts the templates in the database
#It opens the Store Admin Settings as an Admin
#This scenerio registers and modifies the accounts and their authority in ERS Admin and CTS
#Goes to completion page then goes back
#Checks if the changes made is reflected in the system

	Given Insert Template 7.2/administrator_t Into administrator_t
		And Insert Template 7.2/setup_t Into setup_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/store/asp/store_admin.asp Wait[Loaded] 
	    And Enter List user_list
			 | user_login_id | passwd   | user_name   | role_gcode | agent_id |
			| ivpers        | ivpers   | テスト管理者    | ADMIN      | 2        |
			| TESTING       | TESTING  | TESTINGU       | ADMIN      | 2        |
			| gyousya       | gyousya  | 倉庫業者        | ADMIN      | 2        |
			| hansoku       | hansoku  | 販促管理者      | ADMIN      | 2        |
			| nagaike       | nagaike  | 長池           | 5          |          |
			| konomi        | konomi   | てすとこのみ    | ADMIN      | 2        |
			| mitch         | 123123   | test           | ADMIN      | 2        |
			| sugai         | sugai123 | sugaiテスト    | ADMIN       | 2        |
			| morita        | morita   | moritaテスト   | 10          | 2        |
			| takahara      | takahara | タカハラテスト  | ADMIN       | 2        |
			| warehouse1    | 12345678 | warehouse 1    | 12          | 2        |
		And Click reg_btn Wait[Loaded] 
		And Click dialog_ok_button Wait[Loaded] 
		And Click back_btn Wait[Loaded] 

	Then Assert List user_list
	        | user_login_id | passwd   | user_name   | role_gcode | agent_id|
			| ivpers        | ivpers   | テスト管理者    | ADMIN      | 2    |
			| TESTING       | TESTING  | TESTINGU       | ADMIN      | 2    |
			| gyousya       | gyousya  | 倉庫業者        | ADMIN      | 2    |
			| hansoku       | hansoku  | 販促管理者      | ADMIN      | 2    |
			| nagaike       | nagaike  | 長池           | 5          |      |
			| konomi        | konomi   | てすとこのみ    | ADMIN      | 2    |
			| mitch         | 123123   | test           | ADMIN      | 2    |
			| sugai         | sugai123 | sugaiテスト    | ADMIN      | 2     |
			| morita        | morita   | moritaテスト   | 10         | 2     |
			| takahara      | takahara | タカハラテスト  | ADMIN      | 2     |
			| warehouse1    | 12345678 | warehouse 1    | 12         | 2     |

#-------------------------------------------------------------------------------------------------------
Scenario:  User Account Deletion
#-------------------------------------------------------------------------------------------------------
#This scenerio inserts the templates in the database
#It opens the Store Admin Settings as an Admin
#It deletes the accounts and their authority in ERS Admin and CTS by putting a value of 1 under the delete column
#Saves the changes by clicking the register button
#Goes to completion page then goes back
#Checks if the deleted accounts is no longer in the system

	Given Insert Template 7.2/administrator_t Into administrator_t
		And Insert Template 7.2/setup_t Into setup_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd  |
			| ivpers       | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/store/asp/store_admin.asp Wait[Loaded] 
	    And Enter List user_list
		| delete | user_login_id | passwd   | user_name   | role_gcode | agent_id |
		|        | ivpers        | ivpers   | テスト管理者    | ADMIN      | 2        |
		| 1      | syouhin       | syouhin  | 商品登録者      | ADMIN      | 2        |
		| 1      | gyousya       | gyousya  | 倉庫業者        | ADMIN      | 2        |
		| 1      | hansoku       | hansoku  | 販促管理者      | ADMIN      | 2        |
		| 1      | nagaike       | nagaike  | 長池            | 5         |          |
		| 1      | konomi        | konomi   | てすとこのみ    | ADMIN      | 2        |
		|        | mitch         | 123123   | test           | ADMIN      | 2        |
		|        | sugai         | sugai123 | sugaiテスト    | ADMIN      | 2        |
		|        | morita        | morita   | moritaテスト   | 10         | 2        |
		|        | takahara      | takahara | タカハラテスト  | ADMIN      | 2        |
		|        | warehouse1    | 12345678 | warehouse 1    | 12         | 2      |

		And Click reg_btn Wait[Loaded] 
		And Click dialog_ok_button Wait[Loaded] 
		And Click back_btn Wait[Loaded] 

	Then Assert Deleted List user_list
	     | user_login_id |
	     | syouhin       |
	     | gyousya       |
	     | hansoku       |
	     | nagaike       |
	     | konomi        |