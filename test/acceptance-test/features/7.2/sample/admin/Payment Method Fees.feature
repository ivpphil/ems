﻿@V7.2Admin

Feature: Payment Method Fees
	Contains the test case in modifying the payment method fee (ERS v7.2)

Scenario: 7.2 Modify Payment Method Fees
#This test case open the Payment Method Fee in ERS as an admin
#It sets the sales outlet
#It sets which payment method fees are charged by putting a value of 1
#It sets the limit of the purchase price (more and less column)
#It modifies the payment method fee by inserting value in the price column(price)
#It will click the register button to save the changes
#Afterwards, it checks if the changes made are applied correctly

	Given Insert Template 7.2/etc_t Into etc_t
		And Insert Template 7.2/pay_t Into pay_t
	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/store/asp/store_etc.asp Wait[Loaded] 
		And Enter
			| Field                     | Value |
			| site_id@name              | 1     |
			| record_key_0_etc_flg@name |       |
			| record_key_1_etc_flg@name | 1     |
			| record_key_2_etc_flg@name | 1     |
			| record_key_3_etc_flg@name | 1     |
			| record_key_4_etc_flg@name |       |
			| record_key_5_etc_flg@name |       |

		And Enter List CREDIT_CARDList
			| more | down | price |
			| 1    | 5    | 100   |
			| 6    | 10   | 150   |
			| 10   | 15   | 200   |
			| 16   | 20   | 250   |
			| 21   | 25   | 300   |
			| 26   | 30   | 350   |
			| 31   | 35   | 400   |
			| 36   | 40   | 450   |
			| 41   | 45   | 500   |
			| 46   | 50   | 550   |

		And Enter List BANKList
			| more | down | price |
			| 11   | 120  | 1     |
			| 121  | 130  | 2     |
			| 131  | 140  | 3     |
			| 141  | 150  | 4     |
			| 151  | 160  | 5     |
			| 161  | 170  | 6     |
			| 171  | 180  | 7     |
			| 181  | 190  | 8     |
			| 191  | 199  | 9     |
			| 200  | 299  | 10     |

		And Enter List CASH_ON_DELIVERYList
			| more | down | price |
			| 21   | 210  | 1     |
			| 221  | 230  | 2     |
			| 231  | 240  | 3     |
			| 241  | 250  | 4     |
			| 251  | 260  | 5     |
			| 261  | 270  | 6     |
			| 271  | 280  | 7     |
			| 281  | 290  | 8     |
			| 291  | 2100 | 9     |

		And Enter List CONVENIENCE_STOREList
			| more | down | price |
			| 1    | 5    | 100   |
			| 6    | 10   | 150   |
			| 10   | 15   | 200   |
			| 16   | 20   | 250   |
			| 21   | 25   | 300   |
			| 26   | 30   | 350   |
			| 31   | 35   | 400   |
			| 36   | 40   | 450   |
			| 41   | 45   | 500   |

		And Enter List PAYPALList
			| more | down | price |
			| 1    | 5    | 1     |
			| 21   | 30   | 2     |
			| 31   | 40   | 3     |
			| 41   | 50   | 4     |
			| 51   | 60   | 5     |
			| 61   | 70   | 6     |
			| 71   | 80   | 7     |
			| 81   | 90   | 8     |
			| 91   | 100  | 9     |
			| 101  | 105  | 10    |
			
		And Enter List NON_NEEDED_PAYMENTList
			| more | down | price |
			| 100  | 199  | 1     |
			| 200  | 299  | 2     |
			| 300  | 399  | 3     |
			| 400  | 499  | 4     |
			| 500  | 599  | 5     |
			| 600  | 699  | 6     |
			| 700  | 799  | 7     |
			| 800  | 899  | 8     |
			| 900  | 999  | 9     |
			| 1000 | 1999 | 10    |

		And Click reg_btn Wait[Loaded] 
		And Click dialog_ok_button Wait[Loaded] 
		And Click back_btn Wait[Loaded] 

	Then Assert Elements
			| Field                     | Value |
			| site_id@name              | 1     |
			| record_key_0_etc_flg@name |       |
			| record_key_1_etc_flg@name | 1     |
			| record_key_2_etc_flg@name | 1     |
			| record_key_3_etc_flg@name | 1     |
			| record_key_4_etc_flg@name |       |
			| record_key_5_etc_flg@name |       |

		And Assert List CREDIT_CARDList
			| more | down | price |
			| 1    | 5    | 100   |
			| 6    | 10   | 150   |
			| 10   | 15   | 200   |
			| 16   | 20   | 250   |
			| 21   | 25   | 300   |
			| 26   | 30   | 350   |
			| 31   | 35   | 400   |
			| 36   | 40   | 450   |
			| 41   | 45   | 500   |
			| 46   | 50   | 550   |

		And Assert List BANKList
			| more | down | price |
			| 11   | 120  | 1     |
			| 121  | 130  | 2     |
			| 131  | 140  | 3     |
			| 141  | 150  | 4     |
			| 151  | 160  | 5     |
			| 161  | 170  | 6     |
			| 171  | 180  | 7     |
			| 181  | 190  | 8     |
			| 191  | 199  | 9     |
			| 200  | 299  | 10     |

		And Assert List CASH_ON_DELIVERYList
			| more | down | price |
			| 21   | 210  | 1     |
			| 221  | 230  | 2     |
			| 231  | 240  | 3     |
			| 241  | 250  | 4     |
			| 251  | 260  | 5     |
			| 261  | 270  | 6     |
			| 271  | 280  | 7     |
			| 281  | 290  | 8     |
			| 291  | 2100 | 9     |

		And Assert List CONVENIENCE_STOREList
			| more | down | price |
			| 1    | 5    | 100   |
			| 6    | 10   | 150   |
			| 10   | 15   | 200   |
			| 16   | 20   | 250   |
			| 21   | 25   | 300   |
			| 26   | 30   | 350   |
			| 31   | 35   | 400   |
			| 36   | 40   | 450   |
			| 41   | 45   | 500   |

		And Assert List PAYPALList
			| more | down | price |
			| 1    | 5    | 1     |
			| 21   | 30   | 2     |
			| 31   | 40   | 3     |
			| 41   | 50   | 4     |
			| 51   | 60   | 5     |
			| 61   | 70   | 6     |
			| 71   | 80   | 7     |
			| 81   | 90   | 8     |
			| 91   | 100  | 9     |
			| 101  | 105  | 10    |
			
		And Assert List NON_NEEDED_PAYMENTList
			| more | down | price |
			| 100  | 199  | 1     |
			| 200  | 299  | 2     |
			| 300  | 399  | 3     |
			| 400  | 499  | 4     |
			| 500  | 599  | 5     |
			| 600  | 699  | 6     |
			| 700  | 799  | 7     |
			| 800  | 899  | 8     |
			| 900  | 999  | 9     |
			| 1000 | 1999 | 10    |