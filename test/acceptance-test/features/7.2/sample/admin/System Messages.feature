﻿@V7.2Admin
Feature: System Messages
	Contains test case for modification of system messages that inform users of various information (ERS v7.2)

Scenario: System Messages
#This test opens the System Messages as an Admin
#It inserts the templates in the database
#It modifies the value of various information 
#Registers the changes by clicking the register button
#Goes to completion page then goes back
#Chooses the sales outlet if its (ERS[1], ERS site2[5])
#Checks the value of the ff. memo under that sales outlet

	Given Insert Template 7.2/setup_t Into setup_t
		And Insert Template 7.2/pay_t_bk Into pay_t_bk
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded]
		And Open /admin/top/store/asp/store_alert.asp Wait[Loaded]
		And Enter
		| Field                     | Value            |
		| site_id@name              | 5                |
		| pri_memo@name             | PRIVACY POLICY   |
		| error_message@name        | ERROR            |
		| m_error_message@name      | ERROR            |
		| regi_memo@name            | CART             |
		| wh_owner_description@name | SHIPPING ADDRESS |
		And Enter List memoList
		| memo              |
		| CREDIT CARD       |
		| BANK              |
		| COD               |
		| CONVENIENCE STORE |
		| PAYPAL            |
	
		And Click reg_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click back_btn Wait[Loaded]
		And Enter
		| Field                     | Value            |
		| site_id@name              | 5                |
	Then Assert Elements
		| Field                     | Value            |
		| pri_memo@name             | PRIVACY POLICY   |
		| error_message@name        | ERROR            |
		| m_error_message@name      | ERROR            |
		| regi_memo@name            | CART             |
		| wh_owner_description@name | SHIPPING ADDRESS |

		And Assert List memoList
		| memo              |
		| CREDIT CARD       |
		| BANK              |
		| COD               |
		| CONVENIENCE STORE |
		| PAYPAL            |

