﻿@V7.2Admin

Feature: CartMixControlSetting
	This feature contains test case for Registration, Modification and Deletion of Cart Mix Control Setting

#--------------------------------------------------------------------------------------
Scenario: Mixed Control Code Registration
#--------------------------------------------------------------------------------------
#This test case opens the Cart Mix Control Setting in ERSv7.2 as an admin
#It deletes the data with the same mixed_group_code of the sample we will register in the database table: mixed_group_t
#It registers the new mixed control code
#It checks the mixed control code we registered

	Given Delete From mixed_group_t
			| mixed_group_code |
			| hoge             |
			| test             |
		When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/item/asp/item_coexistence.asp Wait[Loaded]
		And Enter List erow
			| delete | mixed_group_code | mixed_group_name |
			|        | hoge             | hogehoge         |
			|        | test             | testtest         |

		And Click reg_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click btn01 Wait[Loaded]

	Then Assert List erow
			| delete | mixed_group_code | mixed_group_name |
			|        | hoge             | hogehoge         |
			|        | test             | testtest         |



#--------------------------------------------------------------------------------------
Scenario: Mixed Control Code Modification
#--------------------------------------------------------------------------------------
#This test case opens the Cart Mix Control Setting as an admin
#It deletes the data in the database with the same code as the sample data in the template
#It inserts the data we will modify from mixed_group_t template into database table: mixed_group_t
#It modifies the data in the Mixed Control List
#It saves the changes by clicking the reg_btn
#It checks if the changes made are correct

	Given Delete From mixed_group_t
			| mixed_group_code |
			| hoge             |
			| test             |
		And Insert Template 7.2/mixed_group_t Into mixed_group_t
		When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/item/asp/item_coexistence.asp Wait[Loaded] 
		And Enter List erow
			| delete | mixed_group_code | mixed_group_name |
			|        | hoge             | hogeModified     |
			|        | test             | testModified     |

		And Click reg_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click btn01 Wait[Loaded]

	Then Assert List erow
			| delete | mixed_group_code | mixed_group_name |
			|        | hoge             | hogeModified     |
			|        | test             | testModified     |

#--------------------------------------------------------------------------------------
Scenario: Mixed Control Code Deletion
#--------------------------------------------------------------------------------------
#This test case opens the Cart Mix Control Setting as an admin
#It deletes the data in the database with the same code as the sample saved in the template
#It inserts the data we will delete from mixed_group_t template into database table: mixed_group_t
#It deletes the data in the Mixed Control List by inserting a value of '1' under the delete column
#It saves the changes by clicking the reg_btn
#It checks if the deleted item is no longer in the system

	Given Delete From mixed_group_t
			| mixed_group_code|
			| hoge               |
			| test               |
		And Insert Template 7.2/mixed_group_t Into mixed_group_t
	When Open /admin Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/item/asp/item_coexistence.asp Wait[Loaded] 
		And Enter List erow
			| delete | mixed_group_code | mixed_group_name |
			| 1      | hoge             | hogehoge         |
			|        | test             | testtest         |

		And Click reg_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click btn01 Wait[Loaded]

	Then Assert Deleted List erow
			| mixed_group_code | 
			| hoge             | 
			   
	

