﻿@ers7.2Admin
Feature: AdminAccessSettings
	This feature contains test cases for registering/modifying/deleting Admin User Authority Setting


Scenario: Register Authority Setting
	# This scenario is for registering user's authority setting.
	# Login then open role.asp
	# Click New Registration
	# Enter regist_role_gname and regist_role_access
	# Click role_register and dialog_ok_button then click back
	# Click detail button then assert registered value
	Given Insert Template 7.2/role_group_t Into role_group_t
		And Delete From role_group_t
		| role_gname |
		| CTS        |
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/store/asp/role.asp Wait[Loaded]
			And Click register Wait[Loaded]
			And Enter
			| Field              | Value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
			| regist_role_gname  | CTS                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
			| regist_role_access | {bill_list,price_entry,price_csv,set_item,set_item_csv,store_tax,store_carriage,store_etc,store_delivery,store_calendar} |
			And Click role_register Wait[Loaded]
			And Click dialog_ok_button Wait[Loaded]
			And Click back_role Wait[Loaded]
			#change id upon testing depending on the id of the registered authority set
			And Click authoritylist[0].details Wait[Loaded]
		Then Assert Elements
			| Field              | Value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
			| modify_role_gname  | CTS                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
			| modify_role_access | {bill_list,price_entry,price_csv,set_item,set_item_csv,store_tax,store_carriage,store_etc,store_delivery,store_calendar} |


Scenario: Modify Registered Authority Setting
	# This scenario is for modifying user's authority setting.
	# Login then open role.asp
	# Click detail button
	# Enter regist_role_access to modify the role access of the user 
	# Click change button and dialog_ok_button then click back
	# Click detail button then assert registered value
	Given Insert Template 7.2/role_group_t Into role_group_t
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/store/asp/role.asp Wait[Loaded]
			And Click authoritylist[0].details Wait[Loaded]
			And Enter
			| Field              | Value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
			| modify_role_gname  | CTS                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
			| modify_role_access | {bill_list,summary_list,regist_income} |
			And Click change Wait[Loaded]
			And Click dialog_ok_button Wait[Loaded]
			And Click back Wait[Loaded]
			#change id upon testing depending on the id of the registered authority set
			And Click authoritylist[0].details Wait[Loaded]
		Then Assert Elements
			| Field              | Value                    |
			| modify_role_gname  | CTS                      |
			| modify_role_access | {bill_list,summary_list,regist_income} |



Scenario: Delete Authority Setting
	# This scenario is for deleting user's authority setting.
	# Login then open role.asp
	# Click detail button
	# Click delete button and dialog_ok_button then click back
	# Assert to check if the authority setting was deleted
	Given Insert Template 7.2/role_group_t Into role_group_t
		And Delete From role_group_t
		| role_gname |
		| テスト管理者A    |
		| テスト管理者     |
		| a          |
		| １          |
		| test       |
		When Open /admin Wait[Loaded]
			And Enter
	 		| user_login_id | passwd |
	 		| ivpers        | ivpers |
			And Click login_button Wait[Loaded]
			And Open /admin/top/store/asp/role.asp Wait[Loaded]
			And Click authoritylist[0].details Wait[Loaded]
			And Click delete Wait[Loaded]
			And Click dialog_ok_button Wait[Loaded]
			And Click back Wait[Loaded]
		Then Assert List authoritylist
			| role_name     |
			| pack and ship |
			| 管理者           |