﻿@V7.2Admin
Feature: CustomerRankSettings
	
Scenario: Customer Rank

	Given Insert Template 7.2/member_rank_setup_t Into member_rank_setup_t

	When Open /admin Wait[Loaded]
	And Enter
		| user_login_id | passwd |
		| ivpers        | ivpers |
	And Click login_button Wait[Loaded]
	And Open /admin/top/customer/asp/member_rank.asp Wait[Loaded]
	And Enter
		| Field                                 | Value    |
		| site_select@id                        | 1        |
		| member_rank_criterion@name            | 0        |
		| member_rank_term@name                 | 365      |
		| record_key_0_value_from@name          | 1        |
		| record_key_0_value_to@name            | 1500     |
		| record_key_0_point_magnification@name | 0.3      |
		| record_key_1_value_from@name          | 1501     |
		| record_key_1_value_to@name            | 10000    |
		| record_key_1_point_magnification@name | 1        |
		| record_key_2_value_from@name          | 10001    |
		| record_key_2_value_to@name            | 99999999 |
		| record_key_2_point_magnification@name | 2        |

	And Click register_btn Wait[None]
	And Click dialog_ok_button Wait[Loaded]
	And Click back_btn Wait[Loaded]

	Then Assert List memberRankList
		| l_rank_name | l_value_from | l_value_to | l_pnt_magnification |
		| シルバー１       | 1            | 1500       | 0.3                 |
		| ゴールド１       | 1501         | 10000      | 1                   |
		| プラチナ１       | 10001        | 99999999   | 2                   |
	When Enter
		| Field                                 | Value    |
		| site_select@id                        | 5        |
		| member_rank_criterion@name            | 1        |
		| member_rank_term@name                 | 365      |
		| record_key_0_value_from@name          | 1        |
		| record_key_0_value_to@name            | 100000   |
		| record_key_0_point_magnification@name | 1        |
		| record_key_1_value_from@name          | 100001   |
		| record_key_1_value_to@name            | 500000   |
		| record_key_1_point_magnification@name | 2        |
		| record_key_2_value_from@name          | 500001   |
		| record_key_2_value_to@name            | 99999999 |
		| record_key_2_point_magnification@name | 3        |

	And Click register_btn Wait[None]
	And Click dialog_ok_button Wait[Loaded]
	And Click back_btn Wait[Loaded]
	Then Assert List memberRankList
		| l_rank_name | l_value_from | l_value_to | l_pnt_magnification |
		| シルバー        | 1            | 100000     | 1                   |
		| ゴールド        | 100001       | 500000     | 2                   |
		| プラチナ        | 500001       | 99999999   | 3                   |