﻿@V7.2Admin
Feature: ProductTest
	It contains test cases for Product Registration, Search, Modification and Deletion.
#----------------------------------------------------------------------------------------------
Scenario: 7.2 Product Registration
#----------------------------------------------------------------------------------------------
#First, it inserts the ff. templates in the database
#This test case opens the Product Registration as an Admin
#It deletes the data in the db table where gcode= VITA000
#It fill in the information regarding the new product group
#It clicks the reg button to save the product group
#It checks if the information is the same/correct
#It adds 2 SKU under this product group
#It register the SKU under the group
#It checks if the information is the same/correct
#----------------------------------------------------------------------------------------------
	Given Insert Template 7.2/cate1_t Into cate1_t
		And Insert Template 7.2/cate2_t Into cate2_t
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t
		And Delete From g_master_t
			| gcode   |
			| VITA000 |
		And Delete From s_master_t
			| gcode   |
			| VITA000 |
		And Delete From price_t
			| gcode   |
			| VITA000 |
		And Delete From stock_t
			| scode      |
			| VITASCODE1 |
			| VITASCODE2 |
		And Delete From wh_stock_t
			| scode      |
			| VITASCODE1 |
			| VITASCODE2 |

	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/item/asp/item_regist.asp Wait[Loaded] 
		And Enter
		    | Field           | Value               |
		    | gcode@name      | VITA000             |
		    | gname@name      | VITAMILK            |
		    | m_gname@name    | VITAMILK            |
		    | date_from@name  | 2016/04/07 00:00:00 |
		    | date_to@name    | 2017/04/09 23:59:59 |
		    | s_sale_ptn@name | 1                   |
		    | stock_flg@name  | 1                   |
		    | stock_set2@name | 30                  |
		    | stock_set1@name | 10                  |
		    | cate1@name      | {1,2}               |
		    | cate2@name      | 2                   |
		    #| cate3@name         |                     |
		    #| cate4@name         |                     |
		    #| cate5@name         |                     |

	When Click reco_btn1 Wait[Loaded]
	    And Switch[Popup] /admin/top/item/asp/common_item_search.asp Wait[Loaded]
		And Enter
		    | Field        | Value    |
		    | s_gcode@name | CARIN101 |
		    | s_scode@name |          |
		    | s_sname@name |          |
		    #| s_cate1@name |         |
			#| s_cate2@name |         |
			#| s_cate3@name |         |
			#| s_cate4@name |         |
			#| s_cate5@name |         |
		And Click submit Wait[Loaded]
		And Click item_list[0].ok_btn Wait[None]
		And Close[Popup] Wait[URL] /admin/top/item/asp/item_regist.asp

	When Click reco_btn2 Wait[Loaded]
	    And Switch[Popup] /admin/top/item/asp/common_item_search.asp Wait[Loaded]
		And Enter
		    | Field        | Value   |
		    | s_gcode@name | HERSHEY |
		    | s_scode@name |         |
		    | s_sname@name |         |
		    #| s_cate1@name |         |
			#| s_cate2@name |         |
			#| s_cate3@name |         |
			#| s_cate4@name |         |
			#| s_cate5@name |         |
		And Click submit Wait[Loaded]
		And Click item_list[0].ok_btn Wait[None]
		And Close[Popup] Wait[URL] /admin/top/item/asp/item_regist.asp

		And Enter          
		    | Field                | Value                       |
		    | reco_prod3           |                             |
		    | reco_prod4           |                             |
		    | reco_prod5           |                             |
		    | disp_keyword         | VITA, Vitamins, Milk        |
		    | metatitle            | VitaMilk                    |
		    | metadescription      | vicinity graceful is it at. |
		    | metawords            | $Vitamins                   |
		    | link_url             | info/VitaMilk.html          |
		    | pc_description       | Description PC              |
		    | ph_description       | Description CP              |
		    | description_for_list | Sample Comment              |
		    | carriage_cost_type   | 0                           |
		    | plural_order_type    | 0                           |
		    | set_flg              | 0                           |
		    | doc_bundling_flg     | 0                           |
		    | deliv_method         | {1}                         |
		    | month_interval       | 0                           |
		    | week_interval        | 0                           |
		    | day_interval         | 0                           |
		    | site_id@name         | {1,5}                       |
		    | disp_list_flg@name   | 1                           |
		    | sort@name            | 0                           |
		    | active@name          | 1                           |


			And Click reg_btn Wait[Loaded]
			And Click dialog_ok_button Wait[None] 
			And Accept Alert Wait[Loaded]

	   Then Assert Elements
			| Field           | Value                |
			| gcode@name      | VITA000              |
			| gname@name      | VITAMILK             |
			| m_gname@name    | VITAMILK             |
			| date_from@name  | 4/7/2016 1:00:00 AM  |
			| date_to@name    | 4/10/2017 12:59:59 AM |
			| s_sale_ptn@name | 1                    |
			| stock_flg@name  | 1                    |
			| stock_set2@name | 30                   |
			| stock_set1@name | 10                   |
			| cate1@name      | {1,2}                |
			| cate2@name      | 2                    |
			#| cate3                |                             |
			#| cate4                |                             |
			#| cate5                |                             |
			| reco_prod3           |                             |
			| reco_prod4           |                             |
			| reco_prod5           |                             |
			| disp_keyword         | VITA, Vitamins, Milk        |
			| metatitle            | VitaMilk                    |
			| metadescription      | vicinity graceful is it at. |
			| metawords            | $Vitamins                   |
			| link_url             | info/VitaMilk.html          |
			| pc_description       | Description PC              |
			| ph_description       | Description CP              |
			| description_for_list | Sample Comment              |
			| carriage_cost_type   | 0                           |
			| plural_order_type    | 0                           |
			| set_flg              | 0                           |
			| doc_bundling_flg     | 0                           |
			| deliv_method         | {1}                         |
			| month_interval       | 0                           |
			| week_interval        | 0                           |
			| day_interval         | 0                           |
			| site_id@name         | {1,5}                       |
			| disp_list_flg@name   | 1                           |
			| sort@name            | 0                           |
			| active@name          | 1                           |


	   When Enter List sku_list
			| delete | scode      | jancode  | sname  | soldout_flg | price | regular_price | regular_first_price | price2 | cost_price | supplier_code | maker_scode | wh_order_type | point | attribute1 | attribute2 | disp_order | m_sname | cts_sname | shipping_sname | mixed_group_code | max_purchase_count | point_campaign_from | point_campaign_to   | campaign_point | stock | skuactive |
			| 0      | VITASCODE1 | JANCODE1 | SNAME1 | 1           | 100   | 80            | 70                  | 120    | 50         | VCODE1        | MYK         | 1             | 10    | 1ATT1      | 2ATT1      | 0          | MSNAME1 | CTSSNAME1 | INVSNAME1      | MXD1             | 10                 | 2016/04/07 00:00:00 | 2017/04/07 23:59:59 | 20             | 120   | 1         |
			| 0      | VITASCODE2 | JANCODE2 | SNAME2 | 0           | 110   | 90            | 80                  | 130    | 60         | VCODE1        | MYK         | 2             | 20    | 1ATT2      | 2ATT2      | 1          | MSNAME2 | CTSSNAME2 | INVSNAME2      | MXD2             | 20                 | 2016/04/07 00:00:00 | 2017/04/07 23:59:59 | 40             | 150   | 0         |
			And Click reg_btn Wait[Loaded]
			And Click dialog_ok_button Wait[Loaded]
			And Click back_btn Wait[Loaded]

		Then Assert List sku_list
			| delete | scode      | jancode  | sname  | soldout_flg | price | regular_price | regular_first_price | price2 | cost_price | supplier_code | maker_scode | wh_order_type | point | attribute1 | attribute2 | disp_order | m_sname | cts_sname | shipping_sname | mixed_group_code | max_purchase_count | point_campaign_from | point_campaign_to    | campaign_point | stock | skuactive |
			| 0      | VITASCODE1 | JANCODE1 | SNAME1 | 1           | 100   | 80            | 70                  | 120    | 50         | VCODE1        | MYK         | 1             | 10    | 1ATT1      | 2ATT1      | 0          | MSNAME1 | CTSSNAME1 | INVSNAME1      | MXD1             | 10                 | 4/7/2016 1:00:00 AM | 4/8/2017 12:59:59 AM | 20             | 120   | 1         |
			| 0      | VITASCODE2 | JANCODE2 | SNAME2 | 0           | 110   | 90            | 80                  | 130    | 60         | VCODE1        | MYK         | 2             | 20    | 1ATT2      | 2ATT2      | 1          | MSNAME2 | CTSSNAME2 | INVSNAME2      | MXD2             | 20                 | 4/7/2016 1:00:00 AM | 4/8/2017 12:59:59 AM | 40             | 150   | 0         |

#----------------------------------------------------------------------------------------------			
Scenario:  7.2 Product Search and Checked
#----------------------------------------------------------------------------------------------
#First, it inserts the ff. templates in the database
#This test case opens the Product Search as an Admin
#It searches for the product to be checked by filling in the search criterias
#It checks if that product is stated in the list then clicks the details button
#It checks the data of the product group and the SKU products under it
#----------------------------------------------------------------------------------------------
	Given Insert Template 7.2/cate1_t Into cate1_t
		And Insert Template 7.2/cate2_t Into cate2_t
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t
	
	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/item/asp/item_search.asp Wait[Loaded] 
		And Enter
		    | Field          | Value   |
		    | s_gcode@name   | HERSHEY |
		    | s_scode@name   |         |
		    | s_jancode@name |         |
		    | s_sname@name   |         |
		    #| s_cate1@name   |         |
		    #| s_cate2@name   |         |
		    #| s_cate3@name   |         |
		    #| s_cate4@name   |         |
		    #| s_cate5@name   |         |
		    | s_site_id@name | {1,5}   |
	   And Click submit Wait[Loaded] 
	Then Assert List item_list
	   | gcode   | gname       | date_from             | w_sale_ptn | intime               |
	   | HERSHEY | Hershey Bar | 1/29/2016 12:00:00 AM | 通常       | 1/29/2016 4:45:11 PM |
	When Click item_list[0].ok_btn Wait[Loaded] 
	Then Assert Elements
         | Field                | Value                                                                                                                                                                                                                                                                                                                                                       |
         | gcode                | HERSHEY                                                                                                                                                                                                                                                                                                                                                     |
         | gname                | Hershey Bar                                                                                                                                                                                                                                                                                                                                                 |
         | m_gname              | Hershey Bar                                                                                                                                                                                                                                                                                                                                                 |
         | date_from            | 1/29/2016 12:00:00 AM                                                                                                                                                                                                                                                                                                                                       |
         | date_to              | 1/29/2026 4:27:27 PM                                                                                                                                                                                                                                                                                                                                        |
         | s_sale_ptn           | 1                                                                                                                                                                                                                                                                                                                                                           |
         | stock_flg@name       | 2                                                                                                                                                                                                                                                                                                                                                           |
         | stock_set2@name      |                                                                                                                                                                                                                                                                                                                                                             |
         | stock_set1@name      |                                                                                                                                                                                                                                                                                                                                                             |
         | cate1@name           |                                                                                                                                                                                                                                                                                                                                                             |
         | cate2@name           |                                                                                                                                                                                                                                                                                                                                                             |
         #| cate3@name           |                                                                                                                                                                                                                                                                                                                                                             |
         #| cate4@name           |                                                                                                                                                                                                                                                                                                                                                             |
         #| cate5@name           |                                                                                                                                                                                                                                                                                                                                                             |
         | reco_prod1           |                                                                                                                                                                                                                                                                                                                                                             |
         | reco_prod2           |                                                                                                                                                                                                                                                                                                                                                             |
         | reco_prod3           |                                                                                                                                                                                                                                                                                                                                                             |
         | reco_prod4           |                                                                                                                                                                                                                                                                                                                                                             |
         | reco_prod5           |                                                                                                                                                                                                                                                                                                                                                             |
         | disp_keyword         | Hershey Bar                                                                                                                                                                                                                                                                                                                                                 |
         | metatitle            | Hershey Bar                                                                                                                                                                                                                                                                                                                                                 |
         | metadescription      | Hershey Bar                                                                                                                                                                                                                                                                                                                                                 |
         | metawords            | $Hershey$Bar                                                                                                                                                                                                                                                                                                                                                |
         | link_url             |                                                                                                                                                                                                                                                                                                                                                             |
         | pc_description       | The Hersheys Milk Chocolate Bar (commonly called the Hersheys Bar) is the flagship chocolate bar manufactured by the Hershey Company. It is often referred by Hershey as The Great American Chocolate Bar. The Hershey Milk Chocolate Bar was first sold in 1900 followed by the Hersheys Milk Chocolate with Almonds variety beginning production in 1908. |
         | ph_description       | The Hersheys Milk Chocolate Bar (commonly called the Hersheys Bar) is the flagship chocolate bar manufactured by the Hershey Company. It is often referred by Hershey as The Great American Chocolate Bar. The Hershey Milk Chocolate Bar was first sold in 1900 followed by the Hersheys Milk Chocolate with Almonds variety beginning production in 1908. |
         | description_for_list | The Hersheys Milk Chocolate Bar (commonly called the Hersheys Bar) is the flagship chocolate bar manufactured by the Hershey Company. It is often referred by Hershey as The Great American Chocolate Bar. The Hershey Milk Chocolate Bar was first sold in 1900 followed by the Hersheys Milk Chocolate with Almonds variety beginning production in 1908. |
         | carriage_cost_type   | 0                                                                                                                                                                                                                                                                                                                                                           |
         | plural_order_type    | 0                                                                                                                                                                                                                                                                                                                                                           |
         | set_flg              | 0                                                                                                                                                                                                                                                                                                                                                           |
         | doc_bundling_flg     | 0                                                                                                                                                                                                                                                                                                                                                           |
         | deliv_method         | {1}                                                                                                                                                                                                                                                                                                                                                         |
         | month_interval       | 0                                                                                                                                                                                                                                                                                                                                                           |
         | week_interval        | 0                                                                                                                                                                                                                                                                                                                                                           |
         | day_interval         | 0                                                                                                                                                                                                                                                                                                                                                           |
         | site_id@name         | {1,5}                                                                                                                                                                                                                                                                                                                                                       |
         | disp_list_flg@name   | 1                                                                                                                                                                                                                                                                                                                                                           |
         | sort@name            | 0                                                                                                                                                                                                                                                                                                                                                           |
         | active@name          | 1                                                                                                                                                                                                                                                                                                                                                           |

	And Assert List sku_list
		| delete | scode | jancode | sname                               | soldout_flg | price | regular_price | regular_first_price | price2 | cost_price | supplier_code | maker_scode | wh_order_type | point | attribute1 | attribute2 | disp_order | m_sname                             | cts_sname                           | shipping_sname | mixed_group_code | max_purchase_count | point_campaign_from | point_campaign_to | campaign_point | stock | skuactive |
		|        | HDC   |         | Hershey Dark Chocolate              | 1           | 100   | 110           | 100                 | 12040  | 80         | SC01          | HDC         | 1             | 10    | Dark       |            | 2          | Hershey Dark Chocolate              | Hershey Dark Chocolate              | HDC            |                  | 10                 |                     |                   |                | 500   | 1         |
		|        | HMC   |         | Hershey Milk Chocolate Bar          | 1           | 100   | 110           | 100                 | 12040  | 80         | SC01          | HMC         | 1             | 10    | Milk       |            | 0          | Hershey Milk Chocolate Bar          | Hershey Milk Chocolate Bar          | HMC            |                  | 10                 |                     |                   |                | 490   | 1         |
		|        | HMCA  |         | Hershey Milk Chocolate with Almonds | 1           | 100   | 110           | 100                 | 12040  | 80         | SC01          | HMCA        | 1             | 10    | Milk       | Almonds    | 1          | Hershey Milk Chocolate with Almonds | Hershey Milk Chocolate with Almonds | HMCA           |                  | 10                 |                     |                   |                | 499   | 1         |

#----------------------------------------------------------------------------------------------
Scenario: 7.2 Product Modification
#----------------------------------------------------------------------------------------------
#First, it inserts the ff. templates in the database
#This test case opens the Product Search as an Admin
#It searches for the product to be modified by filling in the search criterias
#It checks if that product is stated in the list then clicks the details button
#It inserts new data in the product group
#It clicks the update button to save the changes made
#It inserts new data in the SKU
#It clicks the register button to save the changes made
#It checks if the product is already deleted
#----------------------------------------------------------------------------------------------
	Given Insert Template 7.2/cate1_t Into cate1_t
		And Insert Template 7.2/cate2_t Into cate2_t
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t
	
	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/item/asp/item_search.asp Wait[Loaded] 
		And Enter
		    | Field          | Value   |
		    | s_gcode@name   | VITA000 |
		    | s_scode@name   |         |
		    | s_jancode@name |         |
		    | s_sname@name   |         |
		    #| s_cate1@name   |         |
		    #| s_cate2@name   |         |
		    #| s_cate3@name   |         |
		    #| s_cate4@name   |         |
		    #| s_cate5@name   |         |
		    | s_site_id@name | {1,5}   |
	   And Click submit Wait[Loaded] 

	Then Assert List item_list
	   | gcode   | gname    | date_from           | w_sale_ptn | intime                |
	   | VITA000 | VITAMILK | 4/7/2016 1:00:00 AM | 通常       | 4/7/2016 4:41:39 PM |

	When Click item_list[0].ok_btn Wait[Loaded] 
		And Enter
		    | Field           | Value               |
		    | gcode@name      | VITA000             |
		    | gname@name      | VITAMILK            |
		    | m_gname@name    | VITAGATAS           |
		    | date_from@name  | 2016/04/07 00:00:00 |
		    | date_to@name    | 2017/04/09 23:59:59 |
		    | s_sale_ptn@name | 1                   |
		    | stock_flg@name  | 1                   |
		    | stock_set2@name | 30                  |
		    | stock_set1@name | 10                  |
		    | cate1@name      | {1}                 |
		    | cate2@name      | {1,2}               |
		    #| cate3@name         |                     |
		    #| cate4@name         |                     |
		    #| cate5@name         |                     |

	When Click reco_btn1 Wait[Loaded]
	    And Switch[Popup] /admin/top/item/asp/common_item_search.asp Wait[Loaded]
		And Enter
		    | Field        | Value    |
		    | s_gcode@name | HERSHEY  |
		    | s_scode@name |          |
		    | s_sname@name |          |
		    #| s_cate1@name |         |
			#| s_cate2@name |         |
			#| s_cate3@name |         |
			#| s_cate4@name |         |
			#| s_cate5@name |         |
		And Click submit Wait[Loaded]
		And Click item_list[0].ok_btn Wait[None]
		And Close[Popup] Wait[Loaded]

	When Click reco_btn2 Wait[Loaded]
	    And Switch[Popup] /admin/top/item/asp/common_item_search.asp Wait[Loaded]
		And Enter
		    | Field        | Value   |
		    | s_gcode@name | CARIN101 |
		    | s_scode@name |         |
		    | s_sname@name |         |
		    #| s_cate1@name |         |
			#| s_cate2@name |         |
			#| s_cate3@name |         |
			#| s_cate4@name |         |
			#| s_cate5@name |         |
		And Click submit Wait[Loaded]
		And Click item_list[0].ok_btn Wait[None]
		And Close[Popup] Wait[Loaded]

		And Enter          
		    | Field                | Value                       |
		    | reco_prod3           |                             |
		    | reco_prod4           |                             |
		    | reco_prod5           |                             |
		    | disp_keyword         | VITA, Vitamins, Gatas       |
		    | metatitle            | VITAGATAS                   |
		    | metadescription      | vicinity graceful is it at. |
		    | metawords            | $Vitamins                   |
		    | link_url             | info/VitaMilk.html          |
		    | pc_description       | Description PC              |
		    | ph_description       | Description CP              |
		    | description_for_list | Sample Comment              |
		    | carriage_cost_type   | 0                           |
		    | plural_order_type    | 0                           |
		    | set_flg              | 0                           |
		    | doc_bundling_flg     | 0                           |
		    | deliv_method         | {1}                         |
		    | month_interval       | 0                           |
		    | week_interval        | 0                           |
		    | day_interval         | 0                           |
		    | site_id@name         | {1,5}                       |
		    | disp_list_flg@name   | 1                           |
		    | sort@name            | 0                           |
		    | active@name          | 1                           |

		And Click item_group_modify_btn@name Wait[Loaded]

	When Click dialog_ok_button Wait[Loaded]
		And Click back_btn Wait[Loaded]

	Then Assert Elements
			| Field           | Value                |
			| gcode@name      | VITA000             |
		    | gname@name      | VITAMILK           |
		    | m_gname@name    | VITAGATAS           |
		    | date_from@name  | 4/7/2016 1:00:00 AM |
		    | date_to@name    | 4/10/2017 12:59:59 AM |
		    | s_sale_ptn@name | 1                   |
		    | stock_flg@name  | 1                   |
		    | stock_set2@name | 30                  |
		    | stock_set1@name | 10                  |
		    | cate1@name      | {1}                 |
		    | cate2@name      | {1,2}               |
			#| cate3                |                             |
			#| cate4                |                             |
			#| cate5                |                             |
			| reco_prod3           |                             |
		    | reco_prod4           |                             |
		    | reco_prod5           |                             |
		    | disp_keyword         | VITA, Vitamins, Gatas       |
		    | metatitle            | VITAGATAS                   |
		    | metadescription      | vicinity graceful is it at. |
		    | metawords            | $Vitamins                   |
		    | link_url             | info/VitaMilk.html          |
		    | pc_description       | Description PC              |
		    | ph_description       | Description CP              |
		    | description_for_list | Sample Comment              |
		    | carriage_cost_type   | 0                           |
		    | plural_order_type    | 0                           |
		    | set_flg              | 0                           |
		    | doc_bundling_flg     | 0                           |
		    | deliv_method         | {1}                         |
		    | month_interval       | 0                           |
		    | week_interval        | 0                           |
		    | day_interval         | 0                           |
		    | site_id@name         | {1,5}                       |
		    | disp_list_flg@name   | 1                           |
		    | sort@name            | 0                           |
		    | active@name          | 1                           |

	When Enter List sku_list
			| delete | scode      | jancode | sname  | soldout_flg | price | regular_price | regular_first_price | price2 | cost_price | supplier_code | maker_scode | wh_order_type | point | attribute1 | attribute2 | disp_order | m_sname  | cts_sname | shipping_sname | mixed_group_code | max_purchase_count | point_campaign_from | point_campaign_to   | campaign_point | stock | skuactive |
			| 0      | VITASCODE1 |         | SNAME1 | 1           | 200   | 180           | 170                 | 220    | 150        | MYK           | VCODE1      | 1             | 10    | 1ATT18     | 2ATT18     | 0          | CPSNAME1 | CTSSNAME1 | INVSNAME1      | MXD1             | 10                 | 2016/04/07 00:00:00 | 2017/04/07 23:59:59 | 40             | 220   |          |
			| 0      | VITASCODE2 |         | SNAME2 | 0           | 210   | 190           | 180                 | 230    | 160        | MYK           | VCODE1      | 2             | 20    | 1ATT28     | 2ATT28     | 1          | CPSNAME2 | CTSSNAME2 | INVSNAME2      | MXD2             | 20                 | 2016/04/07 00:00:00 | 2017/04/07 23:59:59 | 20             | 250   | 1         |
			And Click reg_btn Wait[Loaded]
			And Click dialog_ok_button Wait[Loaded]
			And Click back_btn Wait[Loaded]

		Then Assert List sku_list
			| delete | scode      | jancode | sname  | soldout_flg | price | regular_price | regular_first_price | price2 | cost_price | supplier_code | maker_scode | wh_order_type | point | attribute1 | attribute2 | disp_order | m_sname  | cts_sname | shipping_sname | mixed_group_code | max_purchase_count | point_campaign_from | point_campaign_to   | campaign_point | stock | skuactive |
			| 0      | VITASCODE1 |         | SNAME1 | 1           | 200   | 180           | 170                 | 220    | 150        | MYK           | VCODE1      | 1             | 10    | 1ATT18     | 2ATT18     | 0          | CPSNAME1 | CTSSNAME1 | INVSNAME1      | MXD1             | 10                 | 4/7/2016 1:00:00 AM | 4/8/2017 12:59:59 AM | 40             | 220   |         |
			| 0      | VITASCODE2 |         | SNAME2 | 0           | 210   | 190           | 180                 | 230    | 160        | MYK           | VCODE1      | 2             | 20    | 1ATT28     | 2ATT28     | 1          | CPSNAME2 | CTSSNAME2 | INVSNAME2      | MXD2             | 20                 | 4/7/2016 1:00:00 AM | 4/8/2017 12:59:59 AM | 20             | 250   | 1         |

#----------------------------------------------------------------------------------------------
Scenario: 7.2 Delete SKU
#----------------------------------------------------------------------------------------------
#First, it inserts the ff. templates in the database
#This test case opens the Product Search as an Admin
#It searches for the product to be deleted by filling in the search criterias
#It checks if that product is stated in the list then clicks the details button
#It inserts 1 under the delete column of the SKU product you want to delete
#It checks if the SKU is already deleted
#----------------------------------------------------------------------------------------------
	Given Insert Template 7.2/cate1_t Into cate1_t
		And Insert Template 7.2/cate2_t Into cate2_t
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t
	
	When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/item/asp/item_search.asp Wait[Loaded] 
		And Enter
		    | Field          | Value   |
		    | s_gcode@name   | HERSHEY |
		    | s_scode@name   |         |
		    | s_jancode@name |         |
		    | s_sname@name   |         |
		    #| s_cate1@name   |         |
		    #| s_cate2@name   |         |
		    #| s_cate3@name   |         |
		    #| s_cate4@name   |         |
		    #| s_cate5@name   |         |
		    | s_site_id@name | {1,5}   |
	   And Click submit Wait[Loaded] 
	Then Assert List item_list
	   | gcode   | gname       | date_from             | w_sale_ptn | intime               |
	   | HERSHEY | Hershey Bar | 1/29/2016 12:00:00 AM | 通常       | 1/29/2016 4:45:11 PM |

	When Click item_list[0].ok_btn Wait[Loaded] 
		And Enter List sku_list
		| delete | scode | jancode | sname                               | soldout_flg | price | regular_price | regular_first_price | price2 | cost_price | supplier_code | maker_scode | wh_order_type | point | attribute1 | attribute2 | disp_order | m_sname                             | cts_sname                           | shipping_sname | mixed_group_code | max_purchase_count | point_campaign_from | point_campaign_to | campaign_point | stock | skuactive |
		|   1     | HDC   |         | Hershey Dark Chocolate              | 1           | 100   | 110           | 100                 | 12040  | 80         | SC01          | HDC         | 1             | 10    | Dark       |            | 2          | Hershey Dark Chocolate              | Hershey Dark Chocolate              | HDC            |                  | 10                 |                     |                   |                | 500   | 1         |
		|   0     | HMC   |         | Hershey Milk Chocolate Bar          | 1           | 100   | 110           | 100                 | 12040  | 80         | SC01          | HMC         | 1             | 10    | Milk       |            | 0          | Hershey Milk Chocolate Bar          | Hershey Milk Chocolate Bar          | HMC            |                  | 10                 |                     |                   |                | 490   | 1         |
		|   1     | HMCA  |         | Hershey Milk Chocolate with Almonds | 1           | 100   | 110           | 100                 | 12040  | 80         | SC01          | HMCA        | 1             | 10    | Milk       | Almonds    | 1          | Hershey Milk Chocolate with Almonds | Hershey Milk Chocolate with Almonds | HMCA           |                  | 10                 |                     |                   |                | 499   | 1         |
		And Click reg_btn Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click back_btn Wait[Loaded]

	Then Assert List sku_list
	   | delete | scode | jancode | sname                      | soldout_flg | price | regular_price | regular_first_price | price2 | cost_price | supplier_code | maker_scode | wh_order_type | point | attribute1 | attribute2 | disp_order | m_sname                    | cts_sname                  | shipping_sname | mixed_group_code | max_purchase_count | point_campaign_from | point_campaign_to | campaign_point | stock | skuactive |
	   |        | HMC   |         | Hershey Milk Chocolate Bar | 1           | 100   | 110           | 100                 | 12040  | 80         | SC01          | HMC         | 1             | 10    | Milk       |            | 0          | Hershey Milk Chocolate Bar | Hershey Milk Chocolate Bar | HMC            |                  | 10                 |                     |                   |                | 490   | 1         |
	
#----------------------------------------------------------------------------------------------
Scenario: 7.2 Delete Product Group
#----------------------------------------------------------------------------------------------
#First, it inserts the ff. templates in the database
#This test case opens the Product Search as an Admin
#It searches for the product to be deleted by filling in the search criterias
#It checks if that product is stated in the list then clicks the details button
#It clicks the delete button of the product
#It checks if the product is already deleted
#----------------------------------------------------------------------------------------------
	Given Insert Template 7.2/cate1_t Into cate1_t
		And Insert Template 7.2/cate2_t Into cate2_t
		And Insert Template 7.2/g_master_t Into g_master_t
		And Insert Template 7.2/s_master_t Into s_master_t
		And Insert Template 7.2/stock_t Into stock_t
		And Insert Template 7.2/price_t Into price_t
		And Insert Template 7.2/wh_stock_t Into wh_stock_t

		When Open /admin/top/login/asp/login.asp Wait[Loaded] 
		And Enter
			| user_login_id | passwd |
			| ivpers        | ivpers |
		And Click login_button Wait[Loaded] 
		And Open /admin/top/item/asp/item_search.asp Wait[Loaded] 
		And Enter
		    | Field          | Value   |
		    | s_gcode@name   | HERSHEY |
		    | s_scode@name   |         |
		    | s_jancode@name |         |
		    | s_sname@name   |         |
		    #| s_cate1@name   |         |
		    #| s_cate2@name   |         |
		    #| s_cate3@name   |         |
		    #| s_cate4@name   |         |
		    #| s_cate5@name   |         |
		    | s_site_id@name | {1,5}   |
	   And Click submit Wait[Loaded] 
	Then Assert List item_list
	   | gcode   | gname       | date_from             | w_sale_ptn | intime               |
	   | HERSHEY | Hershey Bar | 1/29/2016 12:00:00 AM | 通常       | 1/29/2016 4:45:11 PM |

	When Click item_list[0].ok_btn Wait[Loaded]

		And Click item_group_delete_btn@name Wait[Loaded]
		And Click dialog_ok_button Wait[Loaded]
		And Click back_btn Wait[Loaded]

	
	
