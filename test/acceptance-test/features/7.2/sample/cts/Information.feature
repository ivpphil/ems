﻿@ers7.2CTS
Feature: CTSInformation
	This feature contains test cases for registering modifying and deleteing CTS Information.


Scenario: Register Information
	# This scenario is for registering CTS Information.
	# Login then open information management
	# Enter values in the ff fields title, body, ang then click register
	# Then assert values to check if registered
	Given Insert Template 7.2/cts_information_t Into cts_information_t
		And Delete From cts_information_t
		| title |
		| Title |  
		When Open /cts Wait[Loaded]
			And Enter
	 		| user_id@name | passwd@name |
	 		| ivpers        | ivpers |
			And Click login_btn@name Wait[Loaded]
			And Open /cts/information Wait[Loaded]
			And Enter
			| Field        | Value    |
			| title@name | Title      |
			| contents@name | This is the body of the title.   |
			And Click regist@name Wait[None]
			And Accept Alert Wait[Loaded]
		Then Assert List information_list
		 | title    | content                           | ag_name |
		 | Title    | This is the body of the title.    | IVP管理者  |
		 | new      | this is new.                       | IVP管理者  |
		 | My Title | This is the body of the My Title. | IVP管理者  |
		 

Scenario: Modify Information
	# This scenario is for modifying CTS Information.
	# Login then open information management
	# Change the values in the ff fields title, body, ang then click confirm
	# Then assert values to check if modified
	Given Delete From cts_information_t
		| title |
		| Title |
		And Insert Template 7.2/cts_information_t Into cts_information_t
		When Open /cts Wait[Loaded]
			And Enter
	 		| user_id@name | passwd@name |
	 		| ivpers        | ivpers |
			And Click login_btn@name Wait[Loaded]
			And Open /cts/information Wait[Loaded]
			And Click information_list[1].modify Wait[Loaded]
			And Enter
			| Field        | Value    |
			| title@name | EDITED Title      |
			| contents@name | This is the EDITED body of the title.   |
			And Click confirm Wait[None]
			And Accept Alert Wait[Loaded]
		Then Assert List information_list
		 | title        | content                               | ag_name |
		 | new          | this is new.                           | IVP管理者  |
		 | EDITED Title | This is the EDITED body of the title. | IVP管理者  |
		

Scenario: Delete Information
	# This scenario is for deleting CTS Information.
	# Login then open information management
	# Click delete button
	# Then assert values to check if deleted
	Given Delete From cts_information_t
		| title |
		| Title |
		And Insert Template 7.2/cts_information_t Into cts_information_t
		When Open /cts Wait[Loaded]
			And Enter
	 		| user_id@name | passwd@name |
	 		| ivpers        | ivpers |
			And Click login_btn@name Wait[Loaded]
			And Open /cts/information Wait[Loaded]
			And Click information_list[0].delete Wait[None]
			And Accept Alert Wait[Loaded]
		Then Assert List information_list
		 | title    | content                           | ag_name |
		 | My Title | This is the body of the My Title. | IVP管理者  |

Scenario: Information List
	# This scenario is for clicking CTS Information list.
	# Login then open information management
	# Click list button
	# Popup will open
	# Then close popup
	Given Insert Template 7.2/cts_information_t Into cts_information_t
		When Open /cts Wait[Loaded]
			And Enter
	 		| user_id@name | passwd@name |
	 		| ivpers        | ivpers |
			And Click login_btn@name Wait[Loaded]
			And Open /cts/information Wait[Loaded]
			And Click information_list[0].unreadlist Wait[Loaded]
			And Switch[Popup] /cts/top/information/asp/unreadlist.asp Wait[Loaded]
			And Click close Wait[None]