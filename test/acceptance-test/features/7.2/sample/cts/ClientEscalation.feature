﻿@V7.2CTS

Feature: ClientEscalation
	This feature contains the Registration, Modification and Deletion of Client Escalation

#-------------------------------------------------------------------------------------------------------
Scenario: Client Escalation Registration
#-------------------------------------------------------------------------------------------------------
#This scenerio inserts the cts_client_escalation_t template into the database
#It opens the Client Escalation Management  as an Admin
#It registers a new supervisor then clicks the register button
#Check if the registered agent is already in the list of supervisor

Given Insert Template 7.2/cts_client_escalation_t Into cts_client_escalation_t
	And Delete From cts_client_escalation_t
		| esc_name |
		| esca    |
	When Open /cts Wait[Loaded]
		And Enter
			| user_id@name | passwd@name |
			| ivpers       | ivpers      |
		And Click login_btn@name Wait[Loaded]
		And Open /cts/top/configuration/asp/ConfigEscalationDetail.asp Wait[Loaded] 
		And Click regist@name Wait[Loaded]
		And Enter
			| Field                | Value                   |
			| esc_name@name        | esca                    |
			| email_from_name@name | nekoneko                |
			| email_from@name      | nekosan@mailinator.com  |
			| email_to@name        | inusan@mailinator.com   |
			| email_cc@name        | mushisan@mailinator.com |
			| email_bcc@name       | butasan@mailinator.com  |
			| email_header@name    | header1                 |
			| email_body@name      | body2                   |
			| email_fotter@name    | footer                  |
			| active_string@name   | 0                       |
		And Click save@name Wait[None]
		And Accept Alert Wait[Loaded]
		Then Assert List escalation_list
			| esc_name@name | email_from_name@name | email_from@name        | email_to@name         | email_cc@name           | email_bcc@name         | email_header@name | email_fotter@name | active@name |
			| esca          | nekoneko             | nekosan@mailinator.com | inusan@mailinator.com | mushisan@mailinator.com | butasan@mailinator.com | header1           | footer            | No          |
			| problem       | midori               | midori@mailinator.com  | aoi@mailinator.com    | akai@mailinator.com     | shiroi@mailinator.com  | header            | footer            | Yes         |



#-------------------------------------------------------------------------------------------------------
Scenario: Client Escalation Modification
#-------------------------------------------------------------------------------------------------------
#This scenerio inserts the cts_client_escalation_t template into the database
#It opens the Client Escalation Management as an Admin
#It modifies the data of escalation then clicks modify button
#Checks if the changes made is already reflected in the system

Given Delete From cts_client_escalation_t
		| esc_name |
		| esca    |
	And Insert Template 7.2/cts_client_escalation_t Into cts_client_escalation_t
	When Open /cts Wait[Loaded]
		And Enter
			| user_id@name | passwd@name |
			| ivpers       | ivpers      |
		And Click login_btn@name Wait[Loaded]
		And Open /cts/top/configuration/asp/ConfigEscalationDetail.asp Wait[Loaded] 
		And Click escalation_list[0].modify Wait[Loaded]
		And Enter
			| Field                | Value                      |
			| esc_name@name        | esca                       |
			| email_from_name@name | kerokero                   |
			| email_from@name      | nekosan123@mailinator.com  |
			| email_to@name        | inusan123@mailinator.com   |
			| email_cc@name        | mushisan123@mailinator.com |
			| email_bcc@name       | butasan123@mailinator.com  |
			| email_header@name    | headerEdited               |
			| email_body@name      | bodyEdited                 |
			| email_fotter@name    | footerEdited               |
			| active_string@name   | 1                          |
		And Click save@name Wait[None]
		And Accept Alert Wait[Loaded]
		Then Assert List escalation_list
			| esc_name@name | email_from_name@name | email_from@name           | email_to@name            | email_cc@name              | email_bcc@name            | email_header@name | email_fotter@name | active@name |
			| esca          | kerokero             | nekosan123@mailinator.com | inusan123@mailinator.com | mushisan123@mailinator.com | butasan123@mailinator.com | headerEdited      | footerEdited      | Yes         |
			| problem       | midori               | midori@mailinator.com     | aoi@mailinator.com       | akai@mailinator.com        | shiroi@mailinator.com     | header            | footer            | Yes         |
	

#------------------------------------------------------------------------------------------------
Scenario: Client Escalation Deletion
#-------------------------------------------------------------------------------------------------------
#This scenerio inserts the cts_client_escalation_t template into the database
#It opens the Client Escalation  as an Admin
#It deletes one of the escalation
#Checks if the deleted item is no longer in the system

Given Delete From cts_client_escalation_t
		| esc_name |
		| esca     |
	And Insert Template 7.2/cts_client_escalation_t Into cts_client_escalation_t
	When Open /cts Wait[Loaded]
		And Enter
			| user_id@name | passwd@name |
			| ivpers       | ivpers      |
		And Click login_btn@name Wait[Loaded]
		And Open /cts/top/configuration/asp/ConfigEscalationDetail.asp Wait[Loaded] 
		And Click escalation_list[0].delete Wait[None]
		And Accept Alert Wait[Loaded]
	Then Assert Deleted List escalation_list
		| esc_name@name |
		| esca       | 
					