﻿@V7.2CTS
Feature: CtsCategoryManagement
			Update Category List Name Display Setting
			1.1 Register New Category List
			1.2 Modify Category List
			1.3 Delete Category List
			2.1 Register New Category List
			2.2 Modify Category List
			2.3 Delete Category List
			3.1 Register New Category List
			3.2 Modify Category List
			3.3 Delete Category List
			4.1 Register New Category List
			4.2 Modify Category List
			4.3 Delete Category List
			5.1 Register New Category List
			5.2 Modify Category List
			5.3 Delete Category List

#------------------------------------------------------
Scenario: Update Category List Name Display Setting
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
         | type_code  |
         | ENQCT_NAME |
	And Insert Template 7.2/cts_enquiry_category_main_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	Then Assert List categoryList
		| l_namename        | l_display |
		| Contact Source    | 表示        |
		| Contact Direction | 表示        |
		| Contact Reason    | 非表示       |
		| Resolution        | 非表示       |
		| BLANK Not Used    | 非表示       |
	# Change the display setting of Contact Reason and Resolution. Make it display.
	When Click categoryList[2].edit_btn Wait[Loaded]
	And Enter
		| active@name |
		| 1           |
	And Click register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[3].edit_btn Wait[Loaded]
	And Enter
		| active@name |
		| 1           |
	And Click register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]

#------------------------------------------------------
Scenario: 1.1 Register New Category List
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
		| namename |
		| SAMPLE   |
	And Insert Template 7.2/cts_enquiry_category_1_t Into cts_enquiry_category_t
	And Delete From cts_enquiry_category_t
		| namename |
		| SAMPLE   |
	 
	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[0].edit_btn Wait[Loaded]
	And Enter
		| Field           | Value  |
		| disp_order@name | 4      |
		| namename@name   | SAMPLE |
	And Click b_register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]

#------------------------------------------------------
Scenario: 1.2 Modify Category List
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
		| namename |
		| SAMPLE   |
	And Insert Template 7.2/cts_enquiry_category_1_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[0].edit_btn Wait[Loaded]
	And Enter
		| Field           | Value  |
		| categoryEditList[3].l_disp_order | 4      |
		| categoryEditList[3].l_namename   | SAMPLE Modified |
	And Click b_register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]
	Then Assert List categoryEditList
		| l_active | l_code | l_disp_order | l_namename |
		| 1        | 1      | 1            | TEL        |
		| 1        | 2      | 2            | WEB        |
		| 1        | 3      | 3            | FAX        |
		| 1        | 4      | 4            | SAMPLE Modified    |
	

#------------------------------------------------------
Scenario: 1.3 Delete Category List
#------------------------------------------------------

	Given Insert Template 7.2/cts_enquiry_category_1_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[0].edit_btn Wait[Loaded]
	And Click categoryEditList[3].l_delete_btn Wait[None]
	And Accept Alert Wait[Loaded]

#------------------------------------------------------
Scenario: 2.1 Register New Category List
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
		| namename |
		| Single   |
	And Insert Template 7.2/cts_enquiry_category_1_t Into cts_enquiry_category_t
	And Delete From cts_enquiry_category_t
		| namename |
		| Single   |

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[1].edit_btn Wait[Loaded]
	And Enter
		| Field           | Value  |
		| disp_order@name | 3      |
		| namename@name   | Single |
	And Click b_register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]

#------------------------------------------------------
Scenario: 2.2 Modify Category List
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
		| namename |
		| Single   |
	And Insert Template 7.2/cts_enquiry_category_2_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[1].edit_btn Wait[Loaded]
	And Enter
		| Field           | Value  |
		| categoryEditList[2].l_disp_order | 3      |
		| categoryEditList[2].l_namename   | Single Modified |
	And Click b_register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]
	Then Assert List categoryEditList
		| l_active | l_code | l_disp_order | l_namename |
		| 1        | 1      | 1            | Inbound    |
		| 1        | 2      | 2            | Outbound   |
		| 1        | 3      | 3            | Single Modified    |

#------------------------------------------------------
Scenario: 2.3 Delete Category List
#------------------------------------------------------

	Given Insert Template 7.2/cts_enquiry_category_2_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[1].edit_btn Wait[Loaded]
	And Click categoryEditList[2].l_delete_btn Wait[None]
	And Accept Alert Wait[Loaded]

#------------------------------------------------------
Scenario: 3.1 Register New Category List
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
		| namename     |
		| Subscription |
	And Insert Template 7.2/cts_enquiry_category_1_t Into cts_enquiry_category_t
	And Delete From cts_enquiry_category_t
		| namename     |
		| Subscription |

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[1].edit_btn Wait[Loaded]
	And Enter
		| Field           | Value        |
		| disp_order@name | 4            |
		| namename@name   | Subscription |
	And Click b_register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]

#------------------------------------------------------
Scenario: 3.2 Modify Category List
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
		| namename     |
		| Subscription |
	And Insert Template 7.2/cts_enquiry_category_3_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[2].edit_btn Wait[Loaded]
	And Enter
		| Field                            | Value        |
		| categoryEditList[3].l_disp_order | 4            |
		| categoryEditList[3].l_namename   | Subscription Modified |
	And Click b_register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]
	Then Assert List categoryEditList
		| l_active | l_code | l_disp_order | l_namename      |
		| 1        | 1      | 1            | Product Inquiry |
		| 1        | 2      | 2            | Cancellation    |
		| 1        | 3      | 3            | Return          |
		| 1        | 4      | 4            | Subscription Modified    |

#------------------------------------------------------
Scenario: 3.3 Delete Category List
#------------------------------------------------------

	Given Insert Template 7.2/cts_enquiry_category_3_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[2].edit_btn Wait[Loaded]
	And Click categoryEditList[3].l_delete_btn Wait[None]
	And Accept Alert Wait[Loaded]

#------------------------------------------------------
Scenario: 4.1 Register New Category List
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
		| namename          |
		| Sample Resolution |
	And Insert Template 7.2/cts_enquiry_category_1_t Into cts_enquiry_category_t
	And Delete From cts_enquiry_category_t
		| namename          |
		| Sample Resolution |

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[3].edit_btn Wait[Loaded]
	And Enter
		| Field           | Value  |
		| disp_order@name | 4      |
		| namename@name   | Sample Resolution |
	And Click b_register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]

#------------------------------------------------------
Scenario: 4.2 Modify Category List
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
		| namename |
		| Sample Resolution   |
	And Insert Template 7.2/cts_enquiry_category_4_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[3].edit_btn Wait[Loaded]
	And Enter
		| Field           | Value  |
		| categoryEditList[3].l_disp_order | 4      |
		| categoryEditList[3].l_namename   | Sample Resolution Modified |
	And Click b_register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]
	Then Assert List categoryEditList
		| l_active | l_code | l_disp_order | l_namename |
		| 1        | 1      | 1            | Solved    |
		| 1        | 2      | 2            | Unsolved   |
		| 1        | 3      | 3            | Call DISCO     |
		| 1        | 4      | 4            | Sample Resolution Modified    |

#------------------------------------------------------
Scenario: 4.3 Delete Category List
#------------------------------------------------------

	Given Insert Template 7.2/cts_enquiry_category_4_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[3].edit_btn Wait[Loaded]
	And Click categoryEditList[3].l_delete_btn Wait[None]
	And Accept Alert Wait[Loaded]

#------------------------------------------------------
Scenario: 5.1 Register New Category List
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
		| namename              |
		| Sample BLANK Not Used |
	And Insert Template 7.2/cts_enquiry_category_1_t Into cts_enquiry_category_t
	And Delete From cts_enquiry_category_t
		| namename              |
		| Sample BLANK Not Used |

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[4].edit_btn Wait[Loaded]
	And Enter
		| Field           | Value  |
		| disp_order@name | 2      |
		| namename@name   | Sample BLANK Not Used |
	And Click b_register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]

#------------------------------------------------------
Scenario: 5.2 Modify Category List
#------------------------------------------------------

	Given Delete From cts_enquiry_category_t
		| namename |
		| Sample BLANK Not Used   |
	And Insert Template 7.2/cts_enquiry_category_5_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[4].edit_btn Wait[Loaded]
	And Enter
		| Field           | Value  |
		| categoryEditList[0].l_disp_order | 1      |
		| categoryEditList[0].l_namename   | Sample BLANK Not Used Modified |
	And Click b_register_edit_btn Wait[None]
	And Accept Alert Wait[Loaded]
	Then Assert List categoryEditList
		| l_active | l_code | l_disp_order | l_namename |
		| 1        | 2      | 1            | Sample BLANK Not Used Modified  |

#------------------------------------------------------
Scenario: 5.3 Delete Category List
#------------------------------------------------------

	Given Insert Template 7.2/cts_enquiry_category_5_t Into cts_enquiry_category_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click category_mngt Wait[Loaded]
	And Click categoryList[4].edit_btn Wait[Loaded]
	And Click categoryEditList[0].l_delete_btn Wait[None]
	And Accept Alert Wait[Loaded]

