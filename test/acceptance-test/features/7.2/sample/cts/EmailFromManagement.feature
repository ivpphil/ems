﻿@V7.2CTS
Feature: CtsEmailFromManagement
			Email From New Registration
			Email From Modification
			Email From Deletion

#-------------------------------------------------------
Scenario: Email From New Registration
#-------------------------------------------------------

# This scenario create new registration of Email From.
# login to cts then go to Other Settings -> Email From Management
# click New Registration button enter data.
# and click Register Button.

	Given Delete From cts_from_address_t
		| email_from_name        |
		| Sample Test Email From |
	
	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click email_fr_mngt Wait[Loaded]
	And Click new_registration_btn Wait[Loaded]
	And Enter
		| Field                | Value                          |
		| email_from_name@name | Sample Test Email From         |
		| email_from@name      | test_email_from@mailinator.com |
		| email_cc@name        |                                |
		| email_bcc@name       |                                |
		| email_header@name    | This is Sample Header.         |
		| email_body@name      | This is Sample Body.           |
		| email_fotter@name    | This is Sample Footer.         |
		#| active_string@name   | 1                              |
	And Click register_btn Wait[None]
	And Accept Alert Wait[Loaded]
 

#-------------------------------------------------------
Scenario: Email From Modification
#-------------------------------------------------------

# This scenario modifies data in the existing Email From.
# login to cts then go to Other Settings -> Email From Management
# click the Modify Button of the Email From you want to edit.
# then assert data and edit data then click Register Button.

	Given Insert Template 7.2/cts_from_address_list_t Into cts_from_address_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click email_fr_mngt Wait[Loaded]
	And Click emailFromList[0].modify_btn Wait[Loaded]
	Then Assert Elements
		|Field|Value|
		| email_from_name@name | Sample Test Email From         |
		| email_from@name      | test_email_from@mailinator.com |
		| email_cc@name        |                                |
		| email_bcc@name       |                                |
		| email_header@name    | This is Sample Header.         |
		| email_body@name      | This is Sample Body.           |
		| email_fotter@name    | This is Sample Footer.         |
	When Enter
		|Field|Value|
		| email_from_name@name | Sample Test Email From Updated         |
		| email_from@name      | test_email_from@mailinator.com |
		| email_cc@name        |                                |
		| email_bcc@name       |                                |
		| email_header@name    | This is Sample Header Updated.         |
		| email_body@name      | This is Sample Body Updated.           |
		| email_fotter@name    | This is Sample Footer Updated.         |
	And Click register_btn Wait[None]
	And Accept Alert Wait[Loaded]


#-------------------------------------------------------
Scenario: Email From Deletion
#-------------------------------------------------------

# This scenario deletes desired Email From.
# login to cts then go to Other Settings -> Email From Management
# click the Delete Button of the Email From you want to delete.

	Given Delete From cts_from_address_t
		| email_from_name        |
		| Sample Test Email From |
		And Insert Template 7.2/cts_from_address_list_t Into cts_from_address_t

	When Open /cts Wait[Loaded]
	And Enter
		| user_id | password |
		| ivpers     | ivpers    |
	And Click login_btn Wait[Loaded]
	And Click other_settings Wait[Loaded]
	And Click email_fr_mngt Wait[Loaded]
	When Click emailFromList[0].delete_btn Wait[None]
	And Accept Alert Wait[Loaded]