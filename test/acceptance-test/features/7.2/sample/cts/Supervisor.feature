﻿@V7.2CTS
Feature: CTS_Supervisor
	This feature contains the Registration, Modification and Deletion of Supervisor

#-------------------------------------------------------------------------------------------------------
Scenario: Supervisor Registration
#-------------------------------------------------------------------------------------------------------
#This scenerio inserts the cts_login_t template into the database
#It opens the Supervisor Management as an Admin
#It registers a new supervisor then clicks the register button
#Check if the registered agent is already in the list of supervisor

Given Insert Template 7.2/cts_login_t Into cts_login_t
	And Delete From cts_login_t
		| user_id |
		| 101     |
	When Open /cts Wait[Loaded]
		And Enter
			| user_id@name | passwd@name |
			| ivpers       | ivpers      |
		And Click login_btn@name Wait[Loaded]
		And Open /cts/operator/manage_supervisors Wait[Loaded] 
		And Enter
		| Field        | Value    |
		| ag_name@name | aly      |
		| user_id@name | 101      |
		| passwd@name  | 12345678 |
	And Click regist@name Wait[None]
	And Accept Alert Wait[Loaded]
	Then Assert List agent_list
	| ag_name       | user_id | intime     | utime      |
	| IVP管理者     | ivpers  | 2012/01/01 | 2012/01/01 |
	| Mik Fernandez | mf007b  | 2016/01/28 | 2016/01/28 |
	| aly           | 101     | 2016/04/12 | 2016/04/12 |


#-------------------------------------------------------------------------------------------------------
Scenario: Supervisor Modification
#-------------------------------------------------------------------------------------------------------
#This scenerio inserts the cts_login_t template into the database
#It opens the Supervisor Management as an Admin
#It modifies the data of a superviosor then clicks modify button
#Check if the changes made is already reflected in the system

Given Delete From cts_login_t
		| user_id |
		| 101     |
		And Insert Template 7.2/cts_login_t Into cts_login_t
	When Open /cts Wait[Loaded]
		And Enter
			| user_id@name | passwd@name |
			| ivpers       | ivpers      |
		And Click login_btn@name Wait[Loaded]
		And Open /cts/operator/manage_supervisors Wait[Loaded] 
		And Click agent_list[2].modify Wait[Loaded]
		And Enter
		| Field        | Value    |
		| ag_name@name | sugoi    |
		| user_id@name | 101      |
		| passwd@name  | 87654321 |
		And Click confirm Wait[None]
		And Accept Alert Wait[Loaded]
	Then Assert List agent_list
	| ag_name       | user_id | intime     | utime      |
	| IVP管理者      | ivpers  | 2012/01/01 | 2012/01/01 |
	| Mik Fernandez | mf007b  | 2016/01/28 | 2016/01/28 |
	| sugoi         | 101     | 2016/04/12 | 2016/04/12 |


#-------------------------------------------------------------------------------------------------------
Scenario: Supervisor Deletion
#------------------------------------------------------------------------------------------------------
#This scenerio inserts the cts_login_t template into the database
#It opens the Supervisor Management as an Admin
#It deletes one of the supervisor by clicking the delete button
#Check if the changes made is already reflected in the system

Given Delete From cts_login_t
		| user_id |
		| 101     |
		And Insert Template 7.2/cts_login_t Into cts_login_t
	When Open /cts Wait[Loaded]
		And Enter
			| user_id@name | passwd@name |
			| ivpers       | ivpers      |
		And Click login_btn@name Wait[Loaded]
		And Open /cts/operator/manage_supervisors Wait[Loaded] 
		And Click agent_list[2].delete Wait[None]
		And Accept Alert Wait[Loaded]
	Then Assert Deleted List agent_list
	| ag_name |
	| aly     |
