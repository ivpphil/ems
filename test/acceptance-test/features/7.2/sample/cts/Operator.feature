﻿@ers7.2CTS
Feature: CTSOperator
	This feature contains test cases for registering modifying and deleteing Delivery CTS Operator.


Scenario: Register Operator
	# This scenario is for registering CTS Operator.
	# Login then open manage_operators
	# Enter values in the ff fields ag_name, user_id, passwd, ag_type
	# Then assert values to check if registered
	Given Insert Template 7.2/cts_login_t Into cts_login_t
		And Delete From cts_login_t
		| user_id |
		| alyaly  |
		When Open /cts Wait[Loaded]
			And Enter
	 		| user_id@name | passwd@name |
	 		| ivpers        | ivpers |
			And Click login_btn@name Wait[Loaded]
			And Open /cts/operator/manage_operators Wait[Loaded]
			And Enter
			| Field        | Value    |
			| ag_name@name | aly      |
			| user_id@name | alyaly   |
			| passwd@name  | 12345678 |
			| ag_type@name | 2        |
			And Click regist@name Wait[None]
			And Accept Alert Wait[Loaded]
		Then Assert List agent_list
			| ag_name | id     | agent   | intime     | utime      |
			| ten        |  tenten  |  管理     |  2016/04/12      |   2016/04/12    |
			| aly     | alyaly | コールセンター | 2016/04/12 | 2016/04/12 |



Scenario: Modify Operator
	# This scenario is for modifying CTS Operator.
	# Login then open manage_operators
	# Enter values in the ff fields ag_name, user_id, passwd, ag_type
	# Then assert values to check if modified
	Given Delete From cts_login_t
		| user_id |
		| alyaly  |
	    And Insert Template 7.2/cts_login_t Into cts_login_t
		When Open /cts Wait[Loaded]
			And Enter
	 		| user_id@name | passwd@name |
	 		| ivpers        | ivpers |
			And Click login_btn@name Wait[Loaded]
			And Open /cts/operator/manage_operators Wait[Loaded]
			And Click agent_list[0].modify Wait[Loaded]
			And Enter
			| Field        | Value    |
			| ag_name@name | bb       |
			| user_id@name | bb       |
			| passwd@name  | bbbbbbbb |
			| ag_type@name | 2        |
			And Click confirm Wait[None]
			And Accept Alert Wait[Loaded]
		Then Assert List agent_list
			| ag_name | id     | agent   | intime     | utime      |
			| bb      | bb     | コールセンター | 2016/04/12 | 2016/04/12 |
			| ten        |  tenten  |  管理     |  2016/04/12      |   2016/04/12    |

Scenario: Delete Operator
	# This scenario is for deleting CTS Operator.
	# Login then open manage_operators
	# Enter values in the ff fields ag_name, user_id, passwd, ag_type
	# Then assert values to check if deleted
	Given Delete From cts_login_t
		| user_id |
		| alyaly  |
	    And Insert Template 7.2/cts_login_t Into cts_login_t
		When Open /cts Wait[Loaded]
			And Enter
	 		| user_id@name | passwd@name |
	 		| ivpers        | ivpers |
			And Click login_btn@name Wait[Loaded]
			And Open /cts/operator/manage_operators Wait[Loaded]
			And Click agent_list[0].delete Wait[None]
			And Accept Alert Wait[Loaded]
		Then Assert List agent_list
			| ag_name | id | agent | intime | utime |
			| ten        |  tenten  |  管理     |  2016/04/12      |   2016/04/12    |