﻿using jp.co.ivp.ers;
using System;
using System.Linq;
using TechTalk.SpecFlow;
using ersTestLibrary.common;
using OpenQA.Selenium.Support.PageObjects;
using TechTalk.SpecFlow.Assist;
using System.Collections.Generic;
using ersTestLibrary.common.extension;
using NUnit.Framework;
using ersTestLibrary.common.db;
using ersSpecs.elements;
using OpenQA.Selenium;
using FluentAssertions;
using ersTestLibrary.common.containerControl;
using OpenQA.Selenium.Support.UI;
using jp.co.ivp.ers.util;
using System.Configuration;
using System.IO;

namespace ersSpecs
{
    [Binding]
    public class BasicSeleniumSteps
        : ErsTestCommon
    {
        #region data preparation
        [Given(@"Select From (.*) AS (.*)")]
        public void GivenSelectFromTABLENAMEASVARIABLENAME(string tableName, string variableName, Table table)
        {
            table = table.ToHorizontal();

            var where = table.ToDictionaryList().FirstOrDefault();

            var repository = new DapperRepository(tableName);
            var criteria = new DapperCriteria();
            foreach (var key in where.Keys)
            {
                criteria[key, jp.co.ivp.ers.db.Criteria.Operation.EQUAL] = where[key];
            }

            var dbData = repository.Find(criteria);

            ScenarioContext.Current[variableName] = dbData;
        }

        [Given(@"Insert Template (.*) Into (.*)")]
        public void GivenInsertTemplateTEMPLATENAMEIntoTABLENAME(string templateName, string tableName, Table values)
        {
            values = values.ToHorizontal();

            ScenarioContext.Current[templateName] = values.Create(templateName, tableName);
        }

        [Given(@"Insert Template (.*) Into (.*)")]
        public void GivenInsertTemplateTEMPLATENAMEIntoTABLENAME(string templateName, string tableName)
        {
            ScenarioContext.Current[templateName] = ((Table)null).Create(templateName, tableName);
        }

        [Given(@"Delete From (.*)")]
        public void GivenDeleteFromTABLENAME(string tableName, Table table)
        {
            table = table.ToHorizontal();

            var listDictionary = table.ToDictionaryList();

            var repository = new DapperRepository(tableName);
            foreach (var record in listDictionary)
            {
                var criteria = new DapperCriteria();
                foreach (var key in record.Keys)
                {
                    criteria[key, jp.co.ivp.ers.db.Criteria.Operation.EQUAL] = record[key];
                }

                repository.Delete(criteria);
            }
        }
        #endregion

        #region Wait Transition
        [When(@"Wait\[Seconds\] (.*)")]
        public void WaitSecondsSeconds(string seconds)
        {
            System.Threading.Thread.Sleep((int)Convert.ToDouble(seconds) * 1000);
        }

        [When(@"Wait\[Loaded\]")]
        public void WaitLoaded()
        {
            WaitElementElementName("ers_page_loaded@id");
        }

        [When(@"Wait\[Element\] (.*)")]
        public void WaitElementElementName(string missing_element_name)
        {
            var findBy = ErsBy.GetFindBy(missing_element_name);

            while (true)
            {
                System.Threading.Thread.Sleep(500);

                var js = (IJavaScriptExecutor)driver;
                if (js.ExecuteScript("return document.readyState").ToString().ToLower().Trim() == ("complete"))
                {
                    break;
                }
            }

            var wait = CommonVariables.GetDefaultWait(driver);

            wait.Until(ExpectedConditions.ElementExists(ErsBy.GetBy(findBy.FindBy, findBy.FieldName)));
        }

        [When(@"Wait\[URL\] (.*)")]
        public void WaitUrlUrl(string missing_url)
        {
            var wait = CommonVariables.GetDefaultWait(driver);
            var pageEnd = CommonVariables.PageEndCommon;

            var targetUrl = setup.pc_nor_url + missing_url.Substring(1);
            CommonSeleniumUtil.WaitUntilIntoContainsNextPage(driver, wait, targetUrl, pageEnd);

            PrepareElement(missing_url);
        }
        #endregion

        #region Control Transition
        [When(@"Open (.*) Wait\[(None|Element|URL|Loaded|Seconds)\] ?(.*)")]
        public void WhenOpenURL(string url, string waitType, string waitArgs)
        {
            // == Prepare == //
            var wait = CommonVariables.GetDefaultWait(driver);
            var pageEnd = CommonVariables.PageEndCommon;

            var targetUrl = setup.pc_nor_url + url.Substring(1);
            CommonSeleniumUtil.ProceedToTargetPage(targetUrl, driver, wait, pageEnd);

            PrepareElement(url);

            Wait(waitType, waitArgs);
        }

        [When(@"Click (.*) Wait\[(None|Element|URL|Loaded|Seconds)\] ?(.*)")]
        public void WhenClickButtonELEMENTNAME(string element_name, string waitType, string waitArgs)
        {
            var findBy = ErsBy.GetFindBy(element_name);

            var buttonElement = element.GetElement(findBy.FieldName, findBy.FindBy);
            if (buttonElement is ErsWebElementList)
            {
                buttonElement = (buttonElement as ErsWebElementList)?.FirstOrDefault((element) => element.Displayed);
            }

            if (buttonElement == null || !buttonElement.Displayed)
            {
                throw new NoSuchElementException(element_name + " from " + driver.Url);
            }

            buttonElement.Click();

            Wait(waitType, waitArgs);
        }

        [When(@"Accept Alert Wait\[(None|Element|URL|Loaded|Seconds)\] ?(.*)")]
        public void WhenAcceptAlert(string waitType, string waitArgs)
        {
            driver.SwitchTo().Alert().Accept();

            Wait(waitType, waitArgs);
        }

        [When(@"Switch\[Popup\] (.*) Wait\[(None|Element|URL|Loaded|Seconds)\] ?(.*)")]
        public void WhenSwitchPopupURL(string url, string waitType, string waitArgs)
        {
            string baseWindow = driver.CurrentWindowHandle;

            var result = driver.SwitchToPopupWindow(baseWindow, url);

            FeatureContext.Current[baseWindow] = result;

            Wait(waitType, waitArgs);
        }

        [When(@"Close\[Popup\] Wait\[(None|Element|URL|Loaded|Seconds)\] ?(.*)")]
        public void WhenClosePopupAndSwitchToDefault(string waitType, string waitArgs)
        {
            foreach (var baseWindow in driver.WindowHandles)
            {
                if (FeatureContext.Current.ContainsKey(baseWindow))
                {
                    var popupwindow = (string)FeatureContext.Current[baseWindow];
                    if (driver.WindowHandles.Contains(popupwindow))
                    {
                        driver.SwitchTo().Window(popupwindow).Close();
                    }

                    driver.SwitchTo().Window(baseWindow);

                    break;
                }
            }

            Wait(waitType, waitArgs);
        }

        [When(@"Switch\[Frame\] (.*) Wait\[(None|Element|URL|Loaded|Seconds)\] ?(.*)")]
        public void WhenInFrameElementNameWaitUrl(string element_name, string waitType, string waitArgs)
        {
            var findBy = ErsBy.GetFindBy(element_name);

            var frameElement = element.GetElement(findBy.FieldName, findBy.FindBy);
            driver.SwitchTo().Frame(frameElement);

            Wait(waitType, waitArgs);
        }

        [When(@"Close\[Frame\] Wait\[(None|Element|URL|Loaded|Seconds)\] ?(.*)")]
        public void WhenOutFrame(string waitType, string waitArgs)
        {
            driver.SwitchTo().DefaultContent();

            Wait(waitType, waitArgs);
        }
        #endregion

        #region Enter Value
        [When(@"Enter")]
        public void WhenEnter(Table table)
        {
            table = table.ToHorizontal();

            var values = table.ToDictionaryList();

            var row = values.FirstOrDefault();

            foreach (var column in table.Header)
            {
                var findBy = ErsBy.GetFindBy(column);
                element.GetElement(findBy.FieldName, findBy.FindBy, false).SendRawValue(row[column], true);
            }
        }

        [When(@"Enter List (.*)")]
        public void WhenEnterListELEMENTNAME(string element_name, Table table)
        {
            var findBy = ErsBy.GetFindBy(element_name);

            table = table.ToHorizontal();
            var values = table.ToDictionaryList();

            var objElement = element.GetElement(findBy.FieldName, findBy.FindBy);
            var elements = objElement as IEnumerable<IWebElement>;
            if (elements == null)
            {
                elements = new[] { (IWebElement)objElement };
            }

            elements.Count().Should().BeGreaterOrEqualTo(values.Count(), findBy.FieldName);

            for (var i = 0; i < values.Count(); i++)
            {
                var inner_element = new GenericElements(elements.ElementAt(i), $"{element_name}[{i}]");
                foreach (var column in table.Header)
                {
                    var columnFindBy = ErsBy.GetFindBy(column);
                    inner_element.GetElement(columnFindBy.FieldName, columnFindBy.FindBy, false).SendRawValue(values[i][column], true);
                }
            }
        }

        [When(@"Attach File")]
        public void WhenAttachFile(Table table)
        {
            table = table.ToHorizontal();

            var values = table.ToDictionaryList();

            var row = values.FirstOrDefault();

            var uploadPath = ErsCommonContext.MapPath(ConfigurationManager.AppSettings["UploadFilePath"]);
            foreach (var column in table.Header)
            {
                var findBy = ErsBy.GetFindBy(column);
                element.GetElement(findBy.FieldName, findBy.FindBy).SendRawValue(Path.Combine(uploadPath, Convert.ToString(row[column])), true);
            }
        }

        [When(@"Attach File List (.*)")]
        public void WhenAttachFileListELEMENTNAME(string element_name, Table table)
        {
            var findBy = ErsBy.GetFindBy(element_name);

            table = table.ToHorizontal();
            var values = table.ToDictionaryList();
            var uploadPath = ErsCommonContext.MapPath(ConfigurationManager.AppSettings["UploadFilePath"]);

            var objElement = element.GetElement(findBy.FieldName, findBy.FindBy);
            var elements = objElement as IEnumerable<IWebElement>;
            if (elements == null)
            {
                elements = new[] { (IWebElement)objElement };
            }

            elements.Count().Should().BeGreaterOrEqualTo(values.Count(), findBy.FieldName);

            for (var i = 0; i < values.Count(); i++)
            {
                var inner_element = new GenericElements(elements.ElementAt(i), $"{element_name}[{i}]");
                foreach (var column in table.Header)
                {
                    var columnFindBy = ErsBy.GetFindBy(column);
                    inner_element.GetElement(columnFindBy.FieldName, columnFindBy.FindBy).SendRawValue(Path.Combine(uploadPath, Convert.ToString(values[i][column])), true);
                }
            }
        }

        #endregion

        #region Assert
        [Then(@"Assert Data @(.*)")]
        public void ThenAssertData(string variableName, Table table)
        {
            table = table.ToHorizontal();

            var values = (List<Dictionary<string, object>>)ScenarioContext.Current[variableName];
            var assertionData = table.ToDictionaryList();

            for (var i = 0; i < Math.Min(values.Count, assertionData.Count()); i++)
            {
                foreach (var key in assertionData[i].Keys)
                {
                    Assert.AreEqual(assertionData[i][key], values[i][key], key);
                }
            }
        }

        [Then(@"Assert Elements")]
        public void ThenAssertElements(Table table)
        {
            table = table.ToHorizontal();

            var exceptedValue = table.ToDictionaryList().FirstOrDefault();

            element.AssertElements(exceptedValue);
        }

        [Then(@"Assert List (.*)")]
        public void ThenAssertListElementName(string element_name, Table table)
        {
            table = table.ToHorizontal();

            var values = table.ToDictionaryList();

            element.AssertElements(new Dictionary<string, object>() { { element_name, values } });
        }

        [Then(@"Assert Deleted List (.*)")]
        public void ThenAssertDeletedListElementName(string element_name, Table table)
        {
            table = table.ToHorizontal();

            var values = table.ToDictionaryDeletionList();

            element.AssertElements(new Dictionary<string, object>() { { element_name, values } });
        }

        #endregion

        #region Helpers
        [Then(@"Do Nothing")]
        public void ThenDoNothing()
        {
        }

        private static void PrepareElement(string url)
        {
            var urlElement = url.Split(new[] { '?', '#' });
            try
            {
                element = ErsDependencyResolver.GetService<ErsElementContainerControlBase>(urlElement[0]);
            }
            catch (ErsResolutionFailedException)
            {
                element = new GenericElements(driver, url);
            }

            if (element != null)
            {
                PageFactory.InitElements(driver, element);
            }
        }

        private void Wait(string waitType, string waitArgs)
        {
            if (waitType == "Element")
            {
                WaitElementElementName(waitArgs);
            }
            else if (waitType == "URL")
            {
                WaitUrlUrl(waitArgs);
            }
            else if (waitType == "Loaded")
            {
                WaitLoaded();
            }
            else if (waitType == "Seconds")
            {
                WaitSecondsSeconds(waitArgs);
            }
        }

        #endregion
    }
}
