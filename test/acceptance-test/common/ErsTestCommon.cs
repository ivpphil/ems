﻿using ersTestLibrary.common;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace ersSpecs
{
    [Binding]
    public class ErsTestCommon
        : ErsTestCommonCompressor
    {
        protected static Setup setup
        {
            get
            {
                return (Setup)FeatureContext.Current["setup"];
            }
            set
            {
                FeatureContext.Current["setup"] = value;
            }
        }

        protected static bool driverLoaded
        {
            get
            {
                return (bool)FeatureContext.Current["driverLoaded"];
            }
            set
            {
                FeatureContext.Current["driverLoaded"] = value;
            }
        }

        protected static IWebDriver driver
        {
            get
            {
                if (!FeatureContext.Current.ContainsKey("driver"))
                {
                    try
                    {
                        driver = OpenBrowser();
                        PrepareBasicAuth();
                    }
                    catch
                    {
                        CloseBrowser();
                        throw;
                    }
                }
                return (IWebDriver)FeatureContext.Current["driver"];
            }
            set
            {
                FeatureContext.Current["driver"] = value;
            }
        }

        protected static ErsElementContainerControlBase element
        {
            get
            {
                return (ErsElementContainerControlBase)FeatureContext.Current["current_page_element"];
            }
            set
            {
                FeatureContext.Current["current_page_element"] = value;
            }
        }

        public static object[] Drivers = CommonVariables.Drivers;
        public object[] DownloadDrivers = CommonVariables.DownloadDrivers;

        [BeforeFeature]
        public static void Init()
        {
            var ersRoot = Path.GetFullPath(ConfigurationManager.AppSettings["ersRoot"]);
            var objErsTestEnvironment = new ErsTestEnvironment();
            objErsTestEnvironment.Initialization(ersRoot);

            setup = ErsFactory.ersUtilityFactory.getSetup();    
            CommonVariables.setup = ErsFactory.ersUtilityFactory.getSetup();
            //SkipTestCaseResourceDictionary.Reload();
            //RunTestCaseResourceDictionary.Reload();

            driverLoaded = false;
        }

        public static IWebDriver OpenBrowser()
        {
            var driver = ((Drivers.First() as object[])[1] as Func<IWebDriver>).Invoke();
            driverLoaded = true;
            return driver;
        }

        private static void PrepareBasicAuth()
        {
            // == Log-in Basic Authentication == //
            CommonVariables.PrepareBasicAuthentication(driver, setup.pc_nor_url);
            CommonVariables.PrepareBasicAuthentication(driver, setup.pc_sec_url);
        }

        [AfterScenario]
        public static void CaptureWhenFailed()
        {
            var error = ScenarioContext.Current.TestError;
            if (error != null)
            {
                var logPath = ErsCommonContext.MapPath(ConfigurationManager.AppSettings["failedLogPath"]);
                Directory.CreateDirectory(logPath);

                var fileNameBase = Path.Combine(logPath, ScenarioContext.Current.ScenarioInfo.Title);

                File.WriteAllText(Path.Combine(logPath, $"{fileNameBase}.txt"), error.ToString());

                if (driverLoaded && driver != null && driver is ITakesScreenshot)
                {
                    File.WriteAllText(Path.Combine(logPath, $"{fileNameBase}.html"), driver.PageSource);

                    int totalWidth = (int)EvalScript<long>("return Math.max.apply( null, [document.body.clientWidth , document.body.scrollWidth, document.documentElement.scrollWidth, document.documentElement.clientWidth] )");
                    int totalHeight = (int)EvalScript<long>("return Math.max.apply( null, [document.body.clientHeight , document.body.scrollHeight, document.documentElement.scrollHeight, document.documentElement.clientHeight] )");
                    driver.Manage().Window.Size = new Size(totalWidth, totalHeight);
                    var ss = ((ITakesScreenshot)driver).GetScreenshot();
                    ss.SaveAsFile(Path.Combine(logPath, $"{fileNameBase}.png"), ImageFormat.Png); //use any of the built in image formating
                }
            }
        }

        public static T EvalScript<T>(string script)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            return (T)js.ExecuteScript(script);
        }

        [AfterFeature]
        public static void CloseBrowser()
        {
            try
            {
                if (driverLoaded && driver != null)
                {
                    driver.Quit();
                    driver = null;
                    driverLoaded = false;
                }
            }
            catch
            { }
        }
    }
}
