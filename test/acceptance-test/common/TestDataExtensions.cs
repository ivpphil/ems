﻿using ersTestLibrary.common.db;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace ersSpecs
{
    public static class TestDataExtensions
    {
        public static List<Dictionary<string, object>> Create(this Table values, string templateName, string tableName)
        {
            var listDictionary = new List<Dictionary<string, object>>();

            // get template values
            var dbData = GetTemplateData(templateName);

            var tableData = values.ToDictionaryList();

            // overwrite with data
            for (var i = 0; i < dbData.Rows.Count; i++)
            {
                var dictionary = dbData.Rows[i].ToDictionary(dbData, dbData.Header);

                var columns = dictionary.Keys.ToArray();

                if (tableData.Count > i)
                {
                    foreach (var column in columns)
                    {
                        if (tableData[i].ContainsKey(column))
                        {
                            dictionary[column] = tableData[i][column];
                        }
                    }
                }

                listDictionary.Add(dictionary);
            }

            var repository = new DapperRepository(tableName);
            foreach (var record in listDictionary)
            {
                var criteria = new DapperCriteria();
                foreach (var key in dbData.Keys)
                {
                    criteria[key, jp.co.ivp.ers.db.Criteria.Operation.EQUAL] = record[key];
                }

                // register values
                if (repository.Find(criteria).Any())
                {
                    repository.Update(record, criteria);
                }
                else
                {
                    repository.Insert(record);
                }
            }

            return listDictionary;
        }

        class ErsTable
            : Table
        {
            public IList<string> Keys { get; private set; }

            public ErsTable(IList<string> keys, params string[] header) : base(header)
            {
                Keys = keys;
            }
        }

        private static ErsTable GetTemplateData(string templateName)
        {
            var templateDataPath = ErsCommonContext.MapPath(ConfigurationManager.AppSettings["templateDataPath"]);

            var templateValues = new Dictionary<string, object>();

            // read csv
            var parser = new TextFieldParser(Path.Combine(templateDataPath, templateName + ".csv"), Encoding.UTF8) { Delimiters = new[] { "," } };

            var retList = new List<object>();

            ErsTable table = null;

            using (parser)
            {
                while (!parser.EndOfData)
                {
                    var row = parser.ReadFields();
                    if(table == null)
                    {
                        // Header
                        var regKey = new Regex(@"^\{(.+)\}$");
                        var key = new List<string>();

                        var columns = new List<string>();
                        foreach (var columnName in row)
                        {
                            var keymatch = regKey.Match(columnName);
                            if (keymatch.Success)
                            {
                                key.Add(keymatch.Groups[1].Value);
                                columns.Add(keymatch.Groups[1].Value);
                            }
                            else
                            {
                                columns.Add(columnName);
                            }
                        }

                        if (!key.Any())
                        {
                            throw new ArgumentException("There is no key in the specified template data");
                        }

                        table = new ErsTable(key, columns.ToArray());
                    }
                    else
                    {
                        table.AddRow(row);
                    }
                }
            }

            return table;
        }

        public static IList<Dictionary<string, object>> ToDictionaryList(this Table table)
        {
            var listDictionary = new List<Dictionary<string, object>>();
            if (table != null)
            {
                foreach (var row in table.Rows)
                {
                    listDictionary.Add(row.ToDictionary(table, table.Header));
                }
            }

            return listDictionary;
        }

        public static AssertionDeletionList<Dictionary<string, object>> ToDictionaryDeletionList(this Table table)
        {
            var listDictionary = new AssertionDeletionList<Dictionary<string, object>>();
            if (table != null)
            {
                foreach (var row in table.Rows)
                {
                    listDictionary.Add(row.ToDictionary(table, table.Header));
                }
            }

            return listDictionary;
        }

        public static Table ToHorizontal(this Table table)
        {
            if (table.Header.FirstOrDefault() != "Field" ||
                !table.Header.Skip(1).All((val) => val == "Value"))
            {
                return table;
            }

            Table newTable = null;

            // Converts vertical to horizontal
            for (var i = 0; i < table.Header.Count; i++)
            {
                var rowValue = table.Rows.Select((row) => row[i]);

                if (i == 0)
                {
                    newTable = new Table(rowValue.ToArray());
                }
                else
                {
                    newTable.AddRow(rowValue.ToArray());
                }
            }

            return newTable;
        }

        public static Dictionary<string, object> ToDictionary(this TableRow row, Table table, ICollection<string> header)
        {
            var dictionary = new Dictionary<string, object>();

            foreach (var column in header)
            {
                var value = row[column];
                if(value.HasValue())
                {
                    var index = table.Rows.ToList().IndexOf(row);
                    dictionary[column] = GetDbValue(value, index);
                }
                else
                {
                    dictionary[column] = null;
                }
            }
            return dictionary;
        }

        private static object GetDbValue(object _value, int currentIndex)
        {
            var value = Convert.ToString(_value);

            var match = Regex.Match(value, @"^\{(.+)\}$");
            if (match.Success)
            {
                var rawValue = match.Groups[1].Value;
                return rawValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select((arrValue) => GetDataFrom(arrValue.Trim(), currentIndex)).ToArray();
            }

            return GetDataFrom(value, currentIndex);
        }

        private static object GetDataFrom(string strValue, int currentIndex)
        {
            if (!strValue.HasValue())
            {
                return null;
            }

            if (!strValue.StartsWith("@"))
            {
                // Case when tha value is not variable
                return strValue;
            }

            var properties = strValue.Substring(1).Split('.');

            Dictionary<string, object> dictionary;
            var arrPath = properties[0].Split(new[] { '[', ']' }, StringSplitOptions.RemoveEmptyEntries);
            if (arrPath.Count() == 1)
            {
                throw new ArgumentException("Variable should be accessed as List. Pelase specify index.");
            }

            var value = ScenarioContext.Current[arrPath[0]] as List<Dictionary<string, object>>;

            if(arrPath[1] == "*")
            {
                dictionary = value?[currentIndex];
            }
            else
            {
                var index = Convert.ToInt32(arrPath[1]);
                dictionary = value?[index];
            }

            if (dictionary == null)
            {
                return null;
            }

            return dictionary[properties[1]];
        }
    }
}
