﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace ersSpecs.common
{
    [Binding]
    public class GivenHelpers
        : Steps
    {
        [Given(@"Login Admin")]
        public void GivenLoggedInAdmin()
        {
            When("Open /admin/top/login/asp/login.asp");
            var table23 = new Table(new string[] { "user_login_id", "passwd" });
            table23.AddRow(new string[] { "ivpers", "ivpers" });
            And("Enter", table23);
            And("Click login_button Wait /admin/");
        }

        [Given(@"Logout Admin")]
        public void GivenLogoutAdmin()
        {
            When("Click common_logout Wait /admin/top/login/asp/logoutaction.asp");
        }
    }
}
