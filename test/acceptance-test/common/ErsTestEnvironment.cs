﻿using System.Collections.Generic;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.template;
using jp.co.ivp.ers.mvc.compile;
using jp.co.ivp.ers.batch;
using ersSpecs.App_Start;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.compile.entity;

namespace ersSpecs
{
    public class ErsTestEnvironment
        : IErsBatchEnvironment
    {
        public void Initialization(string applicationRootPath)
        {
            UnityWebActivator.Start();

            ErsDependencyResolver.RegisterInstance<global::jp.co.ivp.ers.mvc.IErsSiteTypeService>(EnumSiteType.BATCH.ToString(), new ErsBatchSiteService());

            ErsCommonContext.Initialize(null, applicationRootPath);

            SetFactory();

            ErsTemplateParser.ParserList = GetErsTagParserList();

            //ErsPluginBootstrapper.Initialize();
        }

        /// <summary>
        /// ファクトリーセット
        /// </summary>
        protected void SetFactory()
        {
            ErsFactory.ersBasketFactory = new global::jp.co.ivp.ers.basket.ErsBasketFactory();

            ErsFactory.ersMerchandiseFactory = new global::jp.co.ivp.ers.merchandise.ErsMerchandiseFactory();

            ErsFactory.ersCustomerFactory = new global::jp.co.ivp.ers.customer.ErsCustomerFactory();

            ErsFactory.ersMailFactory = new global::jp.co.ivp.ers.sendmail.ErsMailFactory();

            ErsFactory.ersContentsFactory = new global::jp.co.ivp.ers.contents.ErsContentsFactory();

            ErsFactory.ersSessionStateFactory = new global::jp.co.ivp.ers.state.ErsSessionStateFactory();

            ErsFactory.ersViewServiceFactory = new global::jp.co.ivp.ers.viewService.ErsViewServiceFactory();

            ErsFactory.ersUtilityFactory = new global::jp.co.ivp.ers.util.ErsUtilityFactory();

            ErsFactory.ersPointHistoryFactory = new global::jp.co.ivp.ers.customer.ErsPointHistoryFactory();

            ErsFactory.ersAdminUserFactory = new global::jp.co.ivp.ers.admin_user.ErsAdminUserFactory();

            ErsFactory.ErsAtMailFactory = new global::jp.co.ivp.ers.atmail.ErsAtMailFactory();

            ErsFactory.ersTargetFactory = new global::jp.co.ivp.ers.target.ErsTargetFactory();

            ErsFactory.ersDocBundleFactory = new global::jp.co.ivp.ers.doc_bundle.ErsDocBundleFactory();

            ErsFactory.ersCouponFactory = new global::jp.co.ivp.ers.coupon.ErsCouponFactory();

            ErsFactory.ersBatchFactory = new global::jp.co.ivp.ers.batch.ErsBatchFactory();

            ErsFactory.ersOrderFactory = new global::jp.co.ivp.ers.order.ErsOrderFactory();

            ErsFactory.ersCtsTempOrderFactory = new global::jp.co.ivp.ers.cts_temp_order.ErsCtsTempOrderFactory();

            ErsFactory.ersStepScenarioFactory = new global::jp.co.ivp.ers.step_scenario.ErsStepScenarioFactory();

            ErsFactory.ersStepMailFactory = new global::jp.co.ivp.ers.stepmail.ErsStepMailFactory();

            ErsFactory.ersCtsInquiryFactory = new global::jp.co.ivp.ers.Inquiry.ErsCtsInquiryFactory();

            ErsFactory.ersCommonFactory = new global::jp.co.ivp.ers.common.ErsCommonFactory();

            ErsFactory.ersWarehouseFactory = new global::jp.co.ivp.ers.warehouse.ErsWarehouseFactory();

            ErsFactory.ersSummaryFactory = new global::jp.co.ivp.ers.summary.ErsSummaryFactory();

            ErsFactory.ersRankingFactory = new global::jp.co.ivp.ers.ranking.ErsRankingFactory();

            ErsFactory.ersLpFactory = new global::jp.co.ivp.ers.lp.ErsLpFactory();

            ErsFactory.ersUpdateSpecifiedColumnFactory = new global::jp.co.ivp.ers.update_specified_column.ErsUpdateSpecifiedColumnFactory();

            ErsFactory.ersTableSequenceFactory = new global::jp.co.ivp.ers.common.ErsTableSequenceFactory();
        }

        /// <summary>
        /// ERS独自タグのパーサーのリストを取得
        /// </summary>
        /// <returns>パーサーのリスト</returns>
        public List<ErsTemplateEntityBase> GetErsTagParserList()
        {
            var retVal = new List<ErsTemplateEntityBase>();

            retVal.Add(new ErsTagOutputQuery());
            retVal.Add(new ErsTemplateForm());
            retVal.Add(new ErsTemplateAnchor());
            retVal.Add(new ErsTagPartial());
            retVal.Add(new ErsTagRenderAction());
            retVal.Add(new ErsTagDispError());
            retVal.Add(new ErsTagIsError());
            retVal.Add(new ErsTagForeach());
            retVal.Add(new ErsTagIsEqual());
            retVal.Add(new ErsTagIsNotEqual());
            retVal.Add(new ErsTagIsElse());
            retVal.Add(new ErsTagFormatString());
            retVal.Add(new ErsTagFormatCurrency());
            retVal.Add(new ErsTagFormatDate());
            retVal.Add(new ErsTagMultiLine());
            retVal.Add(new ErsTagTable());
            retVal.Add(new ErsTagNonEncode());
            retVal.Add(new ErsRemoveInvalidInputAttributes());

            return retVal;
        }
    }
}
