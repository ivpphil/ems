﻿using ersTestLibrary.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using System.Reflection;
using ersTestLibrary.common.containerControl;
using NUnit.Framework;
using FluentAssertions;

namespace ersSpecs.elements
{
    public class GenericElements
        : ErsElementContainerControlBase
    {
        private ISearchContext searchContext;
        private string url;
        private bool deletion_validator;

        public GenericElements(ISearchContext _searchContext, string _url)
        {
            searchContext = _searchContext;
            url = _url;
        }

        public GenericElements(ISearchContext _searchContext, string _url, bool __deletion_validator)
            : this(_searchContext, _url)
        {
            deletion_validator = __deletion_validator;
        }

        public override IWebElement GetElement(string element_name, string findBy, bool AllowHidden = true)
        {
            ErsElementContainerControlBase currentElement = this;

            var type = this.GetType();
            PropertyInfo property;

            var element_path = element_name.Split(new[] { '.' });
            var findby_path = findBy.Split(new[] { '/' });
            var element = this.GetElementFromString(type, element_path[0], findby_path[0], AllowHidden, out property);

            if (element_path.Count() == 1)
            {
                return element;
            }

            currentElement = new GenericElements(element, url);

            return currentElement.GetElement(string.Join(".", element_path.Skip(1)), string.Join("/", findby_path.Skip(1)));
        }

        protected override object GetValue(Type type, string element_path, string find_by_path, bool AllowHidden, out PropertyInfo property)
        {
            property = null;
            var findBy = ErsBy.GetBy(find_by_path, element_path);
            var elements = searchContext.FindElements(findBy).Where(s => s.Displayed == true || s.Displayed != AllowHidden).ToList();
            if (elements.Count == 0)
            {
                throw new NoSuchElementException("element which " + find_by_path + " has '" + element_path + "' from " + url);
            }

            if (elements.Count == 1)
            {
                return elements.FirstOrDefault();
            }

            return elements;
        }

        public override void AssertElements(Dictionary<string, object> dic, bool HandleNullElement = false)
        {
            foreach (var key in dic.Keys)
            {
                var findBy = ErsBy.GetFindBy(key);

                var element = GetElement(findBy.FieldName, findBy.FindBy);
                var expectedValue = dic[key];

                if (expectedValue is AssertionDeletionList<Dictionary<string, object>>)
                {
                    var listExpectedValue = expectedValue as IList<Dictionary<string, object>>;
                    var listElement = element as IEnumerable<IWebElement>;
                    if (listElement == null)
                    {
                        listElement = new ErsWebElementList(new[] { element });
                    }

                    for (var i = 0; i < listExpectedValue.Count; i++)
                    {
                        foreach (var elem in listElement)
                        {
                            var container = new GenericElements(elem, url, true);
                            container.AssertElements(listExpectedValue[i]);
                        }
                    }
                }
                else if (expectedValue is IList<Dictionary<string, object>>)
                {
                    var listExpectedValue = expectedValue as IList<Dictionary<string, object>>;
                    var listElement = element as IEnumerable<IWebElement>;
                    if (listElement == null)
                    {
                        listElement = new ErsWebElementList(new[] { element });
                    }
                    listElement.Count().Should().BeGreaterOrEqualTo(listExpectedValue.Count, findBy.FieldName);

                    for (var i = 0; i < listExpectedValue.Count; i++)
                    {
                        var container = new GenericElements(listElement.ElementAt(i), url);
                        container.AssertElements(listExpectedValue[i]);
                    }
                }
                else
                {
                    var strExpectedValue = Convert.ToString(expectedValue);
                    if (strExpectedValue.StartsWith("$"))
                    {
                        if (strExpectedValue == "$invisible")
                        {
                            element.Displayed.Should().BeFalse(findBy.FieldName);
                        }
                        else if (strExpectedValue == "$unchecked")
                        {
                            element.Selected.Should().BeFalse(findBy.FieldName + " should be unchecked");
                        }
                    }
                    else
                    {
                        var validator = GetValidator(element, findBy.FieldName);
                        IList<KeyValuePair<string, IWebElement>> webElements = CommonSeleniumReflection.ConvertWebElementToKeyPair(element, key, validator);
                        var webElement = webElements.First();
                        this.AssertElement(null, webElements, expectedValue, validator, HandleNullElement);
                    }
                }
            }
        }

        /// <summary>
        /// Return Tag Type of Element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private ErsAssertElementAttribute GetValidator(IWebElement element, string customerReason)
        {
            string tagName = element.TagName.ToLower();

            if (tagName == "img")
            {
                return new ErsAssertElementAttributeNameAttribute(EnumAttributeName.src) { CustomReason = customerReason };
            }

            AssertionType assertionType;
            if (tagName == "option")
            {
                assertionType = AssertionType.LabelShouldEqual;
            }
            else if (tagName == "select")
            {
                assertionType = AssertionType.SelectShouldSelected;
            }
            else if (tagName == "input")
            {
                string inputType = element.GetAttribute("type").ToLower();
                if (inputType == "checkbox" || inputType == "radio")
                {
                    assertionType = AssertionType.CheckBoxShouldSelected;
                }
                else if (inputType == "hidden")
                {
                    assertionType = AssertionType.TextShouldEqual;
                }
                else
                {
                    assertionType = AssertionType.TextShouldEqual;
                }
            }
            else if (tagName == "textarea")
            {
                assertionType = AssertionType.TextShouldEqual;
            }
            else
            {
                assertionType = AssertionType.LabelShouldEqual;
            }

            if (deletion_validator)
            {
                if (assertionType == AssertionType.LabelShouldEqual)
                {
                    assertionType = AssertionType.LabelShouldNotEqual;
                }
                else if (assertionType == AssertionType.TextShouldEqual)
                {
                    assertionType = AssertionType.TextShouldNotEqual;
                }
            }

            return new ErsAssertElementAttribute(assertionType, customerReason);
        }
    }
}
