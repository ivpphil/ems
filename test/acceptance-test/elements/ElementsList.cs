﻿using ersTestLibrary.common;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace ersSpecs.elements
{
    public class ElementsList
    {
        public static void GetList()
        {
            //// PC Site
            //Add<pc.indexElements>("/");
            //Add<pc.search.search_listElements>("/top/search/asp/list.asp");
            //Add<pc.detail.detailElements>("/top/detail/asp/detail.asp");
            //Add<pc.cart.cartElements>("/top/cart/asp/cart.asp");
            //Add<pc.register.entry_Elements>("/top/register/asp/login.asp");
            //Add<pc.register.entry_Elements>("/top/register/asp/entry.asp");
            //Add<pc.register.entry2_Elements>("/top/register/asp/entry2.asp");
            //Add<pc.register.entry3_Elements>("/top/register/asp/entry3.asp");
            //Add<pc.register.entry4_Elements>("/top/register/asp/entry4.asp");

            //// Admin Site
            //Add<admin.indexElements>("/admin/");
            //Add<admin.login.loginElements>("/admin/top/login/asp/login.asp");
            //Add<admin.regular.bill_searchElements>("/admin/top/regular/asp/bill_search.asp");
            //Add<admin.regular.bill_listElements>("/admin/top/regular/asp/bill_list.asp");
            //Add<admin.regular.bill_detailElements>("/admin/top/regular/asp/bill_detail.asp");
        }

        #region System use
        static UnityContainer container
        {
            get
            {
                return (UnityContainer)FeatureContext.Current["unity_container"];
            }
            set
            {
                FeatureContext.Current["unity_container"] = value;
            }
        }

        public static void Add<T>(string path)
            where T: ErsElementContainerControlBase
        {
            container.RegisterType<ErsElementContainerControlBase, T>(path);
        }

        public static void Register(UnityContainer _container)
        {
            container = _container;

            GetList();
        }
        #endregion
    }
}
