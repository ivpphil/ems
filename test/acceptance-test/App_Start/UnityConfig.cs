using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersPlugin;
using jp.co.ivp.ers.mvc.template;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.MapperProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.validation.range_check;
using ersTestLibrary.common;
using ersSpecs.elements;

namespace ersSpecs.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            ElementsList.Register(container);
            return container;
        });

        private static void RegisterElements(UnityContainer container)
        {
        }

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // Register site information
            new SiteInfoPc().RegisterDI(container);
            new SiteInfoContact().RegisterDI(container);
            new SiteInfoAdmin().RegisterDI(container);
            new SiteInfoMonitor().RegisterDI(container);
            new SiteInfoBatch().RegisterDI(container);
            new SiteInfoSmartPhone().RegisterDI(container);

            container.RegisterInstance<IErsResources>(new DefaultErsResources());
            container.RegisterInstance<IErsViewReader>(new ErsPluginViewReader(new ErsViewReader()));

            container.RegisterType<IMessageResourceDictionary, DefaultMessageResourceDictionary>("default");
            container.RegisterType<IFieldNameResourceDictionary, DefaultFieldNameResourceDictionary>("default");

            container.RegisterType<ICommandBus, DefaultCommandBus>();
            container.RegisterType<IMapperBus, DefaultMapperBus>();

            // Choose Range Checker
            container.RegisterType<IValueRangeChecker, ByteValueRangeChecker>(EnumTextValueRangeChecker.ByteValueRangeChecker.ToString());
            container.RegisterType<IValueRangeChecker, LengthValueRangeChecker>(EnumTextValueRangeChecker.LengthValueRangeChecker.ToString());
        }
    }
}
