@echo off
Set Path=C:\Program Files (x86)\MSBuild\14.0\Bin;C:\Program Files (x86)\NUnit 2.6.4\bin;%PATH%
nuget restore
msbuild ersSpecs.sln /m /nologo /clp:ErrorsOnly /t:Clean;Build
if %ERRORLEVEL% == 0 (
   echo Build Succeeded
   START "" "nunit.exe" "%1\bin\Debug\ersSpecs.dll"
)
