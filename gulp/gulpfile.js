// プラグイン読み込み
var gulp = require('gulp');
var plumber = require("gulp-plumber");
var minify = require("gulp-minify-css");
var rename = require("gulp-rename");
var sass = require('gulp-ruby-sass');
//var sass = require('gulp-sass');
var csscomb = require('gulp-csscomb');
var pleeease = require('gulp-pleeease');
var cssbeautify = require('gulp-cssbeautify');
var styledocco = require('gulp-styledocco');
var convertEncoding = require('gulp-convert-encoding');
var replace = require('gulp-replace');
var runSequence = require('run-sequence');

// sassコンパイルタスク
gulp.task('sass', function() {
	//option設定
	var sassOptions = {
		style:'nested'
	}
	var pleeeaseOptions = {
		autoprefixer: {browsers: ['last 2 versions']},
		minifier: false // minifyはあとで
	}

	var cssbeautifyOptions = {
		indent: '  ',
		openbrace: 'separate-line',
		autosemicolon: true
	}

	//処理実体（gulp-sass用）
//	return gulp.src(['../ers/ers/css/scss/**/*.scss'],{ base: '../ers/ers/css/scss' })
//		.pipe(plumber())  // エラーが発生しても監視を止めない。
//		.pipe(sass(sassOptions))
//		.pipe(csscomb())
//		.pipe(pleeease(pleeeaseOptions))
//		.pipe(cssbeautify(cssbeautifyOptions))
//		.pipe(gulp.dest('../ers/ers/css'))
//		.pipe(minify())
//		.pipe(rename({extname: ".min.css"}))
//		.pipe(gulp.dest("../ers/ers/css"));

	//処理実体（gulp-ruby-sass用）
//	return sass('../program/pc/shop/css/scss/**/*.scss', sassOptions) /**/
	return sass('../ers/ers/css/scss/**/*.scss', sassOptions) /**/
		.pipe(plumber())   //エラーが発生しても監視を止めない。
		.pipe(csscomb())
		.pipe(pleeease(pleeeaseOptions))
		.pipe(cssbeautify(cssbeautifyOptions))
		.pipe(gulp.dest('../ers/ers/css'))
		.pipe(minify())
		.pipe(rename({extname: ".min.css"}))
		.pipe(gulp.dest("../ers/ers/css"));
});

// Styledocco作成タスク
gulp.task('styledocco', function () {
	//未完成です。使わないで下さい 20160201onoe
	return gulp.src(['../ers/ers/css/scss/**/*.scss'],{ base: '../ers/ers/css/scss' })
	.pipe(styledocco({
		out: '../document/styledocs',
		name: 'Brooks Brothers Japan',
		'include': ['../ers/ers/css/base.css','../ers/ers/css/module.css','../ers/ers/css/layout.css'],
		'no-minify': true
	}))
});

// デフォルトタスク
gulp.task('default', function() {
	gulp.watch("../ers/ers/css/scss/**/*.scss", ['default_exec']);
	//gulp.watch("../program/pc/shop/css/scss/**/*.scss", ['styledocco']);
});

// 処理実体
gulp.task('default_exec', function(callback) {
	//sass→styledoccoの順で処理
	runSequence('sass', 'styledocco', callback);
});
